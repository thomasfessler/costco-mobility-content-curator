//
//
//  Created by Fessler, Thomas
//

//#import <Foundation/Foundation.h>
//#import <QuartzCore/QuartzCore.h>

@protocol CWCAppDefines <NSObject>

#define useDebugEndpoints      false
#define debugOutput         true   //an available switch to output data to the console
#define NONE                @" "
		
typedef enum {
    errorImageWidth,
    errorProdctNameLegth,
	errorImagename,
	errorReqiuredFieldMissing
} errorCodes;

typedef enum {
    sortAll,
	sortAllErrors,
    sortNoErrors,
	sortProofed,
	sortApproved
} couponSortModes;

typedef enum {
	listView = 0,
	largeView,
	legalList
} couponLayout;



#define HEADER_FONT_SIZE 24
#define CELL_WIDTH 155
#define DEFAULT_SPLIT 180

#define FLY_ANIMATION_TIME 0.5f
#define RADIANS(degrees) ((degrees * (float)M_PI) / 180.0f)

typedef enum appModules {
    PhotoCenter = 0,
    OnlineOffers,
    WarehouseCouponOffers,
    Locations,
    CostcoCom,
    Membership,
    Connection,
    Services,
    Pharmacy,
    BusinessDelivery,
    ShoppingList,
    QRScanner,
    CustomerService,
    MyAccount,
    } appModule;


typedef enum messageViewTypes {
    messageOkDismiss = 0,
    messageTwoButtons
} messageViewType;




@end
