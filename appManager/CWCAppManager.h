//
//  CWCAppManager.h
//  CostcoApp
//
//  Created by Thomas Fessler on 4/21/14.
//  Copyright (c) 2014 Costco Wholesale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWCAppDefines.h"
#import "mvmCouponTextView.h"
#import "couponObjectFactory.h"
#import "couponBookObject.h"
#import "NSButton+TextColor.h"

@interface CWCAppManager : NSObject{
 
    
}


 +(id)sharedAppManager;

	@property (nonatomic, strong) couponObjectFactory *currentCouponObject;



    @property (nonatomic, strong) NSMutableDictionary *CWCAppConfigurationDictionary;
    @property (nonatomic, strong) NSArray *CWCMainMenuItemsArray;
    @property (nonatomic, strong) NSMutableDictionary *CWCMainMenuItemsDictionary;
    @property (nonatomic, strong) NSDictionary  *CWCAppTextDictionary;
    @property (nonatomic, strong) NSDictionary  *CWCAppURLDictionary;
	@property (nonatomic, strong) NSDictionary  *CWCColorDictionary;
	@property (nonatomic, strong) NSString* currentLanguage;
    @property (nonatomic, strong) NSString* webViewURL;
    @property (nonatomic, strong) NSString* lastWebURL;
    @property (nonatomic, strong) NSMutableArray *CWCmvmDataArray;
	@property (nonatomic, strong) NSString* mvmActiveCoupon;
	@property (nonatomic, strong) NSString* selectionVaries;
	@property (nonatomic, strong) NSMutableArray *couponObjectArray;

	@property (nonatomic, strong) NSMutableArray *CWCdepartmentsArray;
	@property (nonatomic, strong) NSMutableDictionary *CWCdepartmentsDictionary;
	@property (nonatomic, strong) NSMutableDictionary *CWCdepartmentSortDictionary;


@property (nonatomic, strong) couponBookObject *couponCoverObject;
	@property (nonatomic, strong) NSString* couponCSSHeaderString;
	@property int mvmCouponIndex;
	@property couponSortModes currentSortMode;


-(NSString*)lookupTextWithKey:(NSString*)textKey;
-(NSDictionary*) loadConfigurationFromLocal: (NSString*)resourceName resourceType:(NSString*)resourceType;
//-(void)showModalDialogWithDismiss:(NSString*)messageTitle withMessage:(NSString*)messageText withButtonText:(NSString*)dismissButtonText withButtonAction:(NSString*)buttonAction;

//-(void) setAccessibilityForObject:(UIBarButtonItem*)navigationItem withAccessibilityLabel:(NSString*)navAccessibilityLabel withAccessibilityHint:(NSString*)navAccessibilityHint andAccessibilityIdentifier:(NSString*)navAccessibilityIdentifier;

//-(bool) networkTestWithDialog;
-(void) saveJSON;

- (void)saveCouponCover:(couponBookObject*)couponCoverCtx withFilename:(NSString*)resourceName;
-(couponBookObject*)loadCouponCoverWithFilename:(NSString*)resourceName andFileType:(NSString*)resourceType;

-(void)saveCouponArray:(NSArray*)couponArray withFilename:(NSString*)resourceName;

-(NSMutableArray*)loadCouponCouponArrayWithFilename:(NSString*)resourceName andFileType:(NSString*)resourceType;

-(NSMutableArray*) createArrayForExport;

-(void)editActiveCouponImage;

@end
