//
//  CWCAppManager.m
//  CostcoApp
//
//  Created by Thomas Fessler on 4/21/14.
//  Copyright (c) 2014 Costco Wholesale. All rights reserved.
//

#import "CWCAppManager.h"
//#import "CWCNetworkEngine.h"
//#import "couponData.h"
#import "couponObjectFactory.h"



@implementation CWCAppManager : NSObject{
    
}

static CWCAppManager *sharedInstance = nil;
@synthesize CWCAppConfigurationDictionary, CWCMainMenuItemsDictionary, CWCMainMenuItemsArray, CWCAppTextDictionary, webViewURL, CWCmvmDataArray, mvmActiveCoupon, mvmCouponIndex, couponObjectArray, CWCColorDictionary, currentSortMode, currentCouponObject, couponCoverObject,couponCSSHeaderString,currentLanguage,CWCAppURLDictionary,lastWebURL,CWCdepartmentsArray,CWCdepartmentsDictionary, CWCdepartmentSortDictionary;

+ (id)sharedAppManager {
    static CWCAppManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


- (id)init
{
    self = [super init];
    if (self) {
		self.currentSortMode = sortAll;
        self.currentLanguage  = @"EN-US";
//        [self setupMainMenuItems];
		
		self.CWCAppTextDictionary = [self setupAppTextDictionaryWithJSONResourceName:@"CWCLanguage" andResourceType:@"json"];
		self.CWCdepartmentsArray =  [[self loadConfigurationFromLocal:@"CWCdepartments" resourceType:@"json"] mutableCopy];

		self.CWCdepartmentsDictionary = [[NSMutableDictionary alloc] init];
		
		for(NSMutableDictionary* departmentDict in self.CWCdepartmentsArray){
			NSMutableDictionary *myDict = [[NSMutableDictionary alloc] init];
			
			NSString *departmentString =[NSString stringWithFormat:@"%ld", [[departmentDict objectForKey:@"department"]longValue]];
			[myDict setValue:departmentString forKey:@"department"];
			[myDict setValue:[departmentDict valueForKey:@"description"] forKey:@"description"];
			[myDict setValue:[departmentDict objectForKey:@"sort"] forKey:@"sort"];
			[departmentDict setValue:[departmentDict objectForKey:@"sort"] forKey:@"sort"];
			[departmentDict setObject:departmentString forKey:@"department"];
			[departmentDict removeObjectForKey:@"temp"];
			[self.CWCdepartmentsDictionary setValue:myDict forKey:departmentString];
		}
		
		self.couponCSSHeaderString = [self loadStringFromLocal:@"	" resourceType:@"html"];
		self.couponCoverObject = [[couponBookObject alloc] init];
		self.couponCoverObject =  [self loadCouponCoverWithFilename:@"couponCover" andFileType:@"data"];
		self.couponObjectArray = [self setupCouponDictionaryWithJSONFileName:@"CWCCoupons" andResourceType:@"json"];
		self.CWCColorDictionary =  [self loadConfigurationFromLocal:@"CWCColors" resourceType:@"json"];
		self.CWCAppConfigurationDictionary =  [[self loadConfigurationFromLocal:@"CWCAppData" resourceType:@"json"] mutableCopy];
		self.currentLanguage = @"EN-US";
		mvmCouponIndex = 0;
		
		
		
    }
    return self;
}


-(NSDictionary*) setupAppTextDictionaryWithJSONResourceName:(NSString*)resourceName andResourceType:(NSString*)resourceType{
	
	NSDictionary* textDict = [self loadConfigurationFromLocal:@"CWCLanguage" resourceType:@"json"];
    return textDict;
}

-(NSMutableArray*) createArrayForExport{
	
	
	NSMutableArray* output = [[NSMutableArray alloc] init];
	for(NSMutableDictionary* departmentDict in self.CWCdepartmentsArray){
		
		NSMutableArray* offers = [[NSMutableArray alloc] init];
		
		for(couponObjectFactory* coupon in couponObjectArray){
			if([coupon.sortOrder isEqualToString:[departmentDict valueForKey:@"sort"]]){
				
				[offers addObject:[coupon encodeForExport]];
				
			}
			if(offers.count > 0){
				[departmentDict setObject:offers forKey:@"offers"];
			}
		}
		
	}
	
	
	for(NSMutableDictionary* departmentDict in self.CWCdepartmentsArray){
		
		if([departmentDict valueForKeyPath:@"offers"] != nil){
			[output addObject:departmentDict];
		}
		
		
		
	}
	
	
	
//	NSMutableArray* couponArray = [[NSMutableArray alloc] init];
//	for(couponObjectFactory* coupon in couponObjectArray){
//		[couponArray addObject:[coupon encodeForExport]];
//		
//	}
	return output;
}



-(NSMutableArray*) setupCouponDictionaryWithJSONFileName:(NSString*)resourceName andResourceType:(NSString*)resourceType{

	NSMutableArray *couponTemp = [NSMutableArray alloc];
	couponTemp  =  [[self loadJsonArrayFromLocal:resourceName resourceType:resourceType] mutableCopy];
	NSMutableArray* couponArray = [[NSMutableArray alloc] init];
//	NSMutableArray* HTMLArray = [[NSMutableArray alloc] init];
//	NSString* HTMLString = @" ";
	
	[[self.CWCAppTextDictionary objectForKey:self.currentLanguage] setValue:self.couponCSSHeaderString forKey:@"css"];
	
	for(NSMutableDictionary* dict in couponTemp){
		couponObjectFactory *couponObject = [[couponObjectFactory alloc] initWithCouponJSONDictionary:dict withCouponCoverObject:self.couponCoverObject withSortDictionary:self.CWCdepartmentsDictionary andLanguageDictionary:[self.CWCAppTextDictionary objectForKey:self.currentLanguage]];
		[couponArray addObject:couponObject];
//		[HTMLArray addObject:couponObject.couponTextDetailHTML];
//		HTMLString = [NSString stringWithFormat:@"%@%@",HTMLString, couponObject.couponTextDetailHTML];
	}
	
//	couponObjectFactory *couponFooterObject = [[couponObjectFactory alloc] init];
//	couponFooterObject.couponType = 2;
//	couponFooterObject.cellHeight = 260;
//	couponFooterObject.termsAndConditionsText = couponFooterObject.couponTextList = [[self.CWCAppTextDictionary valueForKey:@"EN-US"] valueForKey:@"mvmTerms"];
//	[couponFooterObject prerenderAttributedTextForCouponStyle:2];
//	[couponArray addObject:couponFooterObject];

	
	
//	self.CWCmvmDataArray = [couponTemp mutableCopy];
//	NSLog(@"%@",HTMLString);
	
	return couponArray;

}


/************************************
 method:    lookupTextWithKey
 Action:    looks up a text string from the localization dictionary for the current selected language
 Inputs:    CWCAppManager
 Outputs:   NSString
 Success:   Returns a string of localized text
 Failure:   Returns the text of "NONE"
 */



-(NSString*)lookupTextWithKey:(NSString*)textKey{
    
    NSString* lookupText = [(self.CWCAppTextDictionary)[self.currentLanguage] valueForKey:textKey];
    
    if(lookupText != nil && lookupText.length > 0){
        return lookupText;
    }
    
    return @"NONE";
}




-(void) setupMainMenuItems{
    self.CWCAppConfigurationDictionary = [[self loadConfigurationFromLocal:@"CWCAppData" resourceType:@"json"] mutableCopy];
    self.CWCMainMenuItemsArray = [CWCAppConfigurationDictionary[@"menuItems"] copy];

    int count = 0;
    CWCMainMenuItemsDictionary = [[NSMutableDictionary alloc] init];
    
    for(NSMutableDictionary* item in self.CWCMainMenuItemsArray){
        (self.CWCMainMenuItemsDictionary)[[item valueForKey:@"objectName"]] = item;
        [(self.CWCMainMenuItemsDictionary)[item] setValue:@(count) forKey:@"index"];
        count ++;
    }
}




-(NSString*) loadStringFromLocal: (NSString*)resourceName resourceType:(NSString*)resourceType{
	
	NSFileManager *myFile = [ NSFileManager defaultManager];
	NSData* myData = [myFile contentsAtPath:[NSString stringWithFormat:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/%@.%@",resourceName,resourceType]];
	NSString *htmlString = [[NSString alloc] initWithData:myData encoding:NSUTF8StringEncoding];
	
	htmlString = [htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
	
		htmlString = [htmlString stringByReplacingOccurrencesOfString:@"    " withString:@" "];
	
	return htmlString;
	
}



-(NSDictionary*) loadConfigurationFromLocal: (NSString*)resourceName resourceType:(NSString*)resourceType{

	
	
	NSFileManager *myFile = [ NSFileManager defaultManager];
	NSData* myData = [myFile contentsAtPath:[NSString stringWithFormat:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/%@.%@",resourceName,resourceType]];
	NSString *tempString = [[NSString alloc] initWithData:myData encoding:NSUTF8StringEncoding];
	NSDictionary* tempDict = [[NSJSONSerialization JSONObjectWithData:[tempString dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error:nil] mutableCopy];
//	NSLog(@"tempString \n%@",tempString);

	return tempDict;
}


-(void)saveCouponArray:(NSArray*)couponArray withFilename:(NSString*)resourceName{
	
	NSData* fileData = [NSKeyedArchiver archivedDataWithRootObject:couponArray];
    NSString *root = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [root stringByAppendingPathComponent:resourceName];
    [fileData writeToFile:filePath atomically:YES];
}

-(NSArray*)loadCouponCouponArrayWithFilename:(NSString*)resourceName andFileType:(NSString*)resourceType{
	
	NSFileManager *myFile = [ NSFileManager defaultManager];
	NSData* couponCoverData = [myFile contentsAtPath:[NSString stringWithFormat:@"/Users/tfessler/Documents/%@.%@",resourceName,resourceType]];
	NSMutableArray *couponBookArray = [NSKeyedUnarchiver unarchiveObjectWithData:couponCoverData] ;
	return couponBookArray;
}




-(void)saveCouponCover:(couponBookObject*)couponCoverCtx withFilename:(NSString*)resourceName{
	
	NSData* fileData = [NSKeyedArchiver archivedDataWithRootObject:couponCoverCtx];
    NSString *root = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [root stringByAppendingPathComponent:resourceName];
    [fileData writeToFile:filePath atomically:YES];
}


-(couponBookObject*)loadCouponCoverWithFilename:(NSString*)resourceName andFileType:(NSString*)resourceType{

	NSFileManager *myFile = [ NSFileManager defaultManager];
	NSData* couponCoverData = [myFile contentsAtPath:[NSString stringWithFormat:@"/Users/tfessler/Documents/%@.%@",resourceName,resourceType]];	
	couponBookObject *couponBookDict = [NSKeyedUnarchiver unarchiveObjectWithData:couponCoverData];
	return couponBookDict;
}





-(void)saveArrayToDocuments:(NSArray*)fileArray withFilename:(NSString*)resourceName{
	
	NSData* fileData = [NSKeyedArchiver archivedDataWithRootObject:fileArray];
    NSString *root = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [root stringByAppendingPathComponent:resourceName];
    [fileData writeToFile:filePath atomically:YES];
}



-(NSArray*) loadArrayFileFromDocumentsNames:(NSString*)resourceName ofResourceType:(NSString*)resourceType{
  
    NSString* path = [[NSBundle mainBundle] pathForResource:resourceName ofType:resourceType];
	NSData *arrayData = [NSData dataWithContentsOfFile:path];
	NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];

	
	return array;
}




-(void)editActiveCouponImage{
	
	
	NSString *path = [NSString stringWithFormat:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/%@",self.currentCouponObject.imageFilenameMobile];
	[[NSWorkspace sharedWorkspace] openFile:path withApplication:[[CWCAppConfigurationDictionary valueForKey:@"appSettings"] valueForKey:@"photoEditor"]];
	
}



-(void)saveNSDataToDocumentsDirectory:(NSData*)fileData resourceName:(NSString*)resourceName{
	
    NSString *root = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [root stringByAppendingPathComponent:resourceName];
    [fileData writeToFile:filePath atomically:YES];
}



-(void)saveJSON{
    
	
	NSMutableDictionary *exportDict = [[NSMutableDictionary alloc] init];
	
	[exportDict setValue:[self.couponCoverObject encodeForExport] forKey:@"couponCover"];
	[[exportDict objectForKey:@"couponCover"] setValue:[self createArrayForExport] forKey:@"departments"];


	
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:exportDict
                                                       options:0
                                                         error:nil];
    NSString *jsonString;
    if(jsonData){
        jsonString = [[NSString alloc] initWithBytes:[jsonData bytes]
											  length:[jsonData length]
											encoding:NSUTF8StringEncoding];
    }
	NSLog(@"\n%@",jsonString);
	
    if(jsonString != nil){
	[self saveStringToDocumentsDirectory:jsonString resourceName:@"mvmData" extension:@"json"];
		
    }
    
}

#pragma mark Save File


-(NSArray*) loadJsonArrayFromLocal: (NSString*)resourceName resourceType:(NSString*)resourceType{
    NSError*  error;
    NSString* path = [[NSBundle mainBundle] pathForResource:resourceName ofType:resourceType];
    NSString* jsonData = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray* tempDict = [[NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers error:nil] mutableCopy];
	
    return tempDict;
}



-(void)saveStringToDocumentsDirectory:(NSString *)fileData resourceName:(NSString *)resourceName extension:(NSString*)extension{
		
    NSString* fileName = [NSString stringWithFormat:@"%@.%@",resourceName,extension];
    NSString *root = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [root stringByAppendingPathComponent:fileName];
    NSError* error;
    [fileData writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"saved file to Documents Directory");
    
}


-(void)saveStringToProjectDirectory:(NSString *)fileData resourceName:(NSString *)resourceName extension:(NSString*)extension{
    
    NSString* fileName = [NSString stringWithFormat:@"%@.%@",resourceName,extension];
    NSString *root = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *filePath = [root stringByAppendingPathComponent:fileName];
    NSError* error;
    [fileData writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    NSLog(@"saved file to Documents Directory");
    
}



-(BOOL) eraseInDocumentsDirectoryWithFilename:(NSString*)fileName extension:(NSString*)extension{
	{
		fileName = [NSString stringWithFormat:@"%@.%@",fileName,extension];
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSString * documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
		
		NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
		NSError *error;
		BOOL success =[fileManager removeItemAtPath:filePath error:&error];
		if (success) {
			NSLog(@"Erased:%@ in documents directory:%@",fileName, filePath);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}



-(BOOL)fileExistsInDocumentsDirectory:(NSString*)resourceName extension:(NSString*)extension{
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
	NSString *completeFileName = [NSString stringWithFormat:@"%@.%@",resourceName,extension];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:completeFileName];
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
	
	NSLog(@"File:%@ in documents directory:%i",completeFileName, fileExists);
	return fileExists;
    
}


-(NSString*)loadStringFromDocumentsDirectory:(NSString*)resourceName extension:(NSString*)extension{
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = paths[0];
    
	NSString *completeFileName = [NSString stringWithFormat:@"%@.%@",resourceName,extension];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:completeFileName];
	NSError* error;
	NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
	if(error!=nil){
		
		return nil;
	}else{
        
		return content;
	}
}




@end
