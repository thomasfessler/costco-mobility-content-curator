//
//  couponCoverObject.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/16/14.
//
//

#import <Foundation/Foundation.h>

@interface couponBookObject : NSObject{
	
	
	
}


@property (strong) NSString *territory;
@property int territoryID;

@property (strong) NSString *couponBookTitle;
@property (strong) NSString *couponBookSavingsText;

@property (strong) NSDate	*couponBookStartDate;
@property (strong) NSDate	*couponBookEndDate;
@property (strong) NSString	*couponBookValidText;

@property (strong) NSString	*couponBookImageFilename;
@property (strong) NSString	*couponImagePath;

//@property (strong) NSString *couponTermsLinkText;
//@property (strong) NSString *couponTermsLinkURL;
@property (strong) NSString *couponBookInstructions;
@property (strong) NSString *couponBookTermsAndConditions;
@property (strong) NSString *couponBookADAText;

@property (strong) NSMutableAttributedString *styledCoverTitle;
@property (strong) NSMutableAttributedString *styledNumDays;
@property (strong) NSMutableAttributedString *styledDaysRemainding;
@property (strong) NSMutableAttributedString *styledSavingsLine;
@property (strong) NSMutableAttributedString *styledValidString;

@property int territoryValue;

@property int fontSizeTitle;
@property int fontSizeDays;
@property int fontSizeRemaining;
@property int fontSizeSavings;
@property int fontSizeValid;

@property (strong) NSString *colorDays;
@property (strong) NSString *colorSavings;
@property (strong) NSString *colorRemaining;
@property (strong) NSString *colorValid;
@property (strong) NSString *couponBookTitleColor;
@property (strong) NSString *couponBookTitleShadowColor;
@property (strong) NSString *colorFooter;

- (void) updateCoverObject:(couponBookObject*) coverPageCtx withLanguageDict:(NSDictionary*)languageDictCtx;

- (NSDictionary*)encodeForExport;
@end
