//
//  couponCoverObject.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/16/14.
//
//

#import "couponBookObject.h"
#import "NSColor+hexcolor.h"

@implementation couponBookObject
@synthesize couponBookTitle, couponBookEndDate, couponBookInstructions,couponBookStartDate,couponBookTermsAndConditions, territory, couponBookSavingsText, couponBookImageFilename,couponImagePath,couponBookADAText,styledCoverTitle,styledDaysRemainding,styledNumDays,styledSavingsLine,styledValidString,colorValid,colorSavings,colorDays,couponBookTitleColor,couponBookValidText,colorRemaining,fontSizeValid,fontSizeTitle,fontSizeSavings,fontSizeRemaining,fontSizeDays,colorFooter,couponBookTitleShadowColor;


- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super init]) {
		self.couponBookADAText = [decoder decodeObjectForKey:@"couponADAText"];
		self.couponBookTitle = [decoder decodeObjectForKey:@"couponCoverTitle"];
		self.couponBookSavingsText = [decoder decodeObjectForKey:@"couponCoverSavingsLine"];
		self.couponBookEndDate = [decoder decodeObjectForKey:@"couponEndDate"];
		self.couponBookImageFilename = [decoder decodeObjectForKey:@"couponImageFilename"];
		self.couponImagePath = [decoder decodeObjectForKey:@"couponImagePath"];
		self.couponBookInstructions = [decoder decodeObjectForKey:@"couponInstructions"];
		self.couponBookStartDate = [decoder decodeObjectForKey:@"couponStartDate"];
		self.couponBookTermsAndConditions = [decoder decodeObjectForKey:@"couponTermsAndConditions"];
		self.couponBookValidText = [decoder decodeObjectForKey:@"couponValidString"];
		self.couponBookStartDate = [decoder decodeObjectForKey:@"couponStartDate"];
		self.fontSizeDays = [[decoder decodeObjectForKey:@"fontSizeDays"] integerValue];
		self.fontSizeRemaining = [[decoder decodeObjectForKey:@"fontSizeRemaining"] integerValue];
		self.fontSizeSavings = [[decoder decodeObjectForKey:@"fontSizeSavings"] integerValue];
		self.fontSizeTitle = [[decoder decodeObjectForKey:@"fontSizeTitle"] integerValue];
		self.fontSizeValid = [[decoder decodeObjectForKey:@"fontSizeValid"]integerValue];
		
		self.styledCoverTitle = [decoder decodeObjectForKey:@"styledCoverTitle"];
		self.styledDaysRemainding = [decoder decodeObjectForKey:@"styledDaysRemainding"];
		self.styledNumDays = [decoder decodeObjectForKey:@"styledNumDays"];
		self.styledSavingsLine = [decoder decodeObjectForKey:@"styledSavingsLine"];
		self.styledValidString = [decoder decodeObjectForKey:@"styledValidString"];
		
		self.couponBookTitleColor = [decoder decodeObjectForKey:@"colorCouponTitle"];
		self.couponBookTitleShadowColor = [decoder decodeObjectForKey:@"colorCouponTitle"];

		self.territory = [decoder decodeObjectForKey:@"territory"];
		self.territoryID = [decoder decodeObjectForKey:@"territoryID"];
	
	}
	return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {

	[encoder encodeObject:self.couponBookADAText forKey:@"couponADAText"];
	[encoder encodeObject:self.couponBookTitle forKey:@"couponCoverTitle"];
	[encoder encodeObject:self.couponBookSavingsText forKey:@"couponCoverSavingsLine"];

	[encoder encodeObject:self.couponBookEndDate forKey:@"couponEndDate"];
	[encoder encodeObject:self.couponBookImageFilename forKey:@"couponImageFilename"];
	[encoder encodeObject:self.couponImagePath forKey:@"couponImagePath"];
	[encoder encodeObject:self.couponBookInstructions forKey:@"couponInstructions"];
	[encoder encodeObject:self.couponBookStartDate forKey:@"couponStartDate"];
	[encoder encodeObject:self.couponBookTermsAndConditions forKey:@"couponTermsAndConditions"];
	[encoder encodeObject:self.couponBookValidText forKey:@"couponValidString"];
	
	[encoder encodeObject:[NSNumber numberWithInt:self.fontSizeDays] forKey:@"fontSizeDays"];
	[encoder encodeObject:[NSNumber numberWithInt:self.fontSizeRemaining] forKey:@"fontSizeRemaining"];
	[encoder encodeObject:[NSNumber numberWithInt:self.fontSizeSavings] forKey:@"fontSizeSavings"];
	[encoder encodeObject:[NSNumber numberWithInt:self.fontSizeTitle] forKey:@"fontSizeTitle"];
	[encoder encodeObject:[NSNumber numberWithInt:self.fontSizeValid] forKey:@"fontSizeValid"];

	[encoder encodeObject:self.styledCoverTitle forKey:@"styledCoverTitle"];
	[encoder encodeObject:self.styledDaysRemainding forKey:@"styledDaysRemainding"];
	[encoder encodeObject:self.styledNumDays forKey:@"styledNumDays"];
	[encoder encodeObject:self.styledSavingsLine forKey:@"styledSavingsLine"];
	[encoder encodeObject:self.styledValidString forKey:@"styledValidString"];
	
	[encoder encodeObject:self.couponBookTitleColor forKey:@"colorCouponTitle"];
	[encoder encodeObject:self.couponBookTitleShadowColor forKey:@"colorCouponTitle"];

	

	[encoder encodeObject:self.territory forKey:@"territory"];
	[encoder encodeObject:[NSNumber numberWithInt:self.territoryID] forKey:@"territoryID"];
	
}




- (NSDictionary*)encodeForExport {
	
	NSMutableDictionary *encoder = [[NSMutableDictionary alloc] init];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	[encoder setValue:self.couponBookTitle forKey:@"couponBookTitle"];
	[encoder setValue:self.couponBookTitleColor forKey:@"couponBookTitleColor"];
	[encoder setValue:self.couponBookTitleShadowColor forKey:@"couponBookTitleShadowColor"];
	[encoder setValue:[dateFormatter stringFromDate:self.couponBookStartDate] forKey:@"couponBookStartDate"];
	[encoder setValue:[dateFormatter stringFromDate:self.couponBookEndDate] forKey:@"couponBookEndDate"];
	[encoder setValue:self.couponBookSavingsText forKey:@"couponBookSavingsText"];
	[encoder setValue:self.couponBookValidText forKey:@"couponBookValidText"];
	[encoder setValue:self.couponBookInstructions forKey:@"couponBookInstructions"];
	[encoder setValue:self.couponBookTermsAndConditions forKey:@"couponBookTermsAndConditions"];
	[encoder setValue:self.couponBookADAText forKey:@"couponBookADAText"];

	NSDictionary* images = [[NSDictionary alloc] initWithObjectsAndKeys:self.couponBookImageFilename,@"large", nil];
	[encoder setValue:images forKey:@"couponBookImageObject"];

	
	return encoder;
}


- (id) init
{
    self = [super init];
    if (self) {
		

		
	}

	self.fontSizeTitle = 40;
	self.fontSizeDays = 74;
	self.fontSizeRemaining = 25;
	self.fontSizeSavings = 25;
	self.fontSizeValid = 25;
	self.colorValid = self.couponBookTitleColor = self.colorDays = self.colorRemaining = self.colorSavings = @"#CCCCCC";
	self.colorFooter = @"#DD22DD";

	return self;
}


- (id) initCoverPageWithJSONDictionary:(NSDictionary*) ctxCoverPageDict andLanguageDictionary:(NSDictionary*)ctxLangDict{
	
	self = [super init];

	self.fontSizeTitle = 40;
	self.fontSizeDays = 74;
	self.fontSizeRemaining = 12;
	self.fontSizeSavings = 25;
	self.fontSizeValid = 25;
	
	self.colorValid = self.couponBookTitleColor = self.colorDays = self.colorRemaining = self.colorSavings = @"#000000";
	self.couponBookTermsAndConditions = [ctxLangDict valueForKey:@"couponTermsAndConditions"];
	self.couponBookInstructions = [ctxLangDict valueForKey:@"couponInstructions"];
	self.couponBookTermsAndConditions = [ctxLangDict valueForKey:@"couponTermsAndConditions"];
	return self;
	
}



- (void) updateCoverObject:(couponBookObject*) coverPageCtx withLanguageDict:(NSDictionary*)languageDictCtx{

	
	NSLog(@"AM I alive??");
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	NSString* validString = [languageDictCtx valueForKey:@"mvmValid"];

//	NSColor* fontColor = [NSColor colorWithHexString:self.colorCouponTitle];
//	

	
	
	self.couponBookValidText = [[NSString alloc] initWithFormat:@"%@ %@ - %@", validString, [dateFormatter stringFromDate:self.couponBookStartDate], [dateFormatter stringFromDate:self.couponBookEndDate]];
	
	NSLog(@"coupon valid string:%@",self.couponBookValidText);
	
	NSFont *couponTitleFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:fontSizeTitle];
	NSFont *couponDaysFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:fontSizeDays];
	NSFont *couponDaysRemainingFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:self.fontSizeRemaining];
	
	NSFont *couponSavingsFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:fontSizeSavings];
	
	NSFont *couponValidFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:fontSizeValid];

	NSMutableParagraphStyle * couponTitleParagraphStyle = [[NSMutableParagraphStyle alloc] init];
	couponTitleParagraphStyle.alignment = kCTTextAlignmentLeft;
	couponTitleParagraphStyle.minimumLineHeight = self.fontSizeTitle-6 *2;
	couponTitleParagraphStyle.maximumLineHeight = self.fontSizeTitle-6 * 2;
	couponTitleParagraphStyle.lineSpacing = 0;
//
	NSMutableParagraphStyle * couponRemainingParaStyle = [[NSMutableParagraphStyle alloc] init];
	couponRemainingParaStyle.alignment = kCTTextAlignmentLeft;
	couponRemainingParaStyle.minimumLineHeight = self.fontSizeRemaining-2;
	couponRemainingParaStyle.maximumLineHeight = self.fontSizeRemaining-2;
	couponRemainingParaStyle.lineSpacing = 0;

	NSMutableParagraphStyle * centeredParaStyle = [[NSMutableParagraphStyle alloc] init];
	centeredParaStyle.alignment = kCTTextAlignmentCenter;
	
	
//	style cover title
	self.styledCoverTitle = [[NSMutableAttributedString alloc] initWithString:self.couponBookTitle];
	[self.styledCoverTitle addAttribute:NSFontAttributeName value:couponTitleFont range:NSMakeRange(0, [self.styledCoverTitle length])];
	[self.styledCoverTitle addAttribute:NSParagraphStyleAttributeName value:couponTitleParagraphStyle range:NSMakeRange(0, [self.styledCoverTitle length])];
	[self.styledCoverTitle addAttribute:NSForegroundColorAttributeName value:[NSColor colorWithHexString:self.couponBookTitleColor] range:NSMakeRange(0, [self.styledCoverTitle length])];
	[self.styledCoverTitle addAttribute:NSBackgroundColorAttributeName value:[NSColor clearColor] range:NSMakeRange(0, [self.styledCoverTitle length])];
	
//	style number of days
	NSString* days = [NSString stringWithFormat:@"%li", (long)[self daysBetweenDate:self.couponBookStartDate andDate:self.couponBookEndDate]];
	self.styledNumDays = [[NSMutableAttributedString alloc] initWithString:days];
	[self.styledNumDays addAttribute:NSFontAttributeName value:couponDaysFont range:[days rangeOfString:days]];
	
	[self.styledNumDays addAttribute:NSForegroundColorAttributeName value:[NSColor colorWithHexString:self.colorDays] range:[days rangeOfString:days]];

//	style daysRemaining
	self.styledDaysRemainding = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",  [languageDictCtx valueForKey:@"mvmDays"],[languageDictCtx valueForKey:@"mvmRemaining"]]];
	
	[self.styledDaysRemainding addAttribute:NSFontAttributeName value:couponDaysRemainingFont range:NSMakeRange(0, [self.styledDaysRemainding length])];
	
	[self.styledDaysRemainding addAttribute:NSParagraphStyleAttributeName value:couponRemainingParaStyle range:NSMakeRange(0, [self.styledDaysRemainding length])];
	
	[self.styledDaysRemainding addAttribute:NSForegroundColorAttributeName value:[NSColor colorWithHexString:self.colorDays] range:NSMakeRange(0, [self.styledDaysRemainding length])];
	 
//	style Savings
	self.styledSavingsLine = [[NSMutableAttributedString alloc] initWithString:self.couponBookSavingsText];
	
	[self.styledSavingsLine addAttribute:NSFontAttributeName value:couponSavingsFont range:NSMakeRange(0, [self.styledSavingsLine length])];
	
	[self.styledSavingsLine addAttribute:NSForegroundColorAttributeName value:[NSColor colorWithHexString:self.colorSavings] range:NSMakeRange(0, [self.styledSavingsLine length])];
	
	[self.styledSavingsLine addAttribute:NSParagraphStyleAttributeName value:centeredParaStyle range:NSMakeRange(0, [self.styledSavingsLine length])];
	

	//	style Valid
	self.styledValidString = [[NSMutableAttributedString alloc] initWithString:self.couponBookValidText];
	
	[self.styledValidString addAttribute:NSFontAttributeName value:couponValidFont range:NSMakeRange(0, [self.styledValidString length])];
	
	[self.styledValidString addAttribute:NSForegroundColorAttributeName value:[NSColor colorWithHexString:self.colorValid] range:NSMakeRange(0, [self.styledValidString length])];
	[self.styledValidString addAttribute:NSParagraphStyleAttributeName value:centeredParaStyle range:NSMakeRange(0, [self.styledValidString length])];
	
	

}


- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
	
    NSCalendar *calendar = [NSCalendar currentCalendar];
	
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
				 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
				 interval:NULL forDate:toDateTime];
	
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
											   fromDate:fromDate toDate:toDate options:0];
    return [difference day];
}




@end
