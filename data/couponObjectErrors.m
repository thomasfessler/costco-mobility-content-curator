//
//  couponObject.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/12/14.
//
//

#import "couponObjectFactory.h"

@implementation couponObjectFactory
@synthesize bulletsSourceData,productDescriptionBulletText, bulletTextHTML,cents,comBug,countSourceText,dollar,dollarSign,dotComURL, items;

@synthesize currencyDelimiter,imageFilenameMobile,instantSavings,itemLimit,itemNumbers,itemPrefix,couponTextFull, couponTextFullAttributed,off,productName,selectionVaries,item, iconInsertIndex;

@synthesize	couponTextList,couponTextListAttributed ,xtraPricingInfo,styleAndOr,styleChooseFrom, styleOr, upTo, imageURL, productDescriptionCountText, cellHeight, showDotComLink, showBookAppRequired, showUpTo, couponValidText, couponEndDate, couponStartDate,styleTermsAndConditionsTitle, couponTextFullHTML, couponTextListHTML, errorState;

@synthesize errorArray, couponType;


- (id) init
{
    self = [super init];
    if (self) {
        //do setup here
		
	}
    return self;
}

/*
init object from an encoded NSDATA object
 */


- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super init]) {
		self.bulletsSourceData = [decoder decodeObjectForKey:@"bullets"];
		self.bulletTextHTML = [decoder decodeObjectForKey:@"bulletTextHTML"];
		self.productDescriptionBulletText = [decoder decodeObjectForKey:@"bulletTextLarge"];
		self.cellHeight = [[decoder decodeObjectForKey:@"cellHeight"]  integerValue];
		self.cents = [decoder decodeObjectForKey:@"cents"];
		self.countSourceText = [decoder decodeObjectForKey:@"count"];
		self.productDescriptionCountText = [decoder decodeObjectForKey:@"countLarge"];
		self.couponTextFull = [decoder decodeObjectForKey:@"couponTextFull"];
		self.couponTextFullAttributed = [decoder decodeObjectForKey:@"couponTextFullAttributed"];
		self.couponTextList = [decoder decodeObjectForKey:@"couponTextList"];
		self.couponTextListAttributed = [decoder decodeObjectForKey:@"couponTextListAttributed"];
		
		self.couponTextFullHTML = [decoder decodeObjectForKey:@"couponTextFullHTML"];
		self.couponTextListHTML = [decoder decodeObjectForKey:@"couponTextListHTML"];
		
		self.couponType = [[decoder decodeObjectForKey:@"couponType"] integerValue];
		self.dollar = [decoder decodeObjectForKey:@"dollar"];
		self.dollarSign = [decoder decodeObjectForKey:@"dollarSign"];
		self.dotComURL = [decoder decodeObjectForKey:@"dotComURL"];
		self.item = [decoder decodeObjectForKey:@"item"];
		self.itemLimit = [decoder decodeObjectForKey:@"itemLimit"];
		self.itemNumbers = [decoder decodeObjectForKey:@"itemNumbers"];
		self.itemPrefix = [decoder decodeObjectForKey:@"itemPrefix"];
		self.items = [decoder decodeObjectForKey:@"items"];

		self.instantSavings = [decoder decodeObjectForKey:@"instantSavings"];
		self.currencyDelimiter = [decoder decodeObjectForKey:@"currencyDelimiter"];
		self.comBug = [decoder decodeObjectForKey:@"comBug"];
		self.iconInsertIndex = [[decoder decodeObjectForKey:@"insertIndex"] integerValue];

		self.imageFilenameMobile = [decoder decodeObjectForKey:@"imageFilename"];
		self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
		
		self.off = [decoder decodeObjectForKey:@"off"];
		self.productName = [decoder decodeObjectForKey:@"productName"];
		self.selectionVaries = [decoder decodeObjectForKey:@"selectionVaries"];
		self.showBookAppRequired = [[decoder decodeObjectForKey:@"showBookAppRequired"] boolValue];
		self.showDotComLink = [[decoder decodeObjectForKey:@"showDotComLink"] boolValue];
		
		self.termsAndConditionsText = [decoder decodeObjectForKey:@"termsAndConditionsText"];
		self.errorState = [[decoder decodeObjectForKey:@"errorState"] boolValue];
		self.showUpTo = [[decoder decodeObjectForKey:@"showUpTo"] boolValue];
		self.styleAndOr = [decoder decodeObjectForKey:@"styleAndOr"];
		self.styleChooseFrom = [decoder decodeObjectForKey:@"styleChooseFrom"];
		self.styleOr = [decoder decodeObjectForKey:@"styleOr"];
		self.upTo = [decoder decodeObjectForKey:@"upTo"];
		self.xtraPricingInfo = [decoder decodeObjectForKey:@"xtraPricingInfo"];

		self.couponValidText = [decoder decodeObjectForKey:@"couponValidText"];
		self.couponStartDate = [decoder decodeObjectForKey:@"couponStartDate"];
		self.couponEndDate = [decoder decodeObjectForKey:@"couponEndDate"];
		self.styleTermsAndConditionsTitle = [decoder decodeObjectForKey:@"termsAndConditionsTitle"];
	}
	return self;
}




/*
 save object to a saved NSDATA object in encoded format
 */


- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.bulletsSourceData forKey:@"bullets"];
	[encoder encodeObject:self.productDescriptionBulletText forKey:@"bulletTextLarge"];
	[encoder encodeObject:self.bulletTextHTML forKey:@"bulletTextHTML"];
	[encoder encodeObject:[NSNumber numberWithInt:self.cellHeight] forKey:@"cellHeight"];
	[encoder encodeObject:self.cents forKey:@"cents"];
	[encoder encodeObject:self.countSourceText forKey:@"count"];
	[encoder encodeObject:self.productDescriptionCountText forKey:@"countLarge"];
	[encoder encodeObject:self.couponTextFull forKey:@"couponTextFull"];
	[encoder encodeObject:self.couponTextFullAttributed forKey:@"couponTextFullAttributed"];
	[encoder encodeObject:self.couponTextList forKey:@"couponTextList"];
	[encoder encodeObject:self.couponTextListAttributed forKey:@"couponTextListAttributed"];

	[encoder encodeObject:self.couponTextFullHTML forKey:@"couponTextFullHTML"];
	[encoder encodeObject:self.couponTextListHTML forKey:@"couponTextListHTML"];
	
	[encoder encodeObject:[NSNumber numberWithInt:self.couponType] forKey:@"couponType"];
	[encoder encodeObject:self.dollar forKey:@"dollar"];
	[encoder encodeObject:self.dollarSign forKey:@"dollarSign"];
	[encoder encodeObject:self.dotComURL forKey:@"dotComURL"];
	
	[encoder encodeObject:self.instantSavings forKey:@"instantSavings"];
	[encoder encodeObject:self.currencyDelimiter forKey:@"currencyDelimiter"];
	[encoder encodeObject:self.comBug forKey:@"comBug"];
	[encoder encodeObject:[NSNumber numberWithInt:self.iconInsertIndex] forKey:@"insertIndex"];
	
	[encoder encodeObject:self.item forKey:@"item"];
	[encoder encodeObject:self.itemLimit forKey:@"itemLimit"];
	[encoder encodeObject:self.itemNumbers forKey:@"itemNumbers"];
	[encoder encodeObject:self.itemPrefix forKey:@"itemPrefix"];
	[encoder encodeObject:self.items forKey:@"items"];
	
	[encoder encodeObject:self.imageFilenameMobile forKey:@"imageFilename"];
	[encoder encodeObject:self.imageURL forKey:@"imageURL"];

	[encoder encodeObject:self.off forKey:@"off"];
	[encoder encodeObject:self.productName forKey:@"productName"];
	[encoder encodeObject:self.selectionVaries forKey:@"selectionVaries"];
	[encoder encodeObject:[NSNumber numberWithBool:self.showBookAppRequired] forKey:@"showBookAppRequired"];
	[encoder encodeObject:[NSNumber numberWithBool:self.showDotComLink] forKey:@"showDotComLink"];
	[encoder encodeObject:[NSNumber numberWithBool:self.errorState] forKey:@"errorState"];
	
	[encoder encodeObject:self.termsAndConditionsText forKey:@"termsAndConditionsText"];
	
	[encoder encodeObject:[NSNumber numberWithBool:self.showUpTo] forKey:@"showUpTo"];
	[encoder encodeObject:self.styleAndOr forKey:@"styleAndOr"];
	[encoder encodeObject:self.styleChooseFrom forKey:@"styleChooseFrom"];
	[encoder encodeObject:self.styleOr forKey:@"styleOr"];
	[encoder encodeObject:self.upTo forKey:@"upTo"];
	[encoder encodeObject:self.xtraPricingInfo forKey:@"xtraPricingInfo"];
	[encoder encodeObject:self.couponValidText forKey:@"couponValidText"];
	[encoder encodeObject:self.couponStartDate forKey:@"couponStartDate"];
	[encoder encodeObject:self.couponEndDate forKey:@"couponEndDate"];
	[encoder encodeObject:self.styleTermsAndConditionsTitle forKey:@"termsAndConditionsTitle"];
}



/*
 encodes the object into a NSDictionary for export
 generic - needs switches for types of output
 */


- (NSMutableDictionary*)encodeForExport{
	
	NSMutableDictionary *encoder = [[NSMutableDictionary alloc] init];
	
	[encoder setValue:self.couponTextFull forKey:@"couponTextFull"];
	[encoder setValue:self.couponTextList forKey:@"couponTextList"];

	[encoder setValue:self.couponTextFullHTML forKey:@"couponTextFullHTML"];
	[encoder setValue:self.couponTextListHTML forKey:@"couponTextListHTML"];
	
//	[encoder setValue:self.bulletTextHTML forKey:@"bulletTextHTML"];
	[encoder setValue:self.productDescriptionBulletText forKey:@"productDescriptionBulletText"];
	[encoder setValue:[NSNumber numberWithInt:self.cellHeight] forKey:@"cellHeight"];
	[encoder setValue:self.cents forKey:@"cents"];
	[encoder setValue:self.countSourceText forKey:@"count"];
	[encoder setValue:self.productDescriptionCountText forKey:@"productDescriptionCountText"];

//	[encoder setValue:self.couponTextFullAttributed forKey:@"couponTextFullAttributed"];


//	[encoder setValue:self.couponTextListAttributed forKey:@"couponTextListAttributed"];
	[encoder setValue:[NSNumber numberWithInt:self.couponType] forKey:@"couponType"];
	
	[encoder setValue:self.instantSavings forKey:@"instantSavings"];
	
	[encoder setValue:self.upTo forKey:@"upTo"];
	[encoder setValue:self.dollarSign forKey:@"dollarSign"];
	[encoder setValue:self.dollar forKey:@"dollar"];
	[encoder setValue:self.off forKey:@"off"];
	[encoder setValue:self.currencyDelimiter forKey:@"currencyDelimiter"];
//	[encoder setValue:self.comBug forKey:@"comBug"];
//	[encoder setValue:[NSNumber numberWithInt:self.iconInsertIndex] forKey:@"insertIndex"];
	
//	[encoder setValue:self.item forKey:@"item"];
	[encoder setValue:self.itemPrefix forKey:@"itemPrefix"];
	[encoder setValue:self.itemLimit forKey:@"itemLimit"];

	[encoder setValue:self.productName forKey:@"productName"];

	[encoder setValue:self.itemNumbers forKey:@"itemNumbers"];

	[encoder setValue:self.selectionVaries forKey:@"selectionVaries"];
	
	[encoder setValue:self.dotComURL forKey:@"dotComURL"];

	[encoder setValue:self.couponValidText forKey:@"couponValidText"];
	[encoder setValue:self.termsAndConditionsText forKey:@"termsAndConditionsText"];

	
	[encoder setValue:self.imageFilenameMobile forKey:@"imageFilenameMobile"];
	[encoder setValue:self.imageFilenameDesktop forKey:@"imageFilenameDesktop"];
	[encoder setValue:self.imageURL forKey:@"imageURL"];
	
	[encoder setValue:[NSNumber numberWithBool:self.errorState] forKey:@"errorState"];
	
	[encoder setValue:[NSNumber numberWithBool:self.showBookAppRequired] forKey:@"showBookAppRequired"];
	[encoder setValue:[NSNumber numberWithBool:self.showDotComLink] forKey:@"showDotComLink"];
	[encoder setValue:[NSNumber numberWithBool:self.showUpTo] forKey:@"showUpTo"];
	
	[encoder setValue:self.styleAndOr forKey:@"styleAndOr"];
	[encoder setValue:self.styleChooseFrom forKey:@"styleChooseFrom"];
	[encoder setValue:self.styleOr forKey:@"styleOr"];
	[encoder setValue:self.styleTermsAndConditionsTitle forKey:@"styleTermsAndConditionsTitle"];
	
//	[encoder setValue:self.xtraPricingInfo forKey:@"xtraPricingInfo"];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];

	
	[encoder setValue:[dateFormatter stringFromDate:self.couponStartDate] forKey:@"couponStartDate"];
	[encoder setValue:[dateFormatter stringFromDate:self.couponEndDate] forKey:@"couponEndDate"];
	
	[encoder setValue:self.termsAndConditionsText forKey:@"termsAndConditionsText"];
	
	return encoder;
}

/****  Set up coupon object
		import text
		pre-render buid text strings
			pre-render styled text for list view
			pre-render style text for large view
 
 */
- (id) initWithCouponJSONDictionary:(NSDictionary*)ctxCouponDict withCouponCoverObject:(couponCoverObject*)ctxCoverObject andLanguageDictionary:(NSDictionary*)ctxLangDict{
    self = [super init];
	[self importCouponObjectJSONDict:ctxCouponDict withCouponCoverObject:ctxCoverObject andLanguageDictionary:ctxLangDict];
	[self buildCouponTextWithLanguageDictionary:ctxLangDict];
	
	[self checkCouponForErrorsWithLocalizationDict: ctxLangDict];
	
	[self prerenderAttributedTextForCouponStyle:listView];
	[self prerenderAttributedTextForCouponStyle:largeView];
    return self;
}



/*  Imports the JSON payload object into the coupon objct  */
-(void) importCouponObjectJSONDict:(NSDictionary*)ctxCouponDict withCouponCoverObject:(couponCoverObject*)ctxCoverObject andLanguageDictionary:(NSDictionary*)ctxLangDict{
	
	if([ctxCouponDict valueForKeyPath:@"couponType"] == nil){
		self.couponType = 1;
	}
		
	
	self.instantSavings = [ctxLangDict valueForKey:@"mvmInstantSavings"];
	self.off = [ctxLangDict valueForKey:@"mvmOff"];
	self.dollarSign = [ctxLangDict valueForKey:@"mvmDollarSign"];
	self.currencyDelimiter = [ctxLangDict valueForKey:@"mcmCurrencyDelimiter"];
	self.styleAndOr = [ctxLangDict valueForKey:@"mvmStyleAndOr"];
	self.styleChooseFrom = [ctxLangDict valueForKey:@"mvmStyleChooseFrom"];
	self.styleOr = [ctxLangDict valueForKey:@"mvmStyleOr"];
	self.item = [ctxLangDict valueForKey:@"mvmItem"];
	self.items = [ctxLangDict valueForKey:@"mvmItems"];
	self.styleTermsAndConditionsTitle = [ctxLangDict valueForKey:@"mvmLayoutTermsTitle"];
	self.termsAndConditionsText =[ctxLangDict valueForKey:@"mvmLayoutTerms"];
	
	self.couponEndDate = ctxCoverObject.couponEndDate;
	self.couponStartDate = ctxCoverObject.couponStartDate;
	self.couponValidText = ctxCoverObject.couponValidString;
	
	if([ctxCouponDict valueForKey:@"xtraPricingInfo"] != nil){
		
		NSString* localValue = [[ctxCouponDict valueForKey:@"xtraPricingInfo"] uppercaseString];
		
		if([localValue hasPrefix:@"UP"]){
			self.showUpTo = true;
		}
		
		if([localValue hasPrefix:@"IRC"]){
			self.showBookAppRequired = TRUE;
		}
		else{
			self.showBookAppRequired = false;
		}
	}
	
	if ([ctxCouponDict valueForKey:@"comBug"] != nil){
		self.comBug = [ctxCouponDict valueForKey:@"comBug"];
		self.comBug = [self.comBug uppercaseString];
		
		if([self.comBug hasPrefix:@"YES"])
		{
			self.showDotComLink = true;
			
		}
		else{
			self.showDotComLink = false;
		}
	}
	
	
	if ([ctxCouponDict valueForKey:@"xtraPricingInfo"] != nil)
		self.xtraPricingInfo = [ctxCouponDict valueForKey:@"xtraPricingInfo"];
	if ([ctxCouponDict valueForKey:@"couponimage"] != nil)
		self.imageFilenameMobile = [ctxCouponDict valueForKey:@"couponimage"];
	
	if ([ctxCouponDict valueForKey:@"imageurl"] != nil)
		self.imageURL = [ctxCouponDict valueForKey:@"imageurl"];
	
	if ([ctxCouponDict valueForKey:@"dotcomurl"] != nil)
		self.dotComURL = [ctxCouponDict valueForKey:@"dotcomurl"];
	
	//**instantSavings
	self.couponTextList = [NSString stringWithFormat:@"%@\n",self.instantSavings];
	
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"<P><span class=\"instantSavings\">%@</span></BR>",
							   self.instantSavings];
	
	
	//**dollar
	if([ctxCouponDict valueForKeyPath:@"dollar"]!= nil){
		self.dollar = [[ctxCouponDict valueForKey:@"dollar"] stringValue];
	}
	
	//**cents
	if([ctxCouponDict valueForKeyPath:@"cents"]!= nil){
		self.cents = [[ctxCouponDict valueForKey:@"cents"] stringValue];
	}
	
	
	if ([ctxCouponDict valueForKey:@"itemPrefix"] != nil)
		self.itemPrefix = [ctxCouponDict valueForKey:@"itemPrefix"];
	
	//** itemLimit
	if([ctxCouponDict valueForKeyPath:@"itemLimit"]!= nil){
		self.itemLimit = [ctxCouponDict valueForKeyPath:@"itemLimit"];
		self.itemLimit = [self.itemLimit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		self.itemLimit = [self.itemLimit stringByReplacingOccurrencesOfString:@"." withString:@""];
		self.itemLimit = [itemLimit uppercaseString];
		
		if(self.itemLimit.length >4){
					self.itemLimit = [NSString stringWithFormat:@"%@",self.itemLimit];
		}
		
	}
	
	//Start the varation for large layout
	self.couponTextFull = self.couponTextList;
	
	//** mainDescription
	if([ctxCouponDict valueForKeyPath:@"mainDescription"]!= nil){
		self.productName =  [ctxCouponDict valueForKey:@"mainDescription"];
		self.productName = [self.productName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		self.productName = [self.productName stringByTrimmingCharactersInSet:								[NSCharacterSet whitespaceCharacterSet]];
		
	}
	
	//	self.iconInsertIndex = self.couponTextFull.length-1;
	
	//** bullets text items
	if([ctxCouponDict valueForKeyPath:@"bullets"]!= nil){
		NSArray *bulletArray = [NSArray alloc];
		bulletArray = [ctxCouponDict[@"bullets"] componentsSeparatedByString:@"\n"];
		
		
		for (NSString* currentString in bulletArray)
		{
			if([bulletArray count]> 1){
				if(self.productDescriptionBulletText == nil || ![self.productDescriptionBulletText isKindOfClass:[NSString class]] || [self.productDescriptionBulletText length] < 3){
					self.productDescriptionBulletText = [NSString stringWithFormat:@" • %@\n",currentString];
				}
				else{
					self.productDescriptionBulletText = [NSString stringWithFormat:@"%@ • %@\n",self.productDescriptionBulletText,currentString];
				}
			}
		}
		self.bulletsSourceData =[ctxCouponDict valueForKeyPath:@"bullets"];
		self.bulletsSourceData = [self.bulletsSourceData stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
		self.bulletsSourceData = [self.bulletsSourceData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		
	}
	
	//** count items text
	if([ctxCouponDict valueForKeyPath:@"count"]!= nil){
		NSArray *countArray = [NSArray alloc];
		countArray = [ctxCouponDict[@"count"] componentsSeparatedByString:@"\n"];
		
		for (NSString* currentString in countArray)
		{
			if([countArray count]> 1){
				if(self.productDescriptionCountText == nil){
					self.productDescriptionCountText = [NSString stringWithFormat:@"%@",currentString];
				}else{
					self.productDescriptionCountText = [NSString stringWithFormat:@"%@%@",self.productDescriptionCountText,currentString];
				}
			}
			else{
				self.productDescriptionCountText = [NSString stringWithFormat:@"%@",currentString];
			}
		}
		
		self.countSourceText = ctxCouponDict[@"count"];
		self.countSourceText = [countSourceText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		self.countSourceText = [countSourceText stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
		self.countSourceText = [countSourceText stringByReplacingOccurrencesOfString:@".." withString:@"."];
	}
	
	//** itemNumbers items text
	if([ctxCouponDict valueForKeyPath:@"itemNumbers"]!= nil){
		NSArray *tempArray = [NSArray alloc];
		if([[ctxCouponDict valueForKeyPath:@"itemNumbers"] isKindOfClass:[NSString class]]){
			tempArray = [ctxCouponDict[@"itemNumbers"] componentsSeparatedByString:@","];
		}
		
		self.itemNumbers = [NSString stringWithFormat:@"%@",[ctxCouponDict valueForKeyPath:@"itemNumbers"]];
		self.itemNumbers = [NSString stringWithFormat:@"%@ %@",self.item,self.itemNumbers];
		
	}
	
	//** selection varies items
	if([ctxCouponDict valueForKeyPath:@"selectionVaries"]!= nil){
		self.selectionVaries = [ctxCouponDict valueForKeyPath:@"selectionVaries"];
	}
}




/*
 builds coupon plain text & html output
 displayed on the screen
 */


- (void) checkCouponForErrorsWithLocalizationDict:(NSDictionary*) ctxLangDict{
	
	NSString *chooseFrom = [ctxLangDict valueForKey:@"mvmStyleChooseFrom"];
	self.errorState = false;
	self.errorArray = nil;
	self.errorArray = [[NSMutableArray alloc] init];
	
	int location = self.productName.length - chooseFrom.length;

	
	//***
	//*** Test for product name ending in choose from
	if (productName.length >= chooseFrom.length){
//		NSRange range = NSMakeRange(location, chooseFrom.length);

		
		
		if([[self.productName substringFromIndex:location] isEqualToString:chooseFrom] ){
			
			self.errorState = true;
			
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorRequiredFieldNotDefined" andLangDict:ctxLangDict]];
			
			
//			[self.errorArray addObject:[ctxLangDict valueForKeyPath:@"errorImageWidth"]];
		}
	}
	
	
	
//	if (self.imageFilenameMobile != nil && self.imageFilenameMobile.length > 10){
//	
//		NSRange range = NSMakeRange(self.imageFilenameMobile.length-5, 4);
//	
//		
//	if(!([[self.productName substringWithRange:range] isEqualToString:@".PNG"] ||
//		 [[self.productName substringWithRange:range] isEqualToString:@".JPG"]
//		 ))
//		{
//			
//			self.errorState = true;
//			
//		}
//	}else{
//				self.errorState = false;
//		
//	}
	
	
	
	
	//***
	//*** Test for product name length > 80
	
	if(productName == nil) {
		
			self.errorState = true;
//		[self.errorArray addObject:@"product name is empty."];
//		[self.errorArray addObject:[ctxLangDict valueForKeyPath:@"errorRequiredFieldNotDefined"]];
		
					[self.errorArray addObject:[self buildErrorDictWithCode:@"errorRequiredFieldNotDefined" andLangDict:ctxLangDict]];
		

		
	}
	else{
	
	if (productName.length > 97){
			self.errorState = true;
//					[self.errorArray addObject:[ctxLangDict valueForKeyPath:@"errorProductNameLength"]];
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorProductNameLength" andLangDict:ctxLangDict]];
		
//			[self.errorArray addObject:[NSString stringWithFormat:@"product name length is %lu.  Reduce by %lu characters.",(unsigned long)productName.length, 97 -productName.length]];
		
		
	}
	
	if(self.imageFilenameMobile != nil){
			
			NSString *fileName = @"/Users/tfessler/costco-mobility-content-curator/mvmImages/";
			fileName = [fileName stringByAppendingString:self.imageFilenameMobile];
			NSImage* testImage = [[NSImage alloc]initWithContentsOfFile:fileName];
		
//		NSImageView *testView = [[NSImageView alloc] init];
//		[testView setImage:testImage];
		
		if(testImage.size.width > testImage.size.height *1.75){
			
			self.errorState = true;
//			[self.errorArray addObject:[ctxLangDict valueForKeyPath:@"errorImageWidth"]];
			
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorImageWidth" andLangDict:ctxLangDict]];
			
			
//			[self.errorArray addObject:@"mobileImage aspect ratio is too wide"];
			
		}
		
		
			if(testImage == nil){
				self.errorState = true;
//				[self.errorArray addObject:@"mobileImageFilename is incorrect or the file is not compatible."];
				[self.errorArray addObject:[self buildErrorDictWithCode:@"errorImageName" andLangDict:ctxLangDict]];
//			 [ctxLangDict valueForKeyPath:@"errorImageName"]];
				
			}
		
		
		
		
		
	}
//	if([self.productName rangeOfString:@"$"].location != NSNotFound){
//		self.errorState = true;
//	}
	

//		if([self.productName rangeOfString:@"ct"].location != NSNotFound){
//			self.errorState = true;
//		}


	
	}
	
	
}


-(NSDictionary*)buildErrorDictWithCode:(NSString*) errorCode andLangDict:(NSDictionary*)langDict{
	
	NSDictionary* errorDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[langDict valueForKey:errorCode],errorCode, nil];
	
	return errorDictionary;
	
}


- (void) buildCouponTextWithLanguageDictionary:(NSDictionary*)ctxLangDict{
	
	
	//**instantSavings
	self.couponTextList = [NSString stringWithFormat:@"%@\n",self.instantSavings];
	
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"<P><span class=\"instantSavings\">%@</span></BR>",
							   self.instantSavings];
	
	//**dollar
	if(self.dollar != nil){
		self.couponTextList = [NSString stringWithFormat:@"%@%@%@",self.couponTextList,self.dollarSign,self.dollar];
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"dollarSign\">%@</span><span class=\"dollar\">%@</span>",
								   self.couponTextListHTML,self.dollarSign,self.dollar];
	}
	
	
	//**cents
	if(self.cents!= nil){
		self.couponTextList = [NSString stringWithFormat:@"%@%@",self.couponTextList,self.cents];
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"dollarsign\">%@</span>",
								   self.couponTextListHTML,self.cents];
	}
	
	
	
	//** off
	self.couponTextList = [NSString stringWithFormat:@"%@ %@",self.couponTextList,self.off];
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"%@<span class=\"superScript\"> </span><span class=\"off\">%@</span>",
							   self.couponTextListHTML,self.off];
	
	//** itemLimit
	if(self.itemLimit!= nil){
		
		if(self.itemLimit.length < 14){
			self.couponTextList = [NSString stringWithFormat:@"%@%@\n",self.couponTextList,self.itemLimit];
		}
		else{
			self.couponTextList = [NSString stringWithFormat:@"%@\n%@\n",self.couponTextList,self.itemLimit];
		}
		
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"superScript\">%@</span></BR></BR>",
								   self.couponTextListHTML,self.itemLimit];
	}
	
	//Start the varation for large layout
	self.couponTextFull = self.couponTextList;
	
	
	//** mainDescription
	if(self.productName!= nil){
		
		self.couponTextList = [NSString stringWithFormat:@"%@%@\n",self.couponTextList,self.productName];
		
		self.couponTextFullHTML = self.couponTextListHTML = [NSString stringWithFormat:
															 @"%@<span class=\"productName\">%@</span></BR>",
															 self.couponTextListHTML,self.productName];
		if(self.dotComURL != nil){
			self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextFull,self.productName];
		}else{
			self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextFull,self.productName];
		}
	}
	
	
	//** bullets text items
	if(self.productDescriptionBulletText != nil && productDescriptionBulletText.length > 1){
		NSArray *bulletArray = [NSArray alloc];
		bulletArray = [self.productDescriptionBulletText componentsSeparatedByString:@"\n"];
		
		self.bulletTextHTML = @"<ul class=\"b\">";
		
		for (NSString* currentString in bulletArray)
		{
			
			if([bulletArray count]> 1){
				if(self.productDescriptionBulletText == nil || ![self.productDescriptionBulletText isKindOfClass:[NSString class]] || [self.productDescriptionBulletText length] < 3){
					self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
					
				}
				else{
					self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
				}
				
			}
			else{
				self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
			}
			
		}
		self.bulletTextHTML = [NSString stringWithFormat:@"%@</ul>",bulletTextHTML];
		
		
		self.couponTextFull =  [NSString stringWithFormat:@"%@%@\n",self.couponTextFull,self.productDescriptionBulletText];
		self.couponTextFullHTML = [NSString stringWithFormat:
								   @"%@%@",
								   self.couponTextFullHTML,self.bulletTextHTML];
	}
	
	
	//** count items text
	if(self.productDescriptionCountText != nil){
		
		self.couponTextFull =  [NSString stringWithFormat:@"%@%@\n",self.couponTextFull,self.productDescriptionCountText];
		
		self.couponTextFullHTML = [NSString stringWithFormat:
								   @"%@%@</BR>",
								   self.couponTextFullHTML,self.productDescriptionCountText];
	}
	
	
	
	//** itemNumbers items text
	if(self.itemNumbers!= nil){
		
		if([self.itemNumbers rangeOfString:@","].location == NSNotFound){
			
			self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextFull,self.itemNumbers];
		}
		else{
			
			self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextFull,self.itemNumbers];
		}
		self.couponTextFullHTML = [NSString stringWithFormat:
								   @"%@%@</BR>",
								   self.couponTextFullHTML,self.itemNumbers];
	}
	
	
	//** selection varies items
	if(self.selectionVaries!= nil){
		
		//		self.selectionVaries = [ctxCouponDict valueForKeyPath:@"selectionVaries"];
		self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n",
							   self.couponTextFull,self.selectionVaries];
		
		self.couponTextFullHTML = [NSString stringWithFormat:
								   @"%@<span class=\"selectionVaries\">%@</span></BR>",
								   self.couponTextFullHTML,self.selectionVaries];
	}
	
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	
	self.couponValidText = [[NSString alloc] initWithFormat:@"%@ %@ - %@*",
							[ctxLangDict valueForKey:@"mvmValid"],
							[dateFormatter stringFromDate:self.couponStartDate],
							[dateFormatter stringFromDate:self.couponEndDate]];
	
	
	//always rebuild using the current legal text
	self.termsAndConditionsText =[ctxLangDict valueForKey:@"mvmLayoutTerms"];
	
	self.couponTextFull = [NSString stringWithFormat:@"%@%@\n\n*%@",
						   self.couponTextFull,
						   self.couponValidText,
						   self.termsAndConditionsText];
	
	NSString *imageName = [NSString stringWithFormat:@"<img src=\"%@\" alt=\"%@\" style=\"height:250px;border:1px solid #fff; float: right; margin:1px;\">", self.imageFilenameMobile,self.couponTextFull];
	
	
	
	self.couponTextFullHTML = [NSString stringWithFormat:@"%@,%@<span class=\"valid\">%@</span></BR></P></BR></BR>",
							   imageName, self.couponTextFullHTML,self.couponValidText];
	
	
	//** final output
	self.couponTextList = [NSString stringWithFormat:@"%@",self.couponTextList];

	if(showDotComLink == true){
		
		self.couponTextFull = [NSString stringWithFormat:@"\n%@\n",self.couponTextFull];
		
	}else{
		self.couponTextFull = [NSString stringWithFormat:@"%@\n",self.couponTextFull];
	}
	
	
	int textLen = (int)[self.couponTextList length];
	int length = 145;
	
//	if(self.showDotComLink == true){
		length += 20;
//	}
	
	if (textLen > 120){
		length += textLen*.24;
	}
	self.cellHeight = length;
	
}




-(void) prerenderAttributedTextForCouponStyle:(int)CouponStyle{
	
	__block NSString* productNameWithDetails;
	int instantSavingsFontSize;
	int titleFontSize;
	int	dollarFontSize;
	int	dollarSignFontSize;
	int baseFontSize;
	int termsFontSize;

	
	if(CouponStyle == legalList){
		
		NSFont *baseFontArrow = [NSFont fontWithName:@"HelveticaNeue" size:13];
		NSFont *baseFont = [NSFont fontWithName:@"HelveticaNeue" size:13];
		NSMutableParagraphStyle * paragraphStyleBase = [[NSMutableParagraphStyle alloc] init];
		paragraphStyleBase.alignment = kCTTextAlignmentLeft;
		paragraphStyleBase.minimumLineHeight = baseFont.pointSize-8 * 2;
		paragraphStyleBase.maximumLineHeight = baseFont.pointSize-8 *2 ;
		paragraphStyleBase.lineSpacing = 0;
		
		NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:self.termsAndConditionsText];
		[title addAttribute:NSFontAttributeName value:baseFont range:[self.termsAndConditionsText rangeOfString:self.termsAndConditionsText]];
		[title addAttribute:NSFontAttributeName value:baseFontArrow range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		
		[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBase range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		[title addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		self.couponTextListAttributed = self.couponTextFullAttributed =  title;
		
	}else{
	
		if(CouponStyle == listView){
			productNameWithDetails = self.couponTextList;
			instantSavingsFontSize = 14;
			titleFontSize = 18;
			dollarFontSize =30;
			dollarSignFontSize = 16;
			baseFontSize = 16;
			
			//place bug icons at the bottom of the text
			self.iconInsertIndex = self.couponTextList.length;

		}

		if(CouponStyle == largeView){
			//put bug icons at the top of the text
			self.iconInsertIndex = 0;
			float scale = 1.5;
			productNameWithDetails = self.couponTextFull;
			instantSavingsFontSize = 15*scale;
			titleFontSize = 14*scale;
			dollarFontSize =30*scale;
			dollarSignFontSize = 12*scale;
			baseFontSize = 12*scale;
			termsFontSize = 12;
		}
		
		
		
		NSFont *instantSavingsFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:instantSavingsFontSize];
		NSFont *titleFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:titleFontSize];
		NSFont *dollarFont = [NSFont fontWithName:@"HelveticaNeue-Bold" size:dollarFontSize];
		NSFont *dollarSignFont = [NSFont fontWithName:@"HelveticaNeue-Bold" size:dollarSignFontSize];
		NSFont *offFont = [NSFont fontWithName:@"HelveticaNeue" size:dollarFontSize];
		NSFont *baseFont = [NSFont fontWithName:@"HelveticaNeue" size:baseFontSize];
		NSFont *selectionVariesFont = [NSFont fontWithName:@"HelveticaNeue-Italic" size:baseFontSize];
		NSFont *TandCFont = [NSFont fontWithName:@"HelveticaNeue" size:termsFontSize];
		
		NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:productNameWithDetails];

		[title addAttribute:NSFontAttributeName value:baseFont range:[productNameWithDetails rangeOfString:productNameWithDetails]];
		[title addAttribute:NSFontAttributeName value:TandCFont range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
		[title addAttribute:NSFontAttributeName value:titleFont range:[productNameWithDetails rangeOfString:productName]];
		[title addAttribute:NSKernAttributeName
								 value:@(-.5)
								 range:[productNameWithDetails rangeOfString:productName]];


		if(itemLimit != nil){

			if(itemLimit.length < 10){

				
			[title addAttribute:NSKernAttributeName
						  value:@(-.5)
						  range:[productNameWithDetails rangeOfString:itemLimit]];
			}
			else
			{
				[title addAttribute:NSKernAttributeName
							  value:@(-1)
							  range:[productNameWithDetails rangeOfString:itemLimit]];

		
			}
		}
		
		
		if(countSourceText != nil){
		[title addAttribute:NSKernAttributeName
					  value:@(-.5)
					  range:[productNameWithDetails rangeOfString:countSourceText]];
		}
		if(productDescriptionBulletText != nil){
			[title addAttribute:NSKernAttributeName
						  value:@(-.5)
						  range:[productNameWithDetails rangeOfString:productDescriptionBulletText]];
		}
		
		
		[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.dollarSign]];
		
		if(self.cents != nil){
			[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.cents]];
		}
		
		[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.itemLimit]];
		
		if(dollar != nil){
			[title addAttribute:NSFontAttributeName value:dollarFont range:[productNameWithDetails rangeOfString:self.dollar]];
		}
		
			[title addAttribute:NSFontAttributeName value:offFont range:[productNameWithDetails rangeOfString:self.off]];
			[title addAttribute:NSFontAttributeName value:instantSavingsFont range:[productNameWithDetails rangeOfString:self.instantSavings]];
		
		NSString* superScriptType = @"1";
		if(CouponStyle == largeView)
			superScriptType = @"2";
		
		if(self.selectionVaries != nil)
			[title addAttribute:NSFontAttributeName value:selectionVariesFont range:[productNameWithDetails rangeOfString:self.selectionVaries]];
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.dollarSign]];
		
		if(cents != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.cents]];
		
		if(itemLimit != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.itemLimit]];
		
		if(itemPrefix != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.itemPrefix]];
			
		
		//TODO: add indent attribute to bullet section!!
		//TODO: add red coloring to selection varies text
		
		
		NSMutableParagraphStyle * paragraphStyleBullets = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleBullets.headIndent = -1;
		
		NSShadow * myShadow = [[NSShadow alloc] init];
			myShadow.shadowColor = [NSColor lightGrayColor];
			myShadow.shadowOffset = CGSizeMake(1,1);
			myShadow.shadowBlurRadius = 4.0;
		
		NSShadow * whiteShadow = [[NSShadow alloc] init];
			whiteShadow.shadowColor = [NSColor whiteColor];
			whiteShadow.shadowOffset = CGSizeMake(2,2);
			whiteShadow.shadowBlurRadius = 1.0;
		
		NSMutableParagraphStyle * paragraphStyleBase = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleBase.alignment = kCTTextAlignmentLeft;
			paragraphStyleBase.minimumLineHeight = baseFont.pointSize-2;
			paragraphStyleBase.maximumLineHeight = baseFont.pointSize-2;
			paragraphStyleBase.lineSpacing = 0;
		
		NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
			paragraphStyle.alignment = kCTTextAlignmentLeft;
			paragraphStyle.minimumLineHeight = dollarFont.pointSize-2;
			paragraphStyle.maximumLineHeight = dollarFont.pointSize-2;
			paragraphStyle.lineSpacing = 0;
		
		NSMutableParagraphStyle * paragraphStyleTitle = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleTitle.alignment = kCTTextAlignmentLeft;
			paragraphStyleTitle.minimumLineHeight = titleFont.pointSize-1;
			paragraphStyleTitle.maximumLineHeight = titleFont.pointSize-1;
			paragraphStyleTitle.lineSpacing = 0;

		NSMutableParagraphStyle * zzz = [[NSMutableParagraphStyle alloc] init];
		zzz.alignment = kCTTextAlignmentRight;
		zzz.minimumLineHeight = -10;
		zzz.maximumLineHeight = -10;
		zzz.lineSpacing = -10;
		
		
		
		[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[title.string rangeOfString:self.termsAndConditionsText]];

		
	//	[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBase range:[productNameWithDetails rangeOfString:productNameWithDetails]];
		
		if(self.productDescriptionBulletText != nil){
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBullets range:[productNameWithDetails rangeOfString:self.productDescriptionBulletText]];}
		
		
	//		[title addAttribute:NSShadowAttributeName value:whiteShadow range:[productNameWithDetails rangeOfString:self.instantSavings]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.instantSavings]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleTitle range:[productNameWithDetails rangeOfString:productName]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.dollarSign]];

			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.dollar]];
		
			if(self.cents != nil)
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.cents]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.off]];
			[title addAttribute:NSBackgroundColorAttributeName value:[NSColor clearColor] range:[productNameWithDetails rangeOfString:productNameWithDetails]];



		
		if(self.selectionVaries != nil)
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.selectionVaries]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:dollar]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.dollarSign]];
		if(self.itemPrefix != nil)
		[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.itemPrefix]];

		if(self.cents != nil)
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.cents]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.off]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.itemLimit]];
			
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.instantSavings]];
			
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleAndOr]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleOr]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleChooseFrom]];

		if(CouponStyle == listView){
			
			if(showUpTo == true){
				NSTextAttachment* textAttachmenta = [NSTextAttachment new];;
				NSTextAttachmentCell* myCella = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"upTo.png"]];
				[textAttachmenta setAttachmentCell:myCella];
				[myCella.image setSize:CGSizeMake(18,20)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachmenta] atIndex:self.instantSavings.length +1];
			}
			
			
			if(self.showDotComLink == true){
				NSTextAttachment* textAttachment = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-available-online-small@2x.png"]];
				[textAttachment setAttachmentCell:imageCell];
				[imageCell.image setSize:CGSizeMake(80,35)];
				[title appendAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
				
			}
			if(self.showBookAppRequired == true){
				NSTextAttachment* textAttachment2 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell2 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-book-app-required-small@2x.png"]];
				[textAttachment2 setAttachmentCell:imageCell2];
				[imageCell2.image setSize:CGSizeMake(80,35)];
				[title appendAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment2]];
			}
			self.couponTextListAttributed = [[NSMutableAttributedString alloc]initWithAttributedString:title];
			}
		else{
			
			
			
			
			if(self.termsAndConditionsText != nil){
				[title addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
	//			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleTerms range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
			}
			
			if(showUpTo == true){
				NSTextAttachment* textAttachmenta = [NSTextAttachment new];;
				NSTextAttachmentCell* myCella = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"upTo@2x.png"]];
				[textAttachmenta setAttachmentCell:myCella];
				[myCella.image setSize:CGSizeMake(28,32)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachmenta] atIndex:self.instantSavings.length +2];
			}
			
			if(self.showBookAppRequired == true){
				NSTextAttachment* textAttachment4 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell4 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-book-app-required@2x.png"]];
				[textAttachment4 setAttachmentCell:imageCell4];
				[imageCell4.image setSize:CGSizeMake(100,44)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment4] atIndex:0];
			}
			
			if(self.showDotComLink == true){
				NSTextAttachment* textAttachment3 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell3 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-available-online@2x.png"]];
				[textAttachment3 setAttachmentCell:imageCell3];
				[imageCell3.image setSize:CGSizeMake(100,44)];

				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment3] atIndex:0];
			}
			
			self.couponTextFullAttributed = [[NSMutableAttributedString alloc]initWithAttributedString:title];
		}
		
		
	}
		
}







@end
