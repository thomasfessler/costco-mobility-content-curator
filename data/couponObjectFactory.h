//
//  couponObject.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/12/14.
//
//

#import <Foundation/Foundation.h>
//#import "CWCAppManager.h"
#import "couponBookObject.h"
#import "CWCAppDefines.h"

@interface couponObjectFactory : NSObject


/*Pre rendered text blocks - plain text, HTML and NSAttributed versions */


@property NSMutableArray *errorArray;

/*coupon list view text blocks   */



@property NSString *couponTextList;

@property NSString *couponTextListHTML;
@property NSString *couponTextListHTMLSimple;

@property NSAttributedString *couponTextListAttributed;

/*complete full screen text block  */
@property NSString *couponTextDetail;

@property NSString *couponTextDetailHeader;
@property NSString *couponTextDetailBody;

@property NSString *couponTextDetailHTML;

@property NSString *couponTextDetailHTMLHeader;
@property NSString *couponTextDetailHTMLBody;

@property NSAttributedString *couponTextDetailAttributed;
@property NSString *couponBannerText;

@property NSString *productCategory;
@property NSString *department;
@property NSString *departmentNameLocalized;
@property NSString *sortOrder;
@property long couponNumber;
@property NSString *couponID;

/*The type of coupon cell*/
@property couponLayout	couponType;

//text elements - used to apply styles to pre-rendered text block
@property NSString *instantSavings;
@property NSString *upTo;
@property NSString *dollarSign;
@property NSString *off;
@property NSString *currencyDelimiter;
@property NSString *dollar;
@property NSString *cents;

@property NSString *productName;
@property NSString *itemPrefix;
@property NSString *itemLimit;

@property NSString *itemNumbers;
@property NSString *selectionVaries;
@property NSString *couponValidText;

@property NSString *productDescriptionCountText;
@property NSString *productDescriptionBulletText;
@property NSString *termsAndConditionsText;
@property NSString *couponCSS;



@property bool approvedDetail;
@property bool approvedList;
@property bool approvedWeb;
@property bool approvedFinal;

//cell height overrides
@property int cellHeight;

//display elements
@property BOOL	   showDotComLink;
@property BOOL	   showBookAppRequired;
@property BOOL	   showUpTo;
@property BOOL	   showCouponBanner;
@property BOOL	   showCouponBannerOverrride;
@property BOOL	   errorState;


//filenames and paths
@property NSString *imageFilenameMobile;
@property NSString *imageFilenameDesktop;
@property NSString *dotComURL;
@property NSString *imageURL;
@property NSDate *couponEndDate;
@property NSDate *couponStartDate;

//Overrides text elements color to red
@property NSString *styleAndOr;
@property NSString *styleOr;
@property NSString *styleChooseFrom;
@property NSString *styleTermsAndConditionsTitle;


//unused
@property NSString *comBug;
@property NSString *xtraPricingInfo;
@property int		iconInsertIndex;
@property NSString *bulletsSourceData;
@property NSString *bulletTextHTML;
@property NSString *item;
@property NSString *items;
@property NSString *countSourceText;


/*   */

- (void) checkCouponForErrorsWithLocalizationDict:(NSDictionary*) ctxLangDict;

- (id) initWithCouponJSONDictionary:(NSDictionary*)ctxCouponDict withCouponCoverObject:(couponBookObject*)ctxCoverObject withSortDictionary:(NSDictionary*)ctxSort andLanguageDictionary:(NSDictionary*)ctxLangDict;
	- (void) buildCouponTextWithLanguageDictionary:(NSDictionary*)ctxLangDict andCoverObject:(couponBookObject*)ctxCoverObject;

- (NSMutableDictionary*)encodeForExport;

-(void) prerenderAttributedTextForCouponStyle:(int)CouponStyle;


@end
