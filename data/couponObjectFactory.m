//
//  couponObject.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/12/14.
//
//

#import "couponObjectFactory.h"

@implementation couponObjectFactory
@synthesize bulletsSourceData,productDescriptionBulletText, bulletTextHTML,cents,comBug,countSourceText,dollar,dollarSign,dotComURL, items;

@synthesize currencyDelimiter,imageFilenameMobile,instantSavings,itemLimit,itemNumbers,itemPrefix,couponTextDetail, couponTextDetailAttributed,off,productName,selectionVaries,item, iconInsertIndex;

@synthesize	couponTextList,couponTextListAttributed ,xtraPricingInfo,styleAndOr,styleChooseFrom, styleOr, upTo, imageURL, productDescriptionCountText, cellHeight, showDotComLink, showBookAppRequired, showUpTo, couponValidText, couponEndDate, couponStartDate,styleTermsAndConditionsTitle, couponTextDetailHTML, couponTextListHTML;

@synthesize errorState, approvedDetail,approvedList,approvedWeb,couponType,errorArray,imageFilenameDesktop,termsAndConditionsText, approvedFinal;

@synthesize couponTextDetailBody,couponTextDetailHeader,couponTextDetailHTMLBody,couponTextDetailHTMLHeader,couponTextListHTMLSimple, couponCSS, showCouponBanner, couponBannerText, showCouponBannerOverrride, productCategory, department, sortOrder, departmentNameLocalized, couponID, couponNumber;


- (id) init
{
    self = [super init];
    if (self) {
        //do setup here
		
	}
    return self;
}

/*
init object from an encoded NSDATA object
 */


- (id)initWithCoder:(NSCoder *)decoder {
	if (self = [super init]) {


		
		
		self.couponNumber = [[decoder decodeObjectForKey:@"couponNumber"]  longValue];
		
		self.departmentNameLocalized = [decoder decodeObjectForKey:@"departmentNameLocalized"];
		self.sortOrder = [decoder decodeObjectForKey:@"sortOrder"];
		
		self.bulletsSourceData = [decoder decodeObjectForKey:@"bullets"];
		self.bulletTextHTML = [decoder decodeObjectForKey:@"bulletTextHTML"];
		self.productDescriptionBulletText = [decoder decodeObjectForKey:@"bulletTextLarge"];
		self.cellHeight = [[decoder decodeObjectForKey:@"cellHeight"]  integerValue];
		self.cents = [decoder decodeObjectForKey:@"cents"];
		self.countSourceText = [decoder decodeObjectForKey:@"count"];
		self.productDescriptionCountText = [decoder decodeObjectForKey:@"countLarge"];
		
		self.couponTextDetail = [decoder decodeObjectForKey:@"couponTextFull"];
		self.couponTextDetailAttributed = [decoder decodeObjectForKey:@"couponTextFullAttributed"];

		self.couponTextList = [decoder decodeObjectForKey:@"couponTextList"];
		self.couponTextListAttributed = [decoder decodeObjectForKey:@"couponTextListAttributed"];

		self.couponTextListHTML = [decoder decodeObjectForKey:@"couponTextListHTML"];
		self.couponTextDetailHTML = [decoder decodeObjectForKey:@"couponTextFullHTML"];
		
		self.couponTextDetailBody = [decoder decodeObjectForKey:@"couponTextDetailBody"];
		self.couponTextDetailHeader = [decoder decodeObjectForKey:@"couponTextDetailHeader"];
		self.couponTextDetailHTMLBody = [decoder decodeObjectForKey:@"couponTextDetailHTMLBody"];
		self.couponTextDetailHTMLHeader = [decoder decodeObjectForKey:@"couponTextDetailHTMLHeader"];
		self.couponTextListHTMLSimple = [decoder decodeObjectForKey:@"couponTextListHTMLSimple"];
		
		self.department = [decoder decodeObjectForKey:@"department"];
		self.productCategory = [decoder decodeObjectForKey:@"productCategory"];


		self.couponType = [[decoder decodeObjectForKey:@"couponType"] integerValue];
		self.dollar = [decoder decodeObjectForKey:@"dollar"];
		self.dollarSign = [decoder decodeObjectForKey:@"dollarSign"];
		self.dotComURL = [decoder decodeObjectForKey:@"dotComURL"];
		self.item = [decoder decodeObjectForKey:@"item"];
		self.itemLimit = [decoder decodeObjectForKey:@"itemLimit"];
		self.itemNumbers = [decoder decodeObjectForKey:@"itemNumbers"];
		self.itemPrefix = [decoder decodeObjectForKey:@"itemPrefix"];
		self.items = [decoder decodeObjectForKey:@"items"];

		self.instantSavings = [decoder decodeObjectForKey:@"instantSavings"];
		self.currencyDelimiter = [decoder decodeObjectForKey:@"currencyDelimiter"];
		self.comBug = [decoder decodeObjectForKey:@"comBug"];
		self.iconInsertIndex = [[decoder decodeObjectForKey:@"insertIndex"] integerValue];

		self.imageFilenameMobile = [decoder decodeObjectForKey:@"imageFilename"];
		self.imageURL = [decoder decodeObjectForKey:@"imageURL"];
		
		self.off = [decoder decodeObjectForKey:@"off"];
		self.productName = [decoder decodeObjectForKey:@"productName"];
		self.selectionVaries = [decoder decodeObjectForKey:@"selectionVaries"];
		self.showBookAppRequired = [[decoder decodeObjectForKey:@"showBookAppRequired"] boolValue];
		self.showDotComLink = [[decoder decodeObjectForKey:@"showDotComLink"] boolValue];
		
		self.termsAndConditionsText = [decoder decodeObjectForKey:@"termsAndConditionsText"];
		self.errorState = [[decoder decodeObjectForKey:@"errorState"] boolValue];
		self.showUpTo = [[decoder decodeObjectForKey:@"showUpTo"] boolValue];
		self.styleAndOr = [decoder decodeObjectForKey:@"styleAndOr"];
		self.styleChooseFrom = [decoder decodeObjectForKey:@"styleChooseFrom"];
		self.styleOr = [decoder decodeObjectForKey:@"styleOr"];
		self.upTo = [decoder decodeObjectForKey:@"upTo"];
		self.xtraPricingInfo = [decoder decodeObjectForKey:@"xtraPricingInfo"];

		self.couponValidText = [decoder decodeObjectForKey:@"couponValidText"];
		self.couponStartDate = [decoder decodeObjectForKey:@"couponStartDate"];
		self.couponEndDate = [decoder decodeObjectForKey:@"couponEndDate"];
		self.styleTermsAndConditionsTitle = [decoder decodeObjectForKey:@"termsAndConditionsTitle"];
		
		
		self.approvedDetail = [[decoder decodeObjectForKey:@"approvedDetail"] boolValue];
		self.approvedList = [[decoder decodeObjectForKey:@"approvedList"] boolValue];
		self.approvedWeb = [[decoder decodeObjectForKey:@"approvedWeb"] boolValue];
		self.approvedFinal = [[decoder decodeObjectForKey:@"approvedFinal"] boolValue];
		
		self.errorArray = [decoder decodeObjectForKey:@"errorArray"];
		self.imageFilenameDesktop = [decoder decodeObjectForKey:@"imageFilenameDesktop"];
		self.imageFilenameMobile = [decoder decodeObjectForKey:@"imageFilenameMobile"];
		self.termsAndConditionsText = [decoder decodeObjectForKey:@"termsAndConditionsText"];
	}
	return self;
}




/*
 save object to a saved NSDATA object in encoded format
 */


- (void)encodeWithCoder:(NSCoder *)encoder {

	
	[encoder encodeObject:[NSNumber numberWithLong:self.couponNumber] forKey:@"couponNumber"];
	
	[encoder encodeObject:self.departmentNameLocalized forKey:@"departmentNameLocalized"];
	[encoder encodeObject:self.sortOrder forKey:@"sortOrder"];
	
	[encoder encodeObject:self.bulletsSourceData forKey:@"bullets"];
	[encoder encodeObject:self.productDescriptionBulletText forKey:@"bulletTextLarge"];
	[encoder encodeObject:self.bulletTextHTML forKey:@"bulletTextHTML"];
	[encoder encodeObject:[NSNumber numberWithInt:self.cellHeight] forKey:@"cellHeight"];
	[encoder encodeObject:self.cents forKey:@"cents"];
	[encoder encodeObject:self.countSourceText forKey:@"count"];
	[encoder encodeObject:self.productDescriptionCountText forKey:@"countLarge"];
	[encoder encodeObject:self.couponTextDetail forKey:@"couponTextFull"];
	[encoder encodeObject:self.couponTextDetailAttributed forKey:@"couponTextFullAttributed"];
	[encoder encodeObject:self.couponTextList forKey:@"couponTextList"];
	[encoder encodeObject:self.couponTextListAttributed forKey:@"couponTextListAttributed"];

	[encoder encodeObject:[NSNumber numberWithInteger:self.department] forKey:@"department"];
	
	[encoder encodeObject:self.couponTextDetailHTML forKey:@"couponTextFullHTML"];
	[encoder encodeObject:self.couponTextListHTML forKey:@"couponTextListHTML"];
	
	[encoder encodeObject:self.couponTextDetailBody forKey:@"couponTextDetailBody"];
	[encoder encodeObject:self.couponTextDetailHeader forKey:@"couponTextDetailHeader"];
	[encoder encodeObject:self.couponTextDetailHTMLBody forKey:@"couponTextDetailHTMLBody"];
	[encoder encodeObject:self.couponTextDetailHTMLHeader forKey:@"couponTextDetailHTMLHeader"];
	[encoder encodeObject:self.couponTextListHTMLSimple forKey:@"couponTextListHTMLSimple"];
		
	[encoder encodeObject:[NSNumber numberWithInt:self.couponType] forKey:@"couponType"];
	[encoder encodeObject:self.dollar forKey:@"dollar"];
	[encoder encodeObject:self.dollarSign forKey:@"dollarSign"];
	[encoder encodeObject:self.dotComURL forKey:@"dotComURL"];
	
	[encoder encodeObject:self.instantSavings forKey:@"instantSavings"];
	[encoder encodeObject:self.currencyDelimiter forKey:@"currencyDelimiter"];
	[encoder encodeObject:self.comBug forKey:@"comBug"];
	[encoder encodeObject:[NSNumber numberWithInt:self.iconInsertIndex] forKey:@"insertIndex"];
	
	[encoder encodeObject:self.item forKey:@"item"];
	[encoder encodeObject:self.itemLimit forKey:@"itemLimit"];
	[encoder encodeObject:self.itemNumbers forKey:@"itemNumbers"];
	[encoder encodeObject:self.itemPrefix forKey:@"itemPrefix"];
	[encoder encodeObject:self.items forKey:@"items"];
	
	[encoder encodeObject:self.imageFilenameMobile forKey:@"imageFilename"];
	[encoder encodeObject:self.imageURL forKey:@"imageURL"];

	[encoder encodeObject:self.off forKey:@"off"];
	[encoder encodeObject:self.productName forKey:@"productName"];
	[encoder encodeObject:self.selectionVaries forKey:@"selectionVaries"];
	[encoder encodeObject:[NSNumber numberWithBool:self.showBookAppRequired] forKey:@"showBookAppRequired"];
	[encoder encodeObject:[NSNumber numberWithBool:self.showDotComLink] forKey:@"showDotComLink"];
	
	[encoder encodeObject:self.productCategory forKey:@"productCategory"];
	
	[encoder encodeObject:[NSNumber numberWithBool:self.showUpTo] forKey:@"showUpTo"];
	[encoder encodeObject:self.styleAndOr forKey:@"styleAndOr"];
	[encoder encodeObject:self.styleChooseFrom forKey:@"styleChooseFrom"];
	[encoder encodeObject:self.styleOr forKey:@"styleOr"];
	[encoder encodeObject:self.upTo forKey:@"upTo"];
	[encoder encodeObject:self.xtraPricingInfo forKey:@"xtraPricingInfo"];
	[encoder encodeObject:self.couponValidText forKey:@"couponValidText"];
	[encoder encodeObject:self.couponStartDate forKey:@"couponStartDate"];
	[encoder encodeObject:self.couponEndDate forKey:@"couponEndDate"];
	[encoder encodeObject:self.styleTermsAndConditionsTitle forKey:@"termsAndConditionsTitle"];


	
	[encoder encodeObject:[NSNumber numberWithBool:self.errorState] forKey:@"errorState"];
	[encoder encodeObject:[NSNumber numberWithBool:self.approvedDetail] forKey:@"approvedDetail"];
	[encoder encodeObject:[NSNumber numberWithBool:self.approvedList] forKey:@"approvedList"];
	[encoder encodeObject:[NSNumber numberWithBool:self.approvedWeb] forKey:@"approvedWeb"];
	[encoder encodeObject:[NSNumber numberWithBool:self.approvedFinal] forKey:@"approvedFinal"];
	
	
	[encoder encodeObject:self.errorArray forKey:@"errorArray"];
	[encoder encodeObject:self.imageFilenameDesktop forKey:@"imageFilenameDesktop"];
	[encoder encodeObject:self.imageFilenameMobile forKey:@"imageFilenameMobile"];
	[encoder encodeObject:self.termsAndConditionsText forKey:@"termsAndConditionsText"];
	
		
}



/*
 encodes the object into a NSDictionary for export
 generic - needs switches for types of output
 */


- (NSMutableDictionary*)encodeForExport{
	
	NSMutableDictionary *encoder = [[NSMutableDictionary alloc] init];
	
	[encoder setValue:[NSNumber numberWithInteger:self.departmentNameLocalized] forKey:@"departmentNameLocalized"];
	[encoder setValue:self.department forKey:@"department"];
	[encoder setValue:self.productCategory forKey:@"productCategory"];
	[encoder setValue: self.sortOrder forKey:@"sortOrder"];
	
	[encoder setValue: self.couponID forKey:@"couponID"];
	
	
	[encoder setValue:self.couponTextDetail forKey:@"couponADADetail"];
	[encoder setValue:self.couponTextList forKey:@"couponADAList"];
	[encoder setValue:self.couponTextDetailHTMLHeader forKey:@"couponDetailHTMLHeader"];
	[encoder setValue:self.couponTextDetailHTMLBody forKey:@"couponDetailHTMLBody"];
	[encoder setValue:self.couponTextListHTML forKey:@"couponListHTML"];
	[encoder setValue:self.couponTextListHTMLSimple forKey:@"couponListHTMLSimple"];
	
//	[encoder setValue:self.couponTextListAttributed forKey:@"couponTextListAttributed"];
	[encoder setValue:@"dynamicTextLayout1" forKey:@"couponType"];
	[encoder setValue:self.couponBannerText forKey:@"couponTimeframeMessage"];

	
	
//	[encoder setValue:self.instantSavings forKey:@"instantSavings"];
	
//	[encoder setValue:self.upTo forKey:@"upTo"];
//	[encoder setValue:self.dollarSign forKey:@"dollarSign"];
//	[encoder setValue:self.dollar forKey:@"dollar"];
//	[encoder setValue:self.off forKey:@"off"];
//	[encoder setValue:self.currencyDelimiter forKey:@"currencyDelimiter"];
//	[encoder setValue:self.comBug forKey:@"comBug"];
//	[encoder setValue:[NSNumber numberWithInt:self.iconInsertIndex] forKey:@"insertIndex"];
	
//	[encoder setValue:self.item forKey:@"item"];
//	[encoder setValue:self.itemPrefix forKey:@"itemPrefix"];
//	[encoder setValue:self.itemLimit forKey:@"itemLimit"];

	[encoder setValue:self.productName forKey:@"productName"];

//	[encoder setValue:self.itemNumbers forKey:@"itemNumbers"];

//	[encoder setValue:self.selectionVaries forKey:@"selectionVaries"];
	
	[encoder setValue:self.dotComURL forKey:@"couponProductURL"];

//	[encoder setValue:self.couponValidText forKey:@"couponValidText"];
//	[encoder setValue:self.termsAndConditionsText forKey:@"termsAndConditionsText"];

	
	
	NSDictionary *image = [[NSDictionary alloc] initWithObjectsAndKeys:self.imageFilenameMobile,@"large", nil];
	
	[encoder setValue:image forKey:@"imageFilenameMobile"];
	
	
//	[encoder setValue:self.imageFilenameDesktop forKey:@"imageFilenameDesktop"];
//	[encoder setValue:self.imageURL forKey:@"imageURL"];
	
//	[encoder setValue:[NSNumber numberWithBool:self.errorState] forKey:@"errorState"];
	[encoder setValue:[NSNumber numberWithBool:self.showCouponBanner] forKey:@"showCouponBanner"];
	[encoder setValue:[NSNumber numberWithBool:self.showBookAppRequired] forKey:@"showRequiresCoupon"];
	[encoder setValue:[NSNumber numberWithBool:self.showDotComLink] forKey:@"showAvailableOnline"];
	[encoder setValue:[NSNumber numberWithBool:self.showUpTo] forKey:@"showUpTo"];
	
//	[encoder setValue:self.styleAndOr forKey:@"styleAndOr"];
//	[encoder setValue:self.styleChooseFrom forKey:@"styleChooseFrom"];
//	[encoder setValue:self.styleOr forKey:@"styleOr"];
//	[encoder setValue:self.styleTermsAndConditionsTitle forKey:@"styleTermsAndConditionsTitle"];
//	[encoder setValue:self.xtraPricingInfo forKey:@"xtraPricingInfo"];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];

	[encoder setValue:[dateFormatter stringFromDate:self.couponStartDate] forKey:@"couponStartDate"];
	[encoder setValue:[dateFormatter stringFromDate:self.couponEndDate] forKey:@"couponEndDate"];
	
//	[encoder setValue:self.termsAndConditionsText forKey:@"termsAndConditionsText"];
	
	return encoder;
}

/****  Set up coupon object
		import text
		pre-render buid text strings
			pre-render styled text for list view
			pre-render style text for large view
 */

- (id) initWithCouponJSONDictionary:(NSDictionary*)ctxCouponDict withCouponCoverObject:(couponBookObject*)ctxCoverObject withSortDictionary:(NSDictionary*)ctxSort andLanguageDictionary:(NSDictionary*)ctxLangDict{
    self = [super init];
	[self importCouponObjectJSONDict:ctxCouponDict withCouponCoverObject:ctxCoverObject withDepartmentSortDict:ctxSort andLanguageDictionary:ctxLangDict];
	[self buildCouponTextWithLanguageDictionary:ctxLangDict andCoverObject:ctxCoverObject];
	[self checkCouponForErrorsWithLocalizationDict: ctxLangDict];
	
	[self prerenderAttributedTextForCouponStyle:listView];
	[self prerenderAttributedTextForCouponStyle:largeView];
    return self;
}


/*  Imports the JSON payload object into the coupon objct  */
-(void) importCouponObjectJSONDict:(NSDictionary*)ctxCouponDict withCouponCoverObject:(couponBookObject*)ctxCoverObject  withDepartmentSortDict:(NSDictionary*)ctxDepartment andLanguageDictionary:(NSDictionary*)ctxLangDict{
	
	if([ctxCouponDict valueForKeyPath:@"couponType"] == nil){
		self.couponType = 1;
	}
	self.department = [[NSString alloc] initWithString:[[ctxCouponDict valueForKey:@"department"] stringValue]];
	self.departmentNameLocalized =  [[ctxDepartment objectForKey:department] valueForKey:@"description"];
	self.sortOrder = [[ctxDepartment objectForKey:department] valueForKey:@"sort"] ;
	self.showCouponBannerOverrride = false;
	self.couponCSS = [ctxLangDict valueForKey:@"css"];
	self.approvedDetail = false;
	self.approvedList = false;
	self.approvedWeb = false;
	self.approvedFinal = false;
	
	self.couponNumber = [[ctxCouponDict valueForKey:@"coupon"] longValue];
		
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	
	[df setDateFormat:@"MMMyy"];
	self.couponID = [NSString stringWithFormat:@"%@_%ld",[df stringFromDate:[NSDate date]],couponNumber];
	
	
	self.instantSavings = [ctxLangDict valueForKey:@"mvmInstantSavings"];
	self.off = [ctxLangDict valueForKey:@"mvmOff"];
	self.dollarSign = [ctxLangDict valueForKey:@"mvmDollarSign"];
	self.currencyDelimiter = [ctxLangDict valueForKey:@"mcmCurrencyDelimiter"];
	self.styleAndOr = [ctxLangDict valueForKey:@"mvmStyleAndOr"];
	self.styleChooseFrom = [ctxLangDict valueForKey:@"mvmStyleChooseFrom"];
	self.styleOr = [ctxLangDict valueForKey:@"mvmStyleOr"];
	self.item = [ctxLangDict valueForKey:@"mvmItem"];
	self.items = [ctxLangDict valueForKey:@"mvmItems"];
	self.styleTermsAndConditionsTitle = [ctxLangDict valueForKey:@"mvmLayoutTermsTitle"];
	self.termsAndConditionsText =[ctxLangDict valueForKey:@"mvmLayoutTerms"];
	
	self.couponEndDate = ctxCoverObject.couponBookEndDate;
	self.couponStartDate = ctxCoverObject.couponBookStartDate;
	
	self.couponValidText = ctxCoverObject.couponBookValidText;
	
	if([ctxCouponDict valueForKey:@"xtraPricingInfo"] != nil){
		
		NSString* localValue = [[ctxCouponDict valueForKey:@"xtraPricingInfo"] uppercaseString];
		
		if([localValue hasPrefix:@"UP"]){
			self.showUpTo = true;
		}
		
		if([localValue hasPrefix:@"IRC"]){
			self.showBookAppRequired = TRUE;
		}
		else{
			self.showBookAppRequired = false;
		}
	}
	
	if ([ctxCouponDict valueForKey:@"comBug"] != nil){
		self.comBug = [ctxCouponDict valueForKey:@"comBug"];
		self.comBug = [self.comBug uppercaseString];
		
		if([self.comBug hasPrefix:@"YES"])
		{
			self.showDotComLink = true;
			
		}
		else{
			self.showDotComLink = false;
		}
	}
	
	
	if ([ctxCouponDict valueForKey:@"xtraPricingInfo"] != nil)
		self.xtraPricingInfo = [ctxCouponDict valueForKey:@"xtraPricingInfo"];
	if ([ctxCouponDict valueForKey:@"couponimage"] != nil)
		self.imageFilenameMobile = [ctxCouponDict valueForKey:@"couponimage"];
	
	if ([ctxCouponDict valueForKey:@"imageurl"] != nil)
		self.imageURL = [ctxCouponDict valueForKey:@"imageurl"];
	
	if ([ctxCouponDict valueForKey:@"dotcomurl"] != nil)
		self.dotComURL = [ctxCouponDict valueForKey:@"dotcomurl"];
	
	//**instantSavings
	self.couponTextList = [NSString stringWithFormat:@"%@\n",self.instantSavings];
	
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"<P><span class=\"instantSavings\">%@</span></BR>",
							   self.instantSavings];
	
	
	//**dollar
	if([ctxCouponDict valueForKeyPath:@"dollar"]!= nil){
		self.dollar = [[ctxCouponDict valueForKey:@"dollar"] stringValue];
	}
	
	//**cents
	if([ctxCouponDict valueForKeyPath:@"cents"]!= nil){
		self.cents = [[ctxCouponDict valueForKey:@"cents"] stringValue];
	}
	
	
	if ([ctxCouponDict valueForKey:@"itemPrefix"] != nil)
		self.itemPrefix = [ctxCouponDict valueForKey:@"itemPrefix"];
	
	//** itemLimit
	if([ctxCouponDict valueForKeyPath:@"itemLimit"]!= nil){
		self.itemLimit = [ctxCouponDict valueForKeyPath:@"itemLimit"];
		self.itemLimit = [self.itemLimit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		self.itemLimit = [self.itemLimit stringByReplacingOccurrencesOfString:@"." withString:@""];
		self.itemLimit = [itemLimit uppercaseString];
		
		if(self.itemLimit.length >4){
					self.itemLimit = [NSString stringWithFormat:@"%@",self.itemLimit];
		}
		
	}
	
	//Start the varation for large layout
	self.couponTextDetail = self.couponTextList;
	
	//** mainDescription
	if([ctxCouponDict valueForKeyPath:@"mainDescription"]!= nil){
		self.productName =  [ctxCouponDict valueForKey:@"mainDescription"];
		self.productName = [self.productName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
		self.productName = [self.productName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	
	//	self.iconInsertIndex = self.couponTextFull.length-1;
	
	//** bullets text items
	if([ctxCouponDict valueForKeyPath:@"bullets"]!= nil){
		NSArray *bulletArray = [NSArray alloc];
		bulletArray = [ctxCouponDict[@"bullets"] componentsSeparatedByString:@"\n"];
		
		
		for (NSString* currentString in bulletArray)
		{
			if([bulletArray count]> 1){
				if(self.productDescriptionBulletText == nil || ![self.productDescriptionBulletText isKindOfClass:[NSString class]] || [self.productDescriptionBulletText length] < 3){
					self.productDescriptionBulletText = [NSString stringWithFormat:@" • %@\n",currentString];
				}
				else{
					self.productDescriptionBulletText = [NSString stringWithFormat:@"%@ • %@\n",self.productDescriptionBulletText,currentString];
				}
			}
		}
		self.bulletsSourceData =[ctxCouponDict valueForKeyPath:@"bullets"];
		self.bulletsSourceData = [self.bulletsSourceData stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
		self.bulletsSourceData = [self.bulletsSourceData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	
	//** count items text
	if([ctxCouponDict valueForKeyPath:@"count"]!= nil){
		NSArray *countArray = [NSArray alloc];
		countArray = [ctxCouponDict[@"count"] componentsSeparatedByString:@"\n"];
		
		for (NSString* currentString in countArray)
		{
			if([countArray count]> 1){
				if(self.productDescriptionCountText == nil){
					self.productDescriptionCountText = [NSString stringWithFormat:@"%@.\n",currentString];
				}else{
					self.productDescriptionCountText = [NSString stringWithFormat:@"%@%@\n",self.productDescriptionCountText,currentString];
				}
			}
			else{
				self.productDescriptionCountText = [NSString stringWithFormat:@"%@",currentString];
			}
		}
		
		self.countSourceText = ctxCouponDict[@"count"];
		self.countSourceText = [countSourceText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//		self.countSourceText = [countSourceText stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
		self.countSourceText = [countSourceText stringByReplacingOccurrencesOfString:@".." withString:@"."];
	}
	
	//** itemNumbers items text
	if([ctxCouponDict valueForKeyPath:@"itemNumbers"]!= nil){
		NSArray *tempArray = [NSArray alloc];
		if([[ctxCouponDict valueForKeyPath:@"itemNumbers"] isKindOfClass:[NSString class]]){
			tempArray = [ctxCouponDict[@"itemNumbers"] componentsSeparatedByString:@","];
		}
		
		self.itemNumbers = [NSString stringWithFormat:@"%@",[ctxCouponDict valueForKeyPath:@"itemNumbers"]];
		self.itemNumbers = [NSString stringWithFormat:@"%@ %@",self.item,self.itemNumbers];
		
	}
	
	//** selection varies items
	if([ctxCouponDict valueForKeyPath:@"selectionVaries"]!= nil){
		self.selectionVaries = [ctxCouponDict valueForKeyPath:@"selectionVaries"];
	}
}

	

/*
 builds coupon plain text & html output
 displayed on the screen
 */


- (void) checkCouponForErrorsWithLocalizationDict:(NSDictionary*) ctxLangDict{
	
	NSString *chooseFrom = [ctxLangDict valueForKey:@"mvmStyleChooseFrom"];
	self.errorState = false;
	self.errorArray = nil;
	self.errorArray = [[NSMutableArray alloc] init];
	
	int location = self.productName.length - chooseFrom.length;

	
	//***
	//*** Test for product name ending in choose from

	if (productName.length >= chooseFrom.length){
		if([[self.productName substringFromIndex:location] isEqualToString:chooseFrom] ){
			self.errorState = true;
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorChooseFrom" andLangDict:ctxLangDict]];
		}
	}
	
	
	//***
	//*** Test for correct filename
	if (self.imageFilenameMobile == nil ||
		([[self.imageFilenameMobile uppercaseString] rangeOfString:@"PNG"].location == NSNotFound &&
		[[self.imageFilenameMobile uppercaseString] rangeOfString:@"JPG"].location == NSNotFound &&
		[[self.imageFilenameMobile uppercaseString] rangeOfString:@"JPEG"].location == NSNotFound)){
		self.errorState = true;
		[self.errorArray addObject:[self buildErrorDictWithCode:@"errorImageName" andLangDict:ctxLangDict]];
		}
	
	
	//***
	//*** Test for required field name not defined
	if(self.productName == nil || self.dollar == nil || self.itemLimit == nil||self.itemNumbers == nil || self.couponValidText == nil){
		self.errorState = true;
		[self.errorArray addObject:[self buildErrorDictWithCode:@"errorRequiredFieldNotDefined" andLangDict:ctxLangDict]];

	}


	//***
	//*** Test for product name Length
	if (productName.length > 97){
		self.errorState = true;
		[self.errorArray addObject:[self buildErrorDictWithCode:@"errorProductNameLength" andLangDict:ctxLangDict]];
	}
	
	if(self.imageFilenameMobile != nil){
			NSString *fileName = @"/Users/tfessler/costco-mobility-content-curator/mvmImages/";
			fileName = [fileName stringByAppendingString:self.imageFilenameMobile];
			NSImage* testImage = [[NSImage alloc]initWithContentsOfFile:fileName];

		
		if(testImage.size.width > testImage.size.height *1.75){
			self.errorState = true;
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorImageWidth" andLangDict:ctxLangDict]];
		}
		
	
		if(testImage == nil){
			self.errorState = true;
			[self.errorArray addObject:[self buildErrorDictWithCode:@"errorImageName" andLangDict:ctxLangDict]];
		}
		
		
		
		
		
	}
//	if([self.productName rangeOfString:@"$"].location != NSNotFound){
//		self.errorState = true;
//	}
	

//		if([self.productName rangeOfString:@"ct"].location != NSNotFound){
//			self.errorState = true;
//		}


	
	}
}


-(NSDictionary*)buildErrorDictWithCode:(NSString*) errorCode andLangDict:(NSDictionary*)langDict{
	
	NSDictionary* errorDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[langDict valueForKey:errorCode],errorCode, nil];
	
	return errorDictionary;
	
}



//builds html and plain text strings from the imported JSON object
//should be refactored into multiple objects


- (void) buildCouponTextWithLanguageDictionary:(NSDictionary*)ctxLangDict andCoverObject:(couponBookObject*)ctxCoverObject{
		
	
	//** coupon valid text
	NSDateFormatter *dateFormatterValid = [[NSDateFormatter alloc] init];
	[dateFormatterValid setDateStyle:NSDateFormatterShortStyle];
	[dateFormatterValid setTimeStyle:NSDateFormatterNoStyle];
	
	
	self.couponValidText = [[NSString alloc] initWithFormat:@"%@ %@ - %@*",
							[ctxLangDict valueForKey:@"mvmValid"],
							[dateFormatterValid stringFromDate:self.couponStartDate],
							[dateFormatterValid stringFromDate:self.couponEndDate]];
	
	
	//** coupon banner
	if(self.showCouponBannerOverrride == true || self.couponStartDate != ctxCoverObject.couponBookStartDate || self.couponEndDate != ctxCoverObject.couponBookEndDate){
		
		self.showCouponBanner = true;
		
		self.couponBannerText = [ctxLangDict valueForKey:@"mvmCouponBannerText"];

		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateStyle:NSDateFormatterShortStyle];
		[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		
		self.couponBannerText = [self.couponBannerText stringByReplacingOccurrencesOfString:@"XX" withString:[dateFormatter stringFromDate:self.couponStartDate]];

		self.couponBannerText = [self.couponBannerText stringByReplacingOccurrencesOfString:@"YY" withString:[dateFormatter stringFromDate:self.couponEndDate]];
		
		
		NSDateFormatter *f = [[NSDateFormatter alloc] init];
		[f setDateFormat:@"yyyy-MM-dd"];
		
		NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
															fromDate:self.couponStartDate
															  toDate:self.couponEndDate
															 options:0];
			
		long weeks = ([components day]+1)/7;
		NSString *weekString;
		long weekNum = weeks;
		
		if(weeks < 1){
			weekString = [ctxLangDict valueForKey:@"mvmCouponBannerDays"];
			weekNum = [components day]+1;
		}
		
		else if(weeks < 2 ){
			weekString = [ctxLangDict valueForKey:@"mvmCouponBannerWeek"];
		}else{
			weekString = [ctxLangDict valueForKey:@"mvmCouponBannerWeeks"];
		}
		
		self.couponBannerText = [NSString stringWithFormat:@"%@ %ld %@",self.couponBannerText,weekNum,weekString];
		
		
	}else{
		self.showCouponBanner = false;
	}
	
	
	self.couponCSS = [ctxLangDict valueForKey:@"css"];

	
	//**instantSavings
	self.couponTextList = [NSString stringWithFormat:@"%@\n",self.instantSavings];
	
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"%@<P><span class=\"instantSavings\">%@</span></BR>",
							   self.couponCSS, self.instantSavings];
	
	self.couponTextListHTMLSimple = [NSString stringWithFormat:
							   @"<P><font color = \"red\"><sub>%@</sub></br>",
							   self.instantSavings];
	
	
	
	
	//**dollar
	if(self.dollar != nil){
		
		if ([self.dollar rangeOfString:@"-"].location != NSNotFound){
			//this is a range
			
			if([self.dollar rangeOfString:@"."].location != NSNotFound){
				//there is a decimal
				
				NSMutableArray *countDecimalsArray = [[NSMutableArray alloc] init];
				NSUInteger count = 0, length = [self.dollar length];
				NSRange range = NSMakeRange(0, length);
				while(range.location != NSNotFound)
				{
					range = [self.dollar rangeOfString: @"." options:0 range:range];
					if(range.location != NSNotFound)
					{
						 id location = [NSNumber numberWithInteger:range.location];
						[countDecimalsArray addObject:location];
						range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
						count++; 
					}
				}
				NSLog(@"counted %lu",(unsigned long)countDecimalsArray.count);
				
			}
			
		}else{
			
		
		
		
		
		
		self.couponTextList = [NSString stringWithFormat:@"%@%@%@",self.couponTextList,self.dollarSign,self.dollar];
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"dollarSign\">%@</span><span class=\"dollar\">%@</span>",
								   self.couponTextListHTML,self.dollarSign,self.dollar];

	
		self.couponTextListHTMLSimple = [NSString stringWithFormat:
								   @"%@<small><sup>%@</sup></small> <big>%@</big>",
								   self.couponTextListHTMLSimple,self.dollarSign,self.dollar];

		}
	}
	
	
	//**cents
	if(self.cents!= nil){
		self.couponTextList = [NSString stringWithFormat:@"%@%@",self.couponTextList,self.cents];
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"dollarsign\">%@</span>",
								   self.couponTextListHTML,self.cents];
	
		self.couponTextListHTMLSimple = [NSString stringWithFormat:
								   @"%@<small><sup>%@/sup></small>",
								   self.couponTextListHTMLSimple,self.cents];
	
	}

	
	
	
	
	//** off
	self.couponTextList = [NSString stringWithFormat:@"%@ %@",self.couponTextList,self.off];
	self.couponTextListHTML = [NSString stringWithFormat:
							   @"%@<span class=\"superScript\"> </span><span class=\"off\">%@</span>",
							   self.couponTextListHTML,self.off];
	

	self.couponTextListHTMLSimple = [NSString stringWithFormat:
							   @"%@<small><sup> </sup></small></large>%@</large>",
							   self.couponTextListHTMLSimple,self.off];
	
	
	//** itemLimit
	if(self.itemLimit!= nil){
		
		if(self.itemLimit.length < 14){
			self.couponTextList = [NSString stringWithFormat:@"%@ %@\n",self.couponTextList,self.itemLimit];
		}
		else{
			self.couponTextList = [NSString stringWithFormat:@"%@\n%@\n",self.couponTextList,self.itemLimit];
		}
		
		self.couponTextListHTML = [NSString stringWithFormat:
								   @"%@<span class=\"superScript\">%@</span></BR></BR>",
								   self.couponTextListHTML,self.itemLimit];

		self.couponTextListHTMLSimple = [NSString stringWithFormat:
								   @"%@<small><sup>%@</sup></small></br>",
								   self.couponTextListHTMLSimple,self.itemLimit];
	}
	
	
	//Start the varation for large layout
	self.couponTextDetail = self.couponTextList;
	
	//** mainDescription
	if(self.productName!= nil){
		
		self.couponTextList = [NSString stringWithFormat:@"%@%@",self.couponTextList,self.productName];
		
		self.couponTextDetailHTML = self.couponTextListHTML = [NSString stringWithFormat:
															 @"%@<span class=\"productName\">%@</span></BR>",
															 self.couponTextListHTML,self.productName];

		self.couponTextListHTMLSimple = [NSString stringWithFormat:
															   @"%@</font><font color = \"black\">  %@</font>",
															   self.couponTextListHTMLSimple,self.productName];
		
		
		if(self.dotComURL != nil){
			self.couponTextDetail = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextDetail,self.productName];
		}else{
			self.couponTextDetail = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextDetail,self.productName];
		}
	}
	self.couponTextDetailHeader = self.couponTextList;
	
	
	//start building the detail html
	self.couponTextDetailHTMLBody = self.couponCSS;
	
	//** bullets text items
	if(self.productDescriptionBulletText != nil && productDescriptionBulletText.length > 1){
		NSArray *bulletArray = [NSArray alloc];
		bulletArray = [self.productDescriptionBulletText componentsSeparatedByString:@"\n"];
		
		self.bulletTextHTML = @"<ul class=\"b\">";
		
		for (NSString* currentString in bulletArray)
		{
			
			if([bulletArray count]> 1){
				if(self.productDescriptionBulletText == nil || ![self.productDescriptionBulletText isKindOfClass:[NSString class]] || [self.productDescriptionBulletText length] < 3){
					self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
					
				}
				else{
					self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
				}
				
			}
			else{
				self.bulletTextHTML = [NSString stringWithFormat:@"%@<li>%@</li>",bulletTextHTML,currentString];
			}
			
		}
		self.bulletTextHTML = [NSString stringWithFormat:@"%@</ul>",bulletTextHTML];
		
		self.couponTextDetailBody = [NSString stringWithFormat:@"%@\n",self.productDescriptionBulletText];
		
		self.couponTextDetailHTMLBody = [NSString stringWithFormat:
										 @"%@%@",
										 self.couponTextDetailHTMLBody,self.bulletTextHTML];
		
		self.couponTextDetail =  [NSString stringWithFormat:@"%@%@\n",self.couponTextDetail,self.productDescriptionBulletText];
		self.couponTextDetailHTML = [NSString stringWithFormat:
								   @"%@%@",
								   self.couponTextDetailHTML,self.bulletTextHTML];
	}
	

	
	
	//** count items text
	if(self.productDescriptionCountText != nil){
		
		if(self.couponTextDetailBody == nil){
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@\n\n",self.productDescriptionCountText];
		}else{
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@%@\n\n",self.couponTextDetailBody, self.productDescriptionCountText];
		}
		
		self.couponTextDetail =  [NSString stringWithFormat:@"%@%@\n",self.couponTextDetail,self.productDescriptionCountText];
		
		self.couponTextDetailHTMLBody = [NSString stringWithFormat:
								   @"%@%@</BR>",
								   self.couponTextDetailHTMLBody,self.productDescriptionCountText];
		
		
		
	}
	
	
	
	//** itemNumbers items text
	if(self.itemNumbers!= nil){
		
		if(self.couponTextDetailBody == nil){
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@\n",self.itemNumbers];
		}else{
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@%@\n",self.couponTextDetailBody, self.itemNumbers];
		}
		
			
		self.couponTextDetail = [NSString stringWithFormat:@"%@%@\n\n",self.couponTextDetail,self.itemNumbers];
		self.couponTextDetailHTML = [NSString stringWithFormat:
								   @"%@%@</BR>",
								   self.couponTextDetailHTML,self.itemNumbers];
		
		self.couponTextDetailHTMLBody = [NSString stringWithFormat:
									 @"%@%@</BR>",
									 self.couponTextDetailHTMLBody,self.itemNumbers];
		
	}
	
	
	//** selection varies items
	if(self.selectionVaries!= nil){
		
		
		if(self.couponTextDetailBody == nil){
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@\n",self.selectionVaries];
		}else{
			self.couponTextDetailBody =  [NSString stringWithFormat:@"%@%@\n",self.couponTextDetailBody, self.selectionVaries];
		}
		
		self.couponTextDetail = [NSString stringWithFormat:@"%@%@\n\n",
							   self.couponTextDetail,self.selectionVaries];
		
		self.couponTextDetailHTML = [NSString stringWithFormat:
								   @"%@<span class=\"selectionVaries\">%@</span></BR>",
								   self.couponTextDetailHTML,self.selectionVaries];

		self.couponTextDetailHTMLBody = [NSString stringWithFormat:
									 @"%@<span class=\"selectionVaries\">%@</span></BR>",
									 self.couponTextDetailHTMLBody,self.selectionVaries];
	
	}
	
	

	
	//always rebuild using the current legal text
	self.termsAndConditionsText =[ctxLangDict valueForKey:@"mvmLayoutTerms"];
	
	self.couponTextDetail = [NSString stringWithFormat:@"%@%@\n\n*%@",
						   self.couponTextDetail,
						   self.couponValidText,
						   self.termsAndConditionsText];
	
	if(self.couponTextDetailBody == nil){
		self.couponTextDetailBody =  [NSString stringWithFormat:@"%@\n\n*%@",
									  self.couponValidText,
									  self.termsAndConditionsText];
	}else{
		self.couponTextDetailBody =  [NSString stringWithFormat:@"%@%@\n\n*%@",
									  self.couponTextDetailBody,
									  self.couponValidText,
									  self.termsAndConditionsText];	}
	
	
	
	NSString *imageName = [NSString stringWithFormat:@"<img src=\"%@\" alt=\"%@\" style=\"height:250px;border:1px solid #fff; float: right; margin:1px;\">", self.imageFilenameMobile,self.couponTextDetail];
	
	
	self.couponTextDetailHTML = [NSString stringWithFormat:@"%@<span class=\"valid\">%@</span></BR></P></BR></BR>",
							   self.couponTextDetailHTML,self.couponValidText];

	self.couponTextDetailHTMLBody = [NSString stringWithFormat:@"%@<span class=\"valid\">%@</span></BR></P></BR></BR>",
								 self.couponTextDetailHTMLBody,self.couponValidText];
	
	
	
	//** final output
	self.couponTextList = [NSString stringWithFormat:@"%@",self.couponTextList];

	if(showDotComLink == true){
		
		self.couponTextDetail = [NSString stringWithFormat:@"\n%@\n\n",self.couponTextDetail];

		self.couponTextList = [NSString stringWithFormat:@"%@\n",self.couponTextList];
		
	}else{
		self.couponTextDetail = [NSString stringWithFormat:@"%@\n",self.couponTextDetail];
	}
	
	
	int textLen = (int)[self.couponTextList length];
	int length = 145;
	
//	if(self.showDotComLink == true){
		length += 20;
//	}
	
	if (textLen > 120){
		length += textLen*.24;
	}
	self.cellHeight = length;
	
	self.couponTextDetailHTMLHeader = self.couponTextListHTML;
	
	
}




-(void) prerenderAttributedTextForCouponStyle:(int)CouponStyle{
	
	__block NSString* productNameWithDetails;
	int instantSavingsFontSize;
	int titleFontSize;
	int	dollarFontSize;
	int	dollarSignFontSize;
	int baseFontSize;
	int termsFontSize;

	
	if(CouponStyle == legalList){
		
		NSFont *baseFontArrow = [NSFont fontWithName:@"HelveticaNeue" size:13];
		NSFont *baseFont = [NSFont fontWithName:@"HelveticaNeue" size:13];
		NSMutableParagraphStyle * paragraphStyleBase = [[NSMutableParagraphStyle alloc] init];
		paragraphStyleBase.alignment = kCTTextAlignmentLeft;
		paragraphStyleBase.minimumLineHeight = baseFont.pointSize-8 * 2;
		paragraphStyleBase.maximumLineHeight = baseFont.pointSize-8 *2 ;
		paragraphStyleBase.lineSpacing = 0;
		
		NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:self.termsAndConditionsText];
		[title addAttribute:NSFontAttributeName value:baseFont range:[self.termsAndConditionsText rangeOfString:self.termsAndConditionsText]];
		[title addAttribute:NSFontAttributeName value:baseFontArrow range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		
		[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBase range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		[title addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:[self.termsAndConditionsText rangeOfString:@"↵"]];
		self.couponTextListAttributed = self.couponTextDetailAttributed =  title;
		
	}else{
	
		if(CouponStyle == listView){
			productNameWithDetails = self.couponTextList;
			instantSavingsFontSize = 14;
			titleFontSize = 18;
			dollarFontSize =30;
			dollarSignFontSize = 16;
			baseFontSize = 16;
			
			//place bug icons at the bottom of the text
			self.iconInsertIndex = self.couponTextList.length;

		}

		if(CouponStyle == largeView){
			//put bug icons at the top of the text
			self.iconInsertIndex = 0;
			float scale = 1.5;
			productNameWithDetails = self.couponTextDetail;
			instantSavingsFontSize = 15*scale;
			titleFontSize = 14*scale;
			dollarFontSize =30*scale;
			dollarSignFontSize = 12*scale;
			baseFontSize = 12*scale;
			termsFontSize = 12;
		}
				
		NSFont *instantSavingsFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:instantSavingsFontSize];
		NSFont *itemLimitfont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:dollarSignFontSize];
		NSFont *titleFont = [NSFont fontWithName:@"HelveticaNeue-CondensedBold" size:titleFontSize];
		NSFont *dollarFont = [NSFont fontWithName:@"HelveticaNeue-Bold" size:dollarFontSize];
		NSFont *dollarSignFont = [NSFont fontWithName:@"HelveticaNeue-Bold" size:dollarSignFontSize];
		NSFont *offFont = [NSFont fontWithName:@"HelveticaNeue" size:dollarFontSize];
		NSFont *baseFont = [NSFont fontWithName:@"HelveticaNeue" size:baseFontSize];
		NSFont *selectionVariesFont = [NSFont fontWithName:@"HelveticaNeue-Italic" size:baseFontSize];
		NSFont *TandCFont = [NSFont fontWithName:@"HelveticaNeue" size:termsFontSize];
		
		NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:productNameWithDetails];

		


		
		[title addAttribute:NSFontAttributeName value:baseFont range:[productNameWithDetails rangeOfString:productNameWithDetails]];
		[title addAttribute:NSFontAttributeName value:TandCFont range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
		[title addAttribute:NSFontAttributeName value:titleFont range:[productNameWithDetails rangeOfString:productName]];
//		[title addAttribute:NSKernAttributeName
//								 value:@(-.5)
//								 range:[productNameWithDetails rangeOfString:productName]];
//

//		if(itemLimit != nil){
//
//			if(itemLimit.length < 10){
//
//				
//			[title addAttribute:NSKernAttributeName
//						  value:@(-.5)
//						  range:[productNameWithDetails rangeOfString:itemLimit]];
//			}
//			else
//			{
//				[title addAttribute:NSKernAttributeName
//							  value:@(-1)
//							  range:[productNameWithDetails rangeOfString:itemLimit]];
//
//		
//			}
//		}
		
		
		if(countSourceText != nil){
		[title addAttribute:NSKernAttributeName
					  value:@(-.5)
					  range:[productNameWithDetails rangeOfString:countSourceText]];
		}
		if(productDescriptionBulletText != nil){
			[title addAttribute:NSKernAttributeName
						  value:@(-.5)
						  range:[productNameWithDetails rangeOfString:productDescriptionBulletText]];
		}
		
		
		[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.dollarSign]];
		
		if(self.cents != nil){
			[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.cents]];
		}
		
		[title addAttribute:NSFontAttributeName value:dollarSignFont range:[productNameWithDetails rangeOfString:self.itemLimit]];
		
		if(dollar != nil){
			[title addAttribute:NSFontAttributeName value:dollarFont range:[productNameWithDetails rangeOfString:self.dollar]];
		}
		
			[title addAttribute:NSFontAttributeName value:offFont range:[productNameWithDetails rangeOfString:self.off]];
			[title addAttribute:NSFontAttributeName value:instantSavingsFont range:[productNameWithDetails rangeOfString:self.instantSavings]];
		
		[title addAttribute:NSFontAttributeName value:itemLimitfont range:[productNameWithDetails rangeOfString:self.itemLimit]];
		

		
		NSString* superScriptType = @"1";
		if(CouponStyle == largeView)
			superScriptType = @"2";
		
		if(self.selectionVaries != nil)
			[title addAttribute:NSFontAttributeName value:selectionVariesFont range:[productNameWithDetails rangeOfString:self.selectionVaries]];
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.dollarSign]];
		
		if(cents != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.cents]];
		
		if(itemLimit != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.itemLimit]];
		
		if(itemPrefix != nil)
			[title addAttribute:(NSString*)kCTSuperscriptAttributeName value:superScriptType range:[productNameWithDetails rangeOfString:self.itemPrefix]];
			
		
		//TODO: add indent attribute to bullet section!!
		//TODO: add red coloring to selection varies text
		
		
		NSMutableParagraphStyle * paragraphStyleBullets = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleBullets.headIndent = -1;
		
		NSShadow * myShadow = [[NSShadow alloc] init];
			myShadow.shadowColor = [NSColor lightGrayColor];
			myShadow.shadowOffset = CGSizeMake(1,1);
			myShadow.shadowBlurRadius = 4.0;
		
		NSShadow * whiteShadow = [[NSShadow alloc] init];
			whiteShadow.shadowColor = [NSColor whiteColor];
			whiteShadow.shadowOffset = CGSizeMake(2,2);
			whiteShadow.shadowBlurRadius = 1.0;
		
		NSMutableParagraphStyle * paragraphStyleBase = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleBase.alignment = kCTTextAlignmentLeft;
			paragraphStyleBase.minimumLineHeight = baseFont.pointSize-2;
			paragraphStyleBase.maximumLineHeight = baseFont.pointSize-2;
			paragraphStyleBase.lineSpacing = 0;
		
		NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
			paragraphStyle.alignment = kCTTextAlignmentLeft;
			paragraphStyle.minimumLineHeight = dollarFont.pointSize-2;
			paragraphStyle.maximumLineHeight = dollarFont.pointSize-2;
			paragraphStyle.lineSpacing = 0;
		
		NSMutableParagraphStyle * paragraphStyleTitle = [[NSMutableParagraphStyle alloc] init];
			paragraphStyleTitle.alignment = kCTTextAlignmentLeft;
			paragraphStyleTitle.minimumLineHeight = titleFont.pointSize-1;
			paragraphStyleTitle.maximumLineHeight = titleFont.pointSize-1;
			paragraphStyleTitle.lineSpacing = 0;

		NSMutableParagraphStyle * paragraphAmountLine = [[NSMutableParagraphStyle alloc] init];
		paragraphAmountLine.alignment = kCTTextAlignmentLeft;
		paragraphAmountLine.minimumLineHeight = dollarFont.pointSize+2;
		paragraphAmountLine.maximumLineHeight = dollarFont.pointSize+2;
		paragraphAmountLine.lineSpacing = 0;
		
		
		NSMutableParagraphStyle * zzz = [[NSMutableParagraphStyle alloc] init];
		zzz.alignment = kCTTextAlignmentRight;
		zzz.minimumLineHeight = -10;
		zzz.maximumLineHeight = -10;
		zzz.lineSpacing = -10;
		
		
		
		[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[title.string rangeOfString:self.termsAndConditionsText]];
		
	//	[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBase range:[productNameWithDetails rangeOfString:productNameWithDetails]];
		
		if(self.productDescriptionBulletText != nil){
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleBullets range:[productNameWithDetails rangeOfString:self.productDescriptionBulletText]];}
		
		
	//		[title addAttribute:NSShadowAttributeName value:whiteShadow range:[productNameWithDetails rangeOfString:self.instantSavings]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[productNameWithDetails rangeOfString:self.instantSavings]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleTitle range:[productNameWithDetails rangeOfString:productName]];
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphAmountLine range:[productNameWithDetails rangeOfString:self.dollarSign]];

			[title addAttribute:NSParagraphStyleAttributeName value:paragraphAmountLine range:[productNameWithDetails rangeOfString:self.dollar]];
		
		
		
		
			if(self.cents != nil)
			[title addAttribute:NSParagraphStyleAttributeName value:paragraphAmountLine range:[productNameWithDetails rangeOfString:self.cents]];

		[title addAttribute:NSParagraphStyleAttributeName value:paragraphAmountLine range:[productNameWithDetails rangeOfString:self.off]];
		
		
		
			[title addAttribute:NSBackgroundColorAttributeName value:[NSColor clearColor] range:[productNameWithDetails rangeOfString:productNameWithDetails]];



		if(self.selectionVaries != nil)
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.selectionVaries]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:dollar]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.dollarSign]];
		if(self.itemPrefix != nil)
		[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.itemPrefix]];

		if(self.cents != nil)
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.cents]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.off]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.itemLimit]];
			
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.instantSavings]];
			
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleAndOr]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleOr]];
			[title addAttribute:NSForegroundColorAttributeName value:[NSColor redColor] range:[productNameWithDetails rangeOfString:self.styleChooseFrom]];

		if(CouponStyle == listView){
			if(showUpTo == true){
				NSTextAttachment* textAttachmenta = [NSTextAttachment new];;
				NSTextAttachmentCell* myCella = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"upTo.png"]];
				[textAttachmenta setAttachmentCell:myCella];
				[myCella.image setSize:CGSizeMake(18,20)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachmenta] atIndex:self.instantSavings.length +1];
			}
			
			
			if(self.showDotComLink == true){
				NSTextAttachment* textAttachment = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-available-online-small@2x.png"]];
				[textAttachment setAttachmentCell:imageCell];
				[imageCell.image setSize:CGSizeMake(80,35)];
				[title appendAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment]];
				
			}
			if(self.showBookAppRequired == true){
				NSTextAttachment* textAttachment2 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell2 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-book-app-required-small@2x.png"]];
				[textAttachment2 setAttachmentCell:imageCell2];
				[imageCell2.image setSize:CGSizeMake(80,35)];
				[title appendAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment2]];
			}
			
			
			[title addAttribute:NSBackgroundColorAttributeName value:[NSColor whiteColor] range:[title.string rangeOfString:title.string]];
			self.couponTextListAttributed = [[NSMutableAttributedString alloc]initWithAttributedString:title];
			}
		else{
			
			
			
			
			if(self.termsAndConditionsText != nil){
				[title addAttribute:NSForegroundColorAttributeName value:[NSColor grayColor] range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
	//			[title addAttribute:NSParagraphStyleAttributeName value:paragraphStyleTerms range:[productNameWithDetails rangeOfString:self.termsAndConditionsText]];
			}
			
			if(showUpTo == true){
				NSTextAttachment* textAttachmentUpTo = [NSTextAttachment new];;
				NSTextAttachmentCell* upToCell = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"upTo@2x.png"]];
				[textAttachmentUpTo setAttachmentCell:upToCell];
				[upToCell.image setSize:CGSizeMake(28,32)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachmentUpTo] atIndex:self.instantSavings.length +2];
			}
			
			if(self.showBookAppRequired == true){
				NSTextAttachment* textAttachment4 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell4 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-book-app-required@2x.png"]];
				[textAttachment4 setAttachmentCell:imageCell4];
				[imageCell4.image setSize:CGSizeMake(100,44)];
				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment4] atIndex:0];
			}
			
			if(self.showDotComLink == true){
				NSTextAttachment* textAttachment3 = [NSTextAttachment new];
				NSTextAttachmentCell *imageCell3 = [[NSTextAttachmentCell alloc] initImageCell:[NSImage imageNamed:@"bug-available-online@2x.png"]];
				[textAttachment3 setAttachmentCell:imageCell3];
				[imageCell3.image setSize:CGSizeMake(100,44)];

				[title insertAttributedString:[NSAttributedString attributedStringWithAttachment:textAttachment3] atIndex:0];
			}
			
			self.couponTextDetailAttributed = [[NSMutableAttributedString alloc]initWithAttributedString:title];
		}
		
		
	}
		
}







@end
