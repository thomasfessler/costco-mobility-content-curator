//
//  NSColor+hexcolor.h
//


#import <Foundation/Foundation.h>


@interface NSColor (hexColor)

+ (NSColor *)colorWithHexString:(NSString *)hexString;
+ (NSArray *)colorDictionaryFromHexString:(NSString *)hexString;

@end
