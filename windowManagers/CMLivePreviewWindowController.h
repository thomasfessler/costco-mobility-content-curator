/*

  
*/

#import <Cocoa/Cocoa.h>
#import "CWCAppManager.h"
#import "ATDesktopEntity.h"
#import "mvmCouponListTableView.h"
#import "CMColorTableController.h"
#import "mvmPathControl.h"


@interface CMLivePreviewWindowController : NSWindowController<NSWindowDelegate, NSApplicationDelegate> {

	
@private
	
	IBOutlet NSTextField *errorText;
//	IBOutlet mvmPathControl *mvmPath;
	CWCAppManager *appManager;
    IBOutlet mvmCouponListTableView *_tableViewMain;
	
	NSSegmentedControl *_couponSort;
	
}

@property IBOutlet NSTextField *errorText;

@property (strong) IBOutlet NSSegmentedControl *couponSort;
@end
