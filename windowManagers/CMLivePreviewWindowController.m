/*

 */

#import <QuartzCore/QuartzCore.h>

#import "CMLivePreviewWindowController.h"
#import "CWCAppManager.h"


@implementation CMLivePreviewWindowController
@synthesize errorText,couponSort;

- (void)dealloc {

	
}



-(NSString *)windowNibName {
    return @"mvmLivePreview";
}

- (void)windowDidLoad {
    [super windowDidLoad];
	
	
	self->appManager = [CWCAppManager sharedAppManager];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateErrorView) name:@"updateWebView" object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadViews) name:NSApplicationDidBecomeActiveNotification object:nil];
	
	
	CALayer *viewLayer = [CALayer layer];
	[viewLayer setBackgroundColor:CGColorCreateGenericRGB(.8, .8,.8, 1.0)];
	[self.window.contentView setWantsLayer:YES];
	[self.window.contentView setLayer:viewLayer];
//	self.window.contentView.layer.borderColor =[NSColor blackColor].CGColor;
//	self.layer.borderWidth = 3;
	
	
	self->appManager.currentSortMode = 1;
	[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadList" object:nil userInfo:nil];

	 
}



-(void) updateErrorView{
	
	
	couponObjectFactory* currentCoupon = [self->appManager.couponObjectArray objectAtIndex:self->appManager.mvmCouponIndex];
	
	if(currentCoupon.errorState == true){
	
		self.errorText.stringValue =currentCoupon.errorArray.description;

	}
	else{
		self.errorText.stringValue = @"no error";
	}
	
}

- (IBAction)SortModeChanged:(NSSegmentedControl *)sender {
	
	self->appManager.currentSortMode = sender.selectedSegment+1;
	[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadList" object:nil userInfo:nil];
	
}

-(void) reloadViews{
	
		[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadList" object:nil userInfo:nil];
	
		[[NSNotificationCenter defaultCenter] postNotificationName:@"updateWebView" object:nil userInfo:nil];
	
	
}


@end
