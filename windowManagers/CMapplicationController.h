//CMapplicationController.h

#import <Cocoa/Cocoa.h>

@interface CMapplicationController : NSObject<NSTableViewDataSource, NSTableViewDelegate> {
@private
    NSMutableArray *_windowControllers;
    IBOutlet NSArrayController *_arryCtrlTableContents;
    NSMutableArray *_tableContents;
}

@end
