//CMapplicationController.m

#import "CMapplicationController.h"

#import "CMLivePreviewWindowController.h"
#import "mvmControlCenterViewController.h"
#import "couponBookCoverWindowController.h"
#import "newMenuWIndowController.h"
#import "couponContentEditor.h"
#import "colorListWindowController.h"
#import "CWCAppManager.h"


@implementation CMapplicationController{
	
	CWCAppManager	*appManager;
	
}


// Keys used for bindings
#define CMKeyClass @"class"
#define CMKeyName @"name"
#define CMKeyShortDescription @"shortDescription"
#define CMKeyImagePreview @"imagePreview"


- (void)_newWindowWithControllerClass:(Class)c {
    NSWindowController *controller = [[c alloc] init];
    if (_windowControllers == nil) {
        _windowControllers = [NSMutableArray new];
    }
    [_windowControllers addObject:controller];
    [controller showWindow:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
	
	self->appManager = [CWCAppManager sharedAppManager];
	
	
    if (_tableContents == nil) {
        _tableContents = [NSMutableArray new];
        [self willChangeValueForKey:@"_tableContents"];
 
		[_tableContents addObject:@{CMKeyClass: [colorListWindowController class],
									CMKeyName: @"Color List",
									CMKeyShortDescription: @"Color List",
									CMKeyImagePreview: [NSImage imageNamed:@"ATComplexTableViewControllerPreview.png"]}];
		
		[_tableContents addObject:@{CMKeyClass: [couponBookCoverWindowController class],
									CMKeyName: @"Coupon Cover Page Editor",
									CMKeyShortDescription: @"Our very own data entry screem",
									CMKeyImagePreview: [NSImage imageNamed:@"ATComplexTableViewControllerPreview.png"]}];

        [_tableContents addObject:@{CMKeyClass: [couponContentEditor class],
									CMKeyName: @"Coupon Content Editor",
									CMKeyShortDescription: @"edit data for the current coupon",
									CMKeyImagePreview: [NSImage imageNamed:@"ATComplexTableViewControllerPreview.png"]}];

		[_tableContents addObject:@{CMKeyClass: [CMLivePreviewWindowController class],
									CMKeyName: @"Coupon Content Preview",
									CMKeyShortDescription: @"A Complex Cell Example",
									CMKeyImagePreview: [NSImage imageNamed:@"ATComplexTableViewControllerPreview.png"]}];
        
		[self didChangeValueForKey:@"_tableContents"];
        
        // Observe all windows closing so we can remove them from our array
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_windowClosed:) name:NSWindowWillCloseNotification object:nil];
    }
}

- (void)_windowClosed:(NSNotification *)note {
    NSWindow *window = [note object];
    for (NSWindowController *winController in _windowControllers) {
        if (winController.window == window) {
			 // Keeps the instance alive a little longer so things can unbind from it
            [_windowControllers removeObject:winController];
            break;
        }
    }
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    // Create something on startup
    [self _newWindowWithControllerClass:[colorListWindowController class]];
    [self _newWindowWithControllerClass:[couponBookCoverWindowController class]];
    [self _newWindowWithControllerClass:[couponContentEditor class]];
    [self _newWindowWithControllerClass:[CMLivePreviewWindowController class]];
}

@end
