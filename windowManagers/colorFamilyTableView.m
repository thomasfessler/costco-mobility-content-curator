//
//  colorTableView.m
//  CostcoMobilityContentManager
//
//  Created by Thomas Fessler on 6/23/14.
//
//

#import "colorFamilyTableView.h"

@interface colorFamilyTableView(){
	
	NSArray *tableArray;
	NSArray *tableArrayOne;
	CWCAppManager *myAppManager;
	NSString *keyOne;
	NSString *keyTwo;
	NSString *keyType;
	NSString *listType;
	
	CGFloat cellHeight;
	NSNumber *minValue;
	NSNumber *maxValue;
	NSNumber *value;
	NSString *sliderCategory;
	
	NSString *dataSignal;
	NSString *actionSignal;
	
	
	
}

@end


@implementation colorFamilyTableView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		self.delegate = self;
		self.dataSource = self;
		
		
    }
	[self awakeFromNib];
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


- (void)awakeFromNib {
	// Initialization code

	
	self->cellHeight = 30;

	
	if(self->myAppManager == nil){
		self->myAppManager = [CWCAppManager sharedAppManager];
		self.delegate = self;
	}
	self->tableArray = [[NSArray alloc] initWithArray:[self->myAppManager.CWCColorDictionary allKeys]];
	[self reloadData];
	
	self.delegate = self;
	self.dataSource = self;
	

	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(styleSelected:) name:@"colorSelected" object:nil];
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorySelected:) name:@"categorySelected" object:nil];
	

	
}


-(void) categorySelected:(NSNotification*) myNotification{
	
	self->tableArray = nil;
	[self reloadData];
}



-(void) styleSelected:(NSNotification*) myNotification{
	
	
	self->cellHeight = 30;
	self->keyType = [[myNotification userInfo] valueForKey:@"keyType"];
	self->keyOne = [[myNotification userInfo] valueForKey:@"keyOne"];
	self->keyTwo = [[myNotification userInfo] valueForKey:@"keyTwo"];
	self->tableArray = nil;
	self->listType =[[myNotification userInfo] valueForKey:@"listType"];
	
	if ([self->keyType isEqualToString:@"webColor"]){
		
		self->tableArray = [self->myAppManager.CWCColorDictionary allKeys];
		[self reloadData];
		
		for(int i = 0 ; i < [tableArray count]; i++){
			
			if([[self->tableArray objectAtIndex:i] isEqualToString:keyOne]){
				
				NSIndexPath *indexPath = [NSIndexPath indexPathWithIndex:i];
				[self selectRow:i byExtendingSelection:NO];
//				[self selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:NSTableView];
				indexPath = nil;
			}
			
		}
	}
	
	
	else{
		self->tableArray = nil;
		[self reloadData];
		
	}
}


- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
	
	return 	self->cellHeight;
	
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	
	return [self->tableArray count];
	
}


- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	
	NSTableCellView *cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];
	NSTextField* listText = (NSTextField*) [cellView viewWithTag:100];
	listText.stringValue = [tableArray objectAtIndex:row] ;
	return cellView;
}


	
	
	
	



@end
