//
//  colorItemsTableView.h
//  CostcoMobilityContentManager
//
//  Created by Thomas Fessler on 6/23/14.
//
//

#import <Cocoa/Cocoa.h>

@interface colorItemsTableView : NSTableView <NSTableViewDataSource, NSTableViewDelegate, NSTextFieldDelegate>

@property (strong) NSMutableArray *tableArray;
-(void)setColorCustomSortStartLocation:(int)startLocation lengthOfSubstring:(int)scanLength;
@end
