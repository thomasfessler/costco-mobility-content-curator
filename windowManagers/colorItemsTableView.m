//
//  colorItemsTableView.m
//  CostcoMobilityContentManager
//
//  Created by Thomas Fessler on 6/23/14.
//
//

#import "colorItemsTableView.h"
#import "CWCAppManager.h"
#import "NSColor+hexcolor.h"

@interface colorItemsTableView(){
	

	NSArray *tableArrayOne;
	CWCAppManager *myAppManager;
	NSString *keyOne;
	NSString *keyTwo;
	NSString *keyType;
	NSString *listType;
	
	CGFloat cellHeight;
	NSNumber *minValue;
	NSNumber *maxValue;
	NSNumber *value;
	NSString *sliderCategory;
	
	NSString *dataSignal;
	NSString *actionSignal;
	
	
	
}
@end

@implementation colorItemsTableView
@synthesize tableArray;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		self.delegate = self;
		self.dataSource = self;
    }
	[self awakeFromNib];
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


- (void)awakeFromNib {
	// Initialization code
	
	
	if(tableArray == nil){
	
	self->cellHeight = 30;
	
	
	self->myAppManager = [CWCAppManager sharedAppManager];

	
	
	
	self.tableArray = [[[[NSMutableArray alloc] initWithArray:[[self->myAppManager.CWCColorDictionary valueForKey:@"expanded"] allKeys]]sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
	[self reloadData];
	
	
	
	self.delegate = self;
	self.dataSource = self;
	
	//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(styleSelected:) name:@"colorSelected" object:nil];
	//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(categorySelected:) name:@"categorySelected" object:nil];}
		
	}
}


-(void)setColorCustomSortStartLocation:(int)startLocation lengthOfSubstring:(int)scanLength  {
	
	NSMutableArray *tempArray = [[NSMutableArray alloc] init];
//	self->tableArray = [[[self->myAppManager.CWCColorDictionary valueForKey:@"expanded"] allKeys]sortedArrayUsingSelector:@selector(compare:)];
	
	if(startLocation != -99){
		
		tempArray = [[self.tableArray sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSString *str1 , NSString *str2)
					  {
						  NSString* first = [[[self->myAppManager.CWCColorDictionary objectForKey:@"expanded"] valueForKey:str1] substringWithRange:NSMakeRange(startLocation, scanLength)];
						  NSString* second =[[[self->myAppManager.CWCColorDictionary objectForKey:@"expanded"] valueForKey:str2] substringWithRange:NSMakeRange(startLocation, scanLength)];
						  
						  return [first caseInsensitiveCompare:second];
					  }] mutableCopy];
		
		
	}else{
		tempArray = [[self.tableArray sortedArrayWithOptions:NSSortStable usingComparator:^NSComparisonResult(NSString *str1 , NSString *str2)
					  {
						  return [str1 caseInsensitiveCompare:str2];
					  }] mutableCopy];

	}
	
	self.tableArray = tempArray;
	
	[self reloadData];
	
}



-(void) categorySelected:(NSNotification*) myNotification{
	
	self.tableArray = nil;
	[self reloadData];
}



-(void) styleSelected:(NSNotification*) myNotification{
	
	
	self->cellHeight = 30;
	self->keyType = [[myNotification userInfo] valueForKey:@"keyType"];
	self->keyOne = [[myNotification userInfo] valueForKey:@"keyOne"];
	self->keyTwo = [[myNotification userInfo] valueForKey:@"keyTwo"];
	self->tableArray = nil;
	self->listType =[[myNotification userInfo] valueForKey:@"listType"];
	
	if ([self->keyType isEqualToString:@"webColor"]){
		
		self.tableArray = [self->myAppManager.CWCColorDictionary allKeys];
		[self reloadData];
		
		for(int i = 0 ; i < [tableArray count]; i++){
			
			if([[self->tableArray objectAtIndex:i] isEqualToString:keyOne]){
				
				NSIndexPath *indexPath = [NSIndexPath indexPathWithIndex:i];
				[self selectRow:i byExtendingSelection:NO];
				//				[self selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:NSTableView];
				indexPath = nil;
			}
			
		}
	}
	
	
	else{
		self->tableArray = nil;
		[self reloadData];
		
	}
}


- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
	
	return 	self->cellHeight;
	
}


- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	
	return [self.tableArray count];
	
}



- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	
	NSTableCellView *cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];
	
	
	NSTextField* listText = (NSTextField*) [cellView viewWithTag:110];
	listText.stringValue = [self.tableArray objectAtIndex:row] ;

	NSColorWell *colorButton = (NSColorWell*) [cellView viewWithTag:111];
	
	NSButton *coloredButton = (NSButton*)[cellView viewWithTag:114];
	[[coloredButton cell] setBackgroundColor:[NSColor redColor]];
	
	
//		CIColor *red = [CIColor colorWithRed:1 green:0 blue:0];
//		CIImage *redImage = [CIImage imageWithColor:red];
		
//		NSCIImageRep *ir = [NSCIImageRep imageRepWithCIImage:redImage];
		
	colorButton.layer.backgroundColor =[NSColor redColor].CGColor;
	
	
	NSString *colorValue = [[[self->myAppManager.CWCColorDictionary objectForKey:@"expanded"] valueForKey:listText.stringValue] uppercaseString];
	
	
	
	NSTextField *hexColorTextfield = (NSTextField*) [cellView viewWithTag:112];
	
	hexColorTextfield.delegate = self;
	
	[hexColorTextfield setStringValue:colorValue];
	
	[colorButton setColor:[NSColor colorWithHexString:[[self->myAppManager.CWCColorDictionary objectForKey:@"expanded"] valueForKey:listText.stringValue  ]]];
			
	return cellView;
}


-(void)tab{
	
	
}

-(void) textDidEndEditing:(NSNotification *)notification{
	

	NSInteger selected = [self selectedRow];
	
    // Get row at specified index
    NSTableCellView *selectedRow = [self viewAtColumn:0 row:selected makeIfNecessary:YES];

	NSTextField* selectedRowName = (NSTextField*) [selectedRow viewWithTag:110];
	NSTextField* selectedRowColorValue = (NSTextField*) [selectedRow viewWithTag:112];
//
//	NSLog(@"ended editing:%@ the selected row:%li !!",selectedRowTextField.stringValue ,[self	selectedRow]);
	
	[[self->myAppManager.CWCColorDictionary objectForKey:@"expanded"] setValue:selectedRowColorValue.stringValue forKey:selectedRowName.stringValue];
	[self reloadData];
	
	
	
//    // Focus on text field to make it auto-editable
//    [[self window] makeFirstResponder:selectedRowTextField];
//	
//    // Set the keyboard carat to the beginning of the text field
//    [[selectedRowTextField currentEditor] setSelectedRange:NSMakeRange(0, 0)];
//	
//	[[self window] makeFirstResponder:nil];
}


- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
    NSInteger selectedRow = [self selectedRow];
	NSLog(@"I'm the selected row:%li",selectedRow);

	
}

- (BOOL)textShouldEndEditing:(NSText *)aTextObject{
	
	
		NSLog(@"did ended editing:%@ the selected row:%li !!", aTextObject.string ,[self selectedRow]);
	
	
	return true;
}




@end
