//
//  colorListWindowController.h
//  CostcoMobilityContentManager
//
//  Created by Thomas Fessler on 6/23/14.
//
//

#import <Cocoa/Cocoa.h>
#import "colorFamilyTableView.h"
#import "colorItemsTableView.h"

@interface colorListWindowController : NSWindowController{
	
	colorFamilyTableView *_colorFamilyList;
	colorItemsTableView *_colorItemList;
	
}

@property (strong) IBOutlet colorFamilyTableView *colorFamilyList;
@property (strong) IBOutlet colorItemsTableView *colorItemList;
@end
