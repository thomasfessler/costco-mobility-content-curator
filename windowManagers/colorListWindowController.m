//
//  colorListWindowController.m
//  CostcoMobilityContentManager
//
//  Created by Thomas Fessler on 6/23/14.
//
//

#import "colorListWindowController.h"

@interface colorListWindowController ()

@end

@implementation colorListWindowController

@synthesize colorFamilyList,colorItemList;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (IBAction)sortColorItems:(NSSegmentedControl *)sender {

	int startLocation;
	int length = 2;
	
	if(sender.selectedSegment == 0){
		startLocation = 1;
	}
	else if(sender.selectedSegment == 1){
		startLocation = 3;
	}
	else if(sender.selectedSegment == 2){
		startLocation = 5;
	}
	if(sender.selectedSegment == 3){
		startLocation = 1;
		length = 5;
	}
		
	if(sender.selectedSegment == 4){
		startLocation = -99;
		length = 5;
	}
	
	[self.colorItemList setColorCustomSortStartLocation:startLocation lengthOfSubstring:length];
	
}




-(NSString *)windowNibName {
    return @"colorListWindowController";
}

@end
