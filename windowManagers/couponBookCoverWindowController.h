//
//  dataEntryViewController.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/10/14.
//
//

#import <Cocoa/Cocoa.h>
#import "couponBookObject.h"
@interface couponBookCoverWindowController : NSWindowController{
//	NSMutableDictionary *expertise;
//	NSMutableArray *technologies;

	couponBookObject *_coverObject;
	NSTextField *_territory;

	NSButtonCell *_territoryMatrix;
	NSTextView *_couponCoverTitle;
	NSTextField *_couponCoverSavingsLine;


	NSDatePicker *_couponStartDate;
	NSDatePicker *_couponEndDate;
	
	NSTextField *_couponImageFilename;
	NSTextField *_couponImagePath;
	
	NSTextField *_colorCouponTitle;
	NSTextField *_colorDays;
	NSTextField *_colorSavings;
	NSTextField *_colorValid;
	NSTextField *_colorCouponFooter;
	
	NSTextField *_fontSizeTitle;
	NSTextField *_fontSizeDays;
	NSTextField *_fontSizeRemaining;
	NSTextField *_fontSizeSavings;
	
	NSTextField *_fontSizeValid;
@private
	NSView *_couponView;
	NSTextField *_couponViewSavingsLine;

	NSTextField *_couponViewValid;
	NSTextField *_couponViewDays;
	NSTextView *_couponViewRemaining;
	NSTextView *_couponViewTitle;
	NSView *_couponFooter;

}

@property (strong) couponBookObject *coverObject;
@property (strong) IBOutlet NSTextField *territory;
@property (strong) IBOutlet NSTextView *couponBookTitle;
@property (strong) IBOutlet NSTextField *couponCoverSavingsLine;

@property (strong) IBOutlet NSDatePicker *couponStartDate;
@property (strong) IBOutlet NSDatePicker *couponEndDate;

@property (strong) IBOutlet NSScrollView *couponInstructions;
@property (strong) IBOutlet NSTextField *couponTermsAndConditions;
@property (strong) IBOutlet NSTextField *couponViewValidString;


@property (strong) IBOutlet NSTextField *couponImageFilename;
@property (strong) IBOutlet NSTextField *couponImagePath;

@property (strong) IBOutlet NSTextField *colorDays;
@property (strong) IBOutlet NSTextField *colorSavings;
@property (strong) IBOutlet NSTextField *colorValid;
@property (strong) IBOutlet NSTextField *colorCouponTitle;
@property (strong) IBOutlet NSTextField *colorCouponFooter;

@property (strong) IBOutlet NSPopUpButton *colorFooterFamily;
@property (strong) IBOutlet NSPopUpButton *colorFooterItem;



@property (strong) IBOutlet NSTextField *fontSizeTitle;
@property (strong) IBOutlet NSTextField *fontSizeDays;
@property (strong) IBOutlet NSTextField *fontSizeRemaining;
@property (strong) IBOutlet NSTextField *fontSizeSavings;
@property (strong) IBOutlet NSTextField *fontSizeValid;

@property (strong) IBOutlet NSView *couponView;
@property (strong) IBOutlet NSTextField *couponViewSavingsLine;
@property (strong) IBOutlet NSTextField *couponViewDays;
@property (strong) IBOutlet NSTextView *couponViewRemaining;
@property (strong) IBOutlet NSTextView *couponViewTitle;
@property (strong) IBOutlet NSTextField *couponViewValid;
@property (strong) IBOutlet NSView *couponFooter;
@property (strong) IBOutlet NSButtonCell *territoryMatrix;
@end
