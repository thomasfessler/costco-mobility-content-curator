//
//  dataEntryViewController.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/10/14.
//
//

#import "couponBookCoverWindowController.h"
#import "CWCAppManager.h"
#import "NSColor+hexcolor.h"


@interface couponBookCoverWindowController (){
	
	CWCAppManager *appManager;
}

@end

@implementation couponBookCoverWindowController

@synthesize couponCoverSavingsLine,couponBookTitle,couponEndDate,couponStartDate,couponImageFilename,couponImagePath,couponView,couponViewSavingsLine,couponViewTitle,couponViewValidString,colorCouponTitle,colorDays,colorSavings,colorValid,fontSizeDays,fontSizeRemaining,fontSizeSavings,fontSizeTitle,couponViewDays,couponViewRemaining,fontSizeValid,colorCouponFooter,couponInstructions,couponFooter, colorFooterFamily, colorFooterItem, couponTermsAndConditions,couponViewValid,coverObject;

-(NSString *)windowNibName {
    return @"couponBookCoverWindowController";
}


- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)windowDidLoad {
    [super windowDidLoad];
	
	[self loadDataFromContext:self.coverObject];
	if(self.coverObject != nil){
		[self updatePageLayoutWithCouponContext:self.coverObject];
	}
}


-(void) awakeFromNib{

	if(self.coverObject == nil){
	self->appManager = [CWCAppManager sharedAppManager];
	
	[self.colorFooterFamily removeAllItems];
	[self.colorFooterItem removeAllItems];
//	self.colorFooterFamily = [[NSPopUpButton alloc] init];
	[self.colorFooterFamily addItemsWithTitles:[self->appManager.CWCColorDictionary allKeys]];
	
	[self.colorFooterItem addItemsWithTitles:[[self->appManager.CWCColorDictionary objectForKey:@"expanded"] allKeys] ];
	
	
	
	
	[self.colorFooterFamily synchronizeTitleAndSelectedItem];
	
	if(self.coverObject != nil){
	self.coverObject = self->appManager.couponCoverObject;
	}
	
	//setup view backgrounds
	CALayer *viewLayer = [CALayer layer];
	[viewLayer setBackgroundColor:CGColorCreateGenericRGB(0.0, 0.0, 0.0, 0.4)];
	[self.couponView setWantsLayer:YES];
	[self.couponView setLayer:viewLayer];
	self.couponView.layer.borderColor =[NSColor blackColor].CGColor;
	self.couponView.layer.borderWidth = 1.5;
	self.couponView.layer.cornerRadius = 3;
	
	CALayer *footerViewLayer = [CALayer layer];
	[footerViewLayer setBackgroundColor:CGColorCreateGenericRGB(0.0, 0.0, 0.0, 0.2)];
	[self.couponFooter setWantsLayer:YES];
	[self.couponFooter setLayer:footerViewLayer];

	[self.couponViewTitle setTextContainerInset:NSMakeSize(0.0f, 5.0f)];
	[self.couponViewRemaining setTextContainerInset:NSMakeSize(0.0f, 5.0f)];
	
	if(self->appManager.couponCoverObject == nil){
		self->appManager.couponCoverObject = [[couponBookObject alloc] init];
	}
	
	self.coverObject = self->appManager.couponCoverObject;
	}
	

}


-(void) loadDataFromContext:(couponBookObject*)coverCtx{

	
	if(coverCtx.colorFooter != nil){
		self.couponFooter.layer.backgroundColor = [NSColor colorWithHexString:coverCtx.colorFooter].CGColor;
		self.colorCouponFooter.stringValue = coverCtx.colorFooter;
	}
	
	if(coverCtx.couponBookTitle != nil){
	self.couponBookTitle.string = coverCtx.couponBookTitle;
	}
	if(coverCtx.couponBookSavingsText != nil){
		self.couponCoverSavingsLine.stringValue = coverCtx.couponBookSavingsText;
	}
	if(coverCtx.couponBookStartDate != nil){
		self.couponStartDate.dateValue = coverCtx.couponBookStartDate;
	}
	if(coverCtx.couponBookEndDate != nil){
		self.couponEndDate.dateValue = coverCtx.couponBookEndDate;
	}
	if(coverCtx.couponBookImageFilename != nil){
		self.couponImageFilename.stringValue = coverCtx.couponBookImageFilename;
	}
	if(coverCtx.couponImagePath != nil){
		self.couponImagePath.stringValue = coverCtx.couponImagePath;
	}
	if(coverCtx.couponBookSavingsText != nil){
		self.couponViewSavingsLine.stringValue = coverCtx.couponBookSavingsText;
	}
	if(coverCtx.couponBookValidText != nil){
		self.couponViewValidString.stringValue = coverCtx.couponBookValidText;
	}
	
	if(coverCtx.couponBookTitleColor != nil){
		self.colorCouponTitle.stringValue = coverCtx.couponBookTitleColor;
	}
	
	if(coverCtx.colorDays != nil){
		self.colorDays.stringValue = coverCtx.colorDays;
	}
	
	if(coverCtx.colorSavings != nil){
		self.colorSavings.stringValue = coverCtx.colorSavings;
	}
	
	if(coverCtx.colorValid != nil){
		self.colorValid.stringValue = coverCtx.colorValid;
	}


//	self.fontSizeDays.stringValue = [NSString stringWithFormat:@"%i",coverCtx.fontSizeDays];
//	self.fontSizeRemaining.stringValue = [NSString stringWithFormat:@"%i",coverCtx.fontSizeRemaining];
//	self.fontSizeSavings.stringValue = [NSString stringWithFormat:@"%i",coverCtx.fontSizeSavings];
	self.fontSizeTitle.stringValue = [NSString stringWithFormat:@"%i",coverCtx.fontSizeTitle];
//	self.fontSizeValid.stringValue = [NSString stringWithFormat:@"%i",coverCtx.fontSizeValid];

}


- (IBAction)saveChanges:(id)sender {
	
	[self saveDataToContext:self.coverObject];
	
}
- (IBAction)exportTest:(id)sender {
	
	[self->appManager saveJSON];
	
//	NSArray* exportArray = [self->appManager createArrayForExport];
//	
//	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:exportArray
//                                                       options:0
//                                                         error:nil];
//    NSString *jsonString;
//    if(jsonData){
//        jsonString = [[NSString alloc] initWithBytes:[jsonData bytes]
//											  length:[jsonData length]
//											encoding:NSUTF8StringEncoding];
//    }
//	NSLog(@"\n%@",jsonString);

}

-(void) saveDataToContext:(couponBookObject*)coverCtx{
	
	
	if(self.colorCouponFooter.stringValue != nil){
		coverCtx.colorFooter = self.colorCouponFooter.stringValue;
	}
	
	if(self.couponBookTitle.string != nil){
		coverCtx.couponBookTitle = self.couponBookTitle.string;
	}

	if(self.couponCoverSavingsLine.stringValue != nil){
		 coverCtx.couponBookSavingsText = self.couponCoverSavingsLine.stringValue;
	}

	if(self.couponStartDate.dateValue != nil){
		coverCtx.couponBookStartDate = self.couponStartDate.dateValue;
	}

	if(self.couponEndDate.dateValue != nil){
		coverCtx.couponBookEndDate = self.couponEndDate.dateValue;
	}
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
		
	
	if(self.couponImageFilename.stringValue != nil){
		 coverCtx.couponBookImageFilename = self.couponImageFilename.stringValue;
	}
	if(self.couponImagePath.stringValue != nil){
		coverCtx.couponImagePath = self.couponImagePath.stringValue;
	}
	
	if(self.colorCouponTitle.stringValue != nil){
		coverCtx.couponBookTitleColor = self.colorCouponTitle.stringValue;
	}
	
	if(self.colorDays.stringValue != nil){
		coverCtx.colorDays = self.colorDays.stringValue;
	}
	
	if(self.colorSavings.stringValue != nil){
		coverCtx.colorSavings = self.colorSavings.stringValue;
	}
	
	if(self.colorValid.stringValue != nil){
		coverCtx.colorValid= self.colorValid.stringValue;
	}
	
	if(self.fontSizeDays.stringValue != nil){
		coverCtx.fontSizeDays = self.fontSizeDays.integerValue;
	}
	
	if(self.fontSizeRemaining.stringValue != nil){
		coverCtx.fontSizeRemaining = self.fontSizeRemaining.integerValue;
	}
	if(self.fontSizeSavings.stringValue != nil){
		coverCtx.fontSizeSavings  = self.fontSizeSavings.integerValue;
	}
	if(self.fontSizeTitle.stringValue != nil){
		coverCtx.fontSizeTitle = self.fontSizeTitle.integerValue;
	}
	if(self.fontSizeValid.stringValue != nil){
		coverCtx.fontSizeValid = self.fontSizeValid.integerValue;
	}
	
	[coverCtx updateCoverObject:coverCtx withLanguageDict:[self->appManager.CWCAppTextDictionary valueForKey:self->appManager.currentLanguage]];
	
	[self updatePageLayoutWithCouponContext:coverCtx];

	//TODO: move save function to the app manager
	[self->appManager saveCouponCover:coverCtx withFilename:@"couponCover.data"];
	[self->appManager saveCouponArray:self->appManager.couponObjectArray withFilename:@"couponArray.data"];
	
}

-(void) updatePageLayoutWithCouponContext:(couponBookObject*)coverCtx{
	
	couponView.layer.contents = (id)[NSImage imageNamed:self.couponImageFilename.stringValue];
	[self.couponView setNeedsDisplay:true];
	
	if (self->appManager.couponCoverObject.styledCoverTitle != nil){
	[[self.couponViewTitle textStorage] setAttributedString:self->appManager.couponCoverObject.styledCoverTitle];
		[[self.couponViewTitle layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0, [[self.couponViewTitle textStorage] length])];
	}
	[self.couponViewTitle display];
	
	self.couponViewDays.attributedStringValue = coverCtx.styledNumDays;
	
	if (self->appManager.couponCoverObject.styledDaysRemainding != nil){
	[[self.couponViewRemaining textStorage] setAttributedString:self->appManager.couponCoverObject.styledDaysRemainding];
		[[self.couponViewRemaining layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0, [[self.couponViewRemaining textStorage] length])];}
	[self.couponViewRemaining display];
	
	self.couponViewSavingsLine.attributedStringValue = coverCtx.styledSavingsLine;
	self.couponViewValid.attributedStringValue = coverCtx.styledValidString;
	
	self.couponFooter.layer.backgroundColor = [NSColor colorWithHexString:coverCtx.colorFooter].CGColor;
}




@end
