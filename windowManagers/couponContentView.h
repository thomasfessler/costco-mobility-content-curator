//
//  newMenuWIndowController.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/11/14.
//
//

#import <Cocoa/Cocoa.h>
#import "couponObjectFactory.h"
#import "CWCAppManager.h"

@interface couponContentView : NSView{
	NSTextField *_instantSavings;
	NSTextView *_itemLimit;
	NSTextField *_upTo;
	NSTextField *_off;
	NSTextField *_dollar;
	NSTextField *_cents;
	NSTextField *_comBug;
	NSTextView *_bullets;
	NSTextView *_count;
	NSTextView *_itemNumbers;
	NSTextView *_selectionVaries;
	NSTextField *_itemPrefix;
	NSTextField *_imageFilename;
	NSTextField *_dotComURL;
	NSTextField *_xtraPricingInfo;
	NSMatrix *_showDotCom;
	NSMatrix *_showBookAppRequired;
	NSTextField *_cellHeight;
	NSMatrix *_showUpTo;
	NSTextField *_styledAndOr;
	NSTextField *_styledOr;
	NSTextField *_styledChooseFrom;
	NSTextField *_productWebAddress;
	
	NSTextView *_productName;
	
		CWCAppManager *myAppManager;
	
	
}


@property (weak) IBOutlet NSArrayController *arrayController;

//product name displayed - ex. Keurig® Platinum Brewer
@property (strong) IBOutlet NSTextView *productName;

//text displayed in the instant savings bock - ex. Instant Savings
@property (strong) IBOutlet NSTextField *instantSavings;

//text displayed in the item limit block - ex. LIMIT 5

@property (strong) IBOutlet NSDatePicker *startDate;
@property (strong) IBOutlet NSDatePicker *EndDate;

@property (strong) IBOutlet NSTextView *itemLimit;

@property (strong) IBOutlet NSTextField *upTo;
@property (strong) IBOutlet NSTextField *dollar;
@property (strong) IBOutlet NSTextField *cents;
@property (strong) IBOutlet NSTextField *comBug;

@property (strong) IBOutlet NSTextView *bullets;
@property (strong) IBOutlet NSTextView *count;
@property (strong) IBOutlet NSTextView *itemNumbers;
@property (strong) IBOutlet NSTextView *selectionVaries;
@property (strong) IBOutlet NSButton *buttonSave;
@property (strong) IBOutlet NSTextField *itemPrefix;
@property (strong) IBOutlet NSTextField *off;
@property (strong) IBOutlet NSTextField *imageFilename;
@property (strong) IBOutlet NSTextField *dotComURL;
@property (strong) IBOutlet NSTextField *xtraPricingInfo;
@property (strong) IBOutlet NSMatrix *showDotCom;
@property (strong) IBOutlet NSMatrix *showBookAppRequired;

@property (strong) IBOutlet NSTextField *cellHeight;
@property (strong) IBOutlet NSMatrix *showUpTo;
@property (strong) IBOutlet NSButton *showDateBannerOverride;


@property (strong) IBOutlet NSTextField *styledAndOr;
@property (strong) IBOutlet NSTextField *styledOr;
@property (strong) IBOutlet NSTextField *styledChooseFrom;
@property (strong) IBOutlet NSTextField *imageURL;
@property bool loaded;


@end
