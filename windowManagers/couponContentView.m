//
//  newMenuWIndowController.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/11/14.
//
//

#import "couponContentView.h"
#import "CWCAppManager.h"



@implementation couponContentView
@synthesize  dollar, loaded, count,dotComURL,comBug,cents,selectionVaries,showDotCom,itemNumbers,upTo,itemPrefix,instantSavings,imageFilename,itemLimit,bullets,buttonSave,off,productName,xtraPricingInfo,showBookAppRequired,styledAndOr,styledChooseFrom,styledOr,imageURL,cellHeight,showUpTo;

//-(NSString *)windowNibName {
//    return @"couponContentEditor";
//}



- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		self->myAppManager = [CWCAppManager sharedAppManager];

        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}




- (IBAction)showComBug:(NSMatrix *)sender {

	
}


- (void)awakeFromNib
{

    if(self.loaded == false){
		
		self.loaded = true;
		[self.showDotCom setState:TRUE atRow:1 column:0];
	
	
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWebView) name:@"updateWebView" object:nil];
		
		CALayer *viewLayer = [CALayer layer];
		[viewLayer setBackgroundColor:CGColorCreateGenericRGB(.9, .9,.9, 1.0)];
		[self setWantsLayer:YES];
		[self setLayer:viewLayer];
		self.layer.borderColor =[NSColor blackColor].CGColor;
		self.layer.borderWidth = 1.5;
		self.layer.cornerRadius = 3;
		
		[self updateWebView];
		
	}
}

-(void) updateWebView{
	
	[self updateScreenWithContext:myAppManager.currentCouponObject];
	
}



-(void) updateScreenWithContext:(couponObjectFactory*)ctxObject{
	
	if(ctxObject.comBug != nil)
	self.comBug.stringValue = ctxObject.comBug;
	else{
		self.comBug.stringValue = @"";
	}
	
	[self.showDotCom setState:TRUE atRow:ctxObject.showDotComLink column:0];
	[self.showBookAppRequired setState:TRUE atRow:ctxObject.showBookAppRequired column:0];
	[self.showUpTo setState:TRUE atRow:ctxObject.showUpTo column:0];
	[self.showDateBannerOverride setState:ctxObject.showCouponBannerOverrride];
	
	if(ctxObject.xtraPricingInfo != nil)
		self.xtraPricingInfo.stringValue = ctxObject.xtraPricingInfo;
	else{
		self.xtraPricingInfo.stringValue = @"";
	}
	
	
	if(ctxObject.instantSavings != nil)
	self.instantSavings.stringValue = ctxObject.instantSavings;
	else{
		self.instantSavings.stringValue = @"";
	}

	if(ctxObject.upTo != nil)
		self.upTo.stringValue = ctxObject.upTo;
	else{
		self.upTo.stringValue = @"";
	}
	
	
	if(ctxObject.itemLimit != nil)
	self.itemLimit.string = ctxObject.itemLimit;
	else{
		self.itemLimit.string = @"";
	}
	
	if(ctxObject.dollar != nil)
	self.dollar.stringValue = ctxObject.dollar;
	else{
		self.dollar.stringValue = @"";
	}
	
	if(ctxObject.cents != nil)
	self.cents.stringValue = ctxObject.cents;
	else{
		self.cents.stringValue = @"";
	}

	
	if(ctxObject.off
	   != nil)
	self.off.stringValue = ctxObject.off;
	else{
		self.off.stringValue = @"";
	}
	
	if(ctxObject.itemPrefix != nil)
	self.itemPrefix.stringValue = ctxObject.itemPrefix;
	else{
		self.itemPrefix.stringValue = @"";
	}
	
	if(ctxObject.itemLimit != nil)
	self.itemLimit.string = ctxObject.itemLimit;
	else{
		self.itemLimit.string = @"";
	}
	
	
	if(ctxObject.imageFilenameMobile != nil)
		self.imageFilename.stringValue = ctxObject.imageFilenameMobile;
	else{
		self.imageFilename.stringValue = @"";
	}
	

	if(ctxObject.dotComURL != nil)
		self.dotComURL.stringValue = ctxObject.dotComURL;
	else{
		self.dotComURL.stringValue = @"";
	}

	
	if(ctxObject.comBug != nil)
		self.comBug.stringValue = ctxObject.comBug;
	else{
		self.comBug.stringValue = @"";
	}
	
	if(ctxObject.productName != nil)
	self.productName.string = ctxObject.productName;
	
	else{
		self.productName.string = @"";
	}
	
	
	if(ctxObject.productDescriptionBulletText != nil)
	self.bullets.string = ctxObject.productDescriptionBulletText;
	else{
		self.bullets.string = @"";
	}
	
	
	if(ctxObject.productDescriptionCountText != nil)
	self.count.string = ctxObject.productDescriptionCountText;
	else{
		self.count.string = @"";
	}
	
	
	if(ctxObject.itemNumbers!= nil)
	self.itemNumbers.string = ctxObject.itemNumbers;
	else{
		self.itemNumbers.string = @"";
	}
	
	
	if(ctxObject.selectionVaries != nil)
	self.selectionVaries.string = ctxObject.selectionVaries;
	else{
		self.selectionVaries.string = @"";
	}
	
	self.cellHeight.stringValue = [NSString stringWithFormat:@"%i",ctxObject.cellHeight];
	
	if(ctxObject.styleAndOr != nil)
		self.styledAndOr.stringValue = ctxObject.styleAndOr;
	else{
		self.styledAndOr.stringValue = @"";
	}
	
	
	if(ctxObject.styleOr != nil)
		self.styledOr.stringValue = ctxObject.styleOr;
	else{
		self.styledOr.stringValue = @"";
	}

	if(ctxObject.styleChooseFrom != nil)
		self.styledChooseFrom.stringValue = ctxObject.styleChooseFrom;
	else{
		self.styledChooseFrom.stringValue = @"";
	}
	
	
	if(ctxObject.imageURL != nil)
		self.imageURL.stringValue = ctxObject.imageURL;
	else{
		self.imageURL.stringValue = @"";
	}
	
	
	if(ctxObject.couponStartDate != nil){
		self.startDate.dateValue = ctxObject.couponStartDate;
	}
	if(ctxObject.couponEndDate != nil){
		self.EndDate.dateValue = ctxObject.couponEndDate;
	}

	
	
}



-(void) saveUpdateToContext:(couponObjectFactory*)ctxObject{
	
	
	ctxObject.showDotComLink = (bool)self.showDotCom.selectedRow;
	ctxObject.showBookAppRequired = (bool)self.showBookAppRequired.selectedRow;
	ctxObject.showUpTo = (bool)self.showUpTo.selectedRow;
	ctxObject.showCouponBannerOverrride = (bool)self.showDateBannerOverride.state;
	if(self.startDate.dateValue != nil){
		ctxObject.couponStartDate = self.startDate.dateValue;
	}
	if(self.EndDate.dateValue != nil){
		ctxObject.couponEndDate = self.EndDate.dateValue;
	}
	
	if(self.cellHeight.stringValue != nil)
		ctxObject.cellHeight = [self.cellHeight.stringValue intValue];
	
	if(self.comBug.stringValue != nil)
		 ctxObject.comBug = self.comBug.stringValue;
	
	if(self.xtraPricingInfo.stringValue != nil)
		ctxObject.xtraPricingInfo = self.xtraPricingInfo.stringValue;
	
	if(self.instantSavings.stringValue != nil)
		ctxObject.instantSavings =   self.instantSavings.stringValue ;
	
	if(self.dollar.stringValue != nil)
		 ctxObject.dollar = self.dollar.stringValue;
	
	if(self.cents.stringValue != nil)
		ctxObject.cents = self.cents.stringValue;
	
	if(self.off.stringValue != nil)
		ctxObject.off = self.off.stringValue;
	
	if(self.itemPrefix.stringValue!= nil)
		ctxObject.itemPrefix = self.itemPrefix.stringValue;
	
	if(self.itemLimit.string != nil)
		 ctxObject.itemLimit =  self.itemLimit.string;
	
	if(self.imageFilename.stringValue != nil)
		ctxObject.imageFilenameMobile = self.imageFilename.stringValue ;
	
	
	if(self.dotComURL.stringValue != nil)
		ctxObject.dotComURL = self.dotComURL.stringValue;
	
	if(self.comBug.stringValue != nil)
		ctxObject.comBug = self.comBug.stringValue;
	
	if(self.productName.string != nil)
		ctxObject.productName =  self.productName.string;

	if(self.bullets.string != nil)
		ctxObject.productDescriptionBulletText = self.bullets.string;
	
	if(self.count.string!= nil)
		ctxObject.productDescriptionCountText =  self.count.string;
	
	if(self.itemNumbers.string != nil)
		ctxObject.itemNumbers =  self.itemNumbers.string;
	
	if(self.selectionVaries.string != nil)
		ctxObject.selectionVaries =  self.selectionVaries.string ;
	
	if(self.styledAndOr.stringValue != nil)
		ctxObject.styleAndOr = self.styledAndOr.stringValue;
	else{
		ctxObject.styleAndOr = @"";
	}
	
	
	if(self.styledOr.stringValue != nil)
		ctxObject.styleOr = self.styledOr.stringValue;
	else{
		ctxObject.styleOr = @"";
	}
	
	if(self.styledChooseFrom.stringValue != nil)
		ctxObject.styleChooseFrom = self.styledChooseFrom.stringValue;
	else{
		ctxObject.styleAndOr = @"";
	}
	
	
	
	if(ctxObject.imageURL != nil)
		self.imageURL.stringValue = ctxObject.imageURL;
	else{
		self.imageURL.stringValue = @"";
	}
	
	
	

	[ctxObject buildCouponTextWithLanguageDictionary:[self->myAppManager.CWCAppTextDictionary objectForKey:self->myAppManager.currentLanguage] andCoverObject:self->myAppManager.couponCoverObject];
	[ctxObject checkCouponForErrorsWithLocalizationDict: [self->myAppManager.CWCAppTextDictionary objectForKey:self->myAppManager.currentLanguage]];
	
	
	[ctxObject prerenderAttributedTextForCouponStyle:0];
	[ctxObject prerenderAttributedTextForCouponStyle:1];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"updateWebView" object:nil userInfo:nil];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadList" object:nil userInfo:nil];

}


-(IBAction)checkboxDetailPressed:(NSButton*)sender{
	
		self->myAppManager.currentCouponObject.showCouponBannerOverrride = sender.state ;
		[self saveUpdateToContext:self->myAppManager.currentCouponObject];
}


- (IBAction)exportTest:(id)sender {
	
	[myAppManager saveJSON];

	
}



- (IBAction)actionSaveChanges:(id)sender {
	
	[self saveUpdateToContext:myAppManager.currentCouponObject];
		
}
- (IBAction)actionRevertLast:(id)sender {
	
}
- (IBAction)actionRevertOriginal:(id)sender {
	
}

@end
