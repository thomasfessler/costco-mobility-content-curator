//
//  dataEntryTableView.h
//  TableViewPlayground
//
//  Created by Thomas Fessler on 6/10/14.
//
//

#import <Cocoa/Cocoa.h>
#import "CWCAppManager.h"
#import "dataEntryTextfieldCell.h"

@interface dataEntryTableView : NSTableView{
    NSNumber *selectedCellRowIndex;
    NSNumber *selectedCellColumnIndex;
    NSCell *customSelectedCell;



	
}
@property (nonatomic, retain) NSNumber *selectedCellRowIndex;
@property (nonatomic, retain) NSNumber *selectedCellColumnIndex;
@property (nonatomic, retain) NSCell *customSelectedCell;


@property CWCAppManager *myAppManager;

@end
