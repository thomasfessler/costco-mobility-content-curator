//
//  editWebSourcesWindowController.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/12/14.
//
//

#import "editWebSourcesWindowController.h"

@interface editWebSourcesWindowController ()

@end

@implementation editWebSourcesWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
