/*

  
*/

#import <Cocoa/Cocoa.h>
#import "CWCAppManager.h"

@interface mvmControlCenterViewController : NSWindowController<NSTableViewDelegate, NSTableViewDataSource> {
@private
	CWCAppManager *_myAppManager;
    // An array of dictionaries that contain the contents to display
    NSMutableArray *_tableContents;
    IBOutlet NSTableView *_tableView;
	NSButton *_reloadProjectData;
	NSButton *_exportMobileJSON;
	NSButton *_exportMobileHTML;
}
@property (strong) CWCAppManager* myAppManager;
@property (strong) IBOutlet NSButton *reloadProjectData;
@property (strong) IBOutlet NSButton *exportMobileJSON;
@property (strong) IBOutlet NSButton *exportMobileHTML;
@end
