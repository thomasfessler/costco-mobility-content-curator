/*

*/

#import <Cocoa/Cocoa.h>
#import "mvmCouponTextView.h"
#import "CWCAppManager.h"

//@class CMColorView;

@interface mvmCouponListCellView : NSTableCellView {

	
	IBOutlet NSButton *editButton;
	
	IBOutlet NSScrollView *couponTextScrollView;
	IBOutlet mvmCouponTextView *_mvmTextView;

	IBOutlet NSImageView *couponImageView;
    BOOL _isSmallSize;
}

@property (strong, nonatomic) couponObjectFactory* couponObject;
@property (strong, nonatomic) IBOutlet NSScrollView *couponTextScrollView;
@property (strong, nonatomic) IBOutlet NSImageView *couponImageView;

@property (strong, nonatomic) IBOutlet NSImageView *couponWarningViewImage;

@property (strong, nonatomic) IBOutlet NSImageView *couponWarningViewText;

@property 	IBOutlet NSButton *editButton;

@property (assign) IBOutlet	mvmCouponTextView *mvmTextView;

@property (strong) IBOutlet	NSTextField *couponValidBanner;


@property (strong, nonatomic) IBOutlet NSImageView *availableDotCom;

-(void) updateCellLayoutWithContext:(CWCAppManager*)appManager andLocalArray:(NSArray*)localArray forIndex:(int)cellIndex;


@end
