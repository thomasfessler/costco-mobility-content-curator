/*

 */

#import "mvmCouponListCellView.h"
#import "CWCAppManager.h"
@implementation mvmCouponListCellView

@synthesize  couponImageView, availableDotCom,mvmTextView, couponTextScrollView, couponWarningViewImage,couponObject, couponWarningViewText, editButton,couponValidBanner;

-(IBAction)editImage:(id)sender{
	
		NSLog(@"blarg down");
	
	NSString *path = [NSString stringWithFormat:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/%@",self.couponObject.imageFilenameMobile];
	
	[[NSWorkspace sharedWorkspace] openFile:path withApplication:@"Adobe Photoshop Elements Editor"];
	
}

-(void) mouseDown:(NSEvent *)theEvent{
	
	NSLog(@"mouse down");
}

-(void) updateCellLayoutWithContext:(CWCAppManager*)appManager andLocalArray:(NSArray*)localArray forIndex:(int)cellIndex{
		
	
	self.couponValidBanner.hidden = true;
	
	
	
	couponObjectFactory* myFactory = [[couponObjectFactory alloc] init];
	myFactory = [localArray objectAtIndex:cellIndex];
	
	if(myFactory.showCouponBanner == true){
		self.couponValidBanner.hidden = false;
		self.couponValidBanner.stringValue = myFactory.couponBannerText;
	}
	self.couponObject = myFactory;
	
	if(myFactory.couponType == 1){
		
	[self.mvmTextView setCouponAttributedString:myFactory.couponTextListAttributed];


	int cellHeight = self.frame.size.height;
	int textHeight = self.mvmTextView.frame.size.height;
	int imageHeight = self.couponImageView.frame.size.height;
	
	int position = (cellHeight - textHeight)/2;
//	int imagePosition = (cellHeight = imageHeight) /2;
	

		
	[self.mvmTextView.layer setPosition:CGPointMake(111,position)];

	}


		self.layer.backgroundColor = [NSColor whiteColor].CGColor;
	self.mvmTextView.backgroundColor = [NSColor clearColor];
	
	if(myFactory.errorState == true){

//		self.layer.backgroundColor = [NSColor redColor].CGColor;
		for(NSDictionary* errors in myFactory.errorArray){
			
			if([errors valueForKeyPath:@"errorImageName"] != nil ||[errors valueForKeyPath:@"errorImagePath"] || [errors valueForKeyPath:@"errorImageWidth"] ){
				
				self.couponWarningViewImage.hidden = false;
			}
			
			

			if([errors valueForKeyPath:@"errorProductNameLength"] != nil ){
				
				self.couponWarningViewText.hidden = false;
			}

			
			if([errors valueForKeyPath:@"errorChooseFrom"] != nil ){
				
				self.couponWarningViewText.hidden = false;
			}
			
		}
		
	}else{

		
		self.couponWarningViewImage.hidden = true;
		self.couponWarningViewText.hidden = true;
	}
	
	//TODO: TF file location is hard coded here
	if(myFactory.imageFilenameMobile != nil){
		self.imageView.image = [NSImage imageNamed:myFactory.imageFilenameMobile];
		NSString *fileName = @"/Users/tfessler/costco-mobility-content-curator/mvmImages/";
		fileName = [fileName stringByAppendingString:myFactory.imageFilenameMobile];
		self.couponImageView.image = [[NSImage alloc]initWithContentsOfFile:fileName];
	}
}







@end
