//
//  CWCmvmTableView.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/6/14.
//
//

#import <Cocoa/Cocoa.h>
#import "CWCAppManager.h"

@interface mvmCouponListTableView : NSTableView<NSTableViewDataSource,NSTableViewDelegate>

@property (weak) CWCAppManager *appManager;
@property NSMutableArray *localArray;


@end
