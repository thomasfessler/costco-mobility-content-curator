//
//  CWCmvmTableView.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/6/14.
//
//

#import "mvmCouponListTableView.h"
#import "mvmCouponListCellView.h"
#import "CMColorView.h"
@implementation mvmCouponListTableView
@synthesize appManager, localArray;

- (instancetype)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

-(void) awakeFromNib{
	if(self.localArray == nil){
	
	self->appManager = [CWCAppManager sharedAppManager];
	self.delegate = self;
	self.dataSource = self;
	
	[self reloadList];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadList) name:@"reloadList" object:nil];
	}
	

}

-(void) reloadList{
	
	self.localArray = nil;
	self.localArray = [[NSMutableArray alloc] init];
	for(couponObjectFactory* arrayObject in self.appManager.couponObjectArray){
		

		if(self->appManager.currentSortMode == sortAll){
				[self.localArray addObject:arrayObject];
			
		}
		
		
		if(self->appManager.currentSortMode == sortAllErrors){
			if(arrayObject.errorState == true){
				[self.localArray addObject:arrayObject];
			}
			
		}
		if(self->appManager.currentSortMode == sortNoErrors){
			if(arrayObject.errorState == false && (arrayObject.approvedWeb == false || arrayObject.approvedList == false || arrayObject.approvedDetail == false)){
				[self.localArray addObject:arrayObject];
			}
		}

		if(self->appManager.currentSortMode == sortProofed){
			if(arrayObject.approvedFinal==false &&  arrayObject.approvedWeb == true && arrayObject.approvedList == true && arrayObject.approvedDetail == true){
				[self.localArray addObject:arrayObject];
			}
		}

		if(self->appManager.currentSortMode == sortApproved){
			if(arrayObject.approvedFinal==true && arrayObject.approvedWeb == true && arrayObject.approvedList == true && arrayObject.approvedDetail == true){
				[self.localArray addObject:arrayObject];
			}
		}
	}
	
	int rowToSelect = self.appManager.mvmCouponIndex;
	if (rowToSelect > localArray.count){
		rowToSelect = localArray.count;
	}
	
	[self reloadData];

	NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:rowToSelect];
	[self selectRowIndexes:indexSet byExtendingSelection:YES];
	[self selectRowIndexes:indexSet byExtendingSelection:YES];
	
	couponObjectFactory* myFactoryCoupon = [self.localArray objectAtIndex:rowToSelect];
	self.appManager.currentCouponObject = myFactoryCoupon;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"updateWebView" object:nil userInfo:nil];
}



- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
	
	
	mvmCouponListCellView *cellView ;
	

	couponObjectFactory* currentCoupon = [self.localArray objectAtIndex:row];
	
	if(currentCoupon.couponType != 2){
	
	
//	if( row > 0 && row < self.localArray.count-1){
		cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];
		
		cellView.editButton.hidden = true;
		
		[cellView updateCellLayoutWithContext:appManager andLocalArray:self.localArray forIndex:row];
//		[cellView updateCellLayoutWithContext:appManager forIndex:(int)row];
		int cellHeight = cellView.frame.size.height;
		int textHeight = cellView.mvmTextView.frame.size.height;
//		int imageHeight = cellView.couponImageView.frame.size.height;

		int position = (cellHeight - textHeight)/2;
//		int imagePosition = (cellHeight = imageHeight) /2;
		
		[cellView.mvmTextView.layer setPosition:CGPointMake(111,position)];

//		self->appManager.currentCouponObject.approvedDetail = false ;
//		self->appManager.currentCouponObject.approvedList = false ;
//		self->appManager.currentCouponObject.approvedWeb = false ;
		
		
	}else{
		cellView = [tableView makeViewWithIdentifier:@"TextCell" owner:self];
		[cellView updateCellLayoutWithContext:appManager andLocalArray:self.localArray forIndex:row];
		}

	return cellView;
}


-(void) tableView:(NSTableView *)tableView willDisplayCell:(mvmCouponListCellView*)cell forTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
	
	
	[cell updateCellLayoutWithContext:appManager andLocalArray:self.localArray forIndex:row];
	
	
	
	if( row > 0 && row < self.localArray.count-1){
	
		int cellHeight = cell.frame.size.height;
		int textHeight = cell.mvmTextView.frame.size.height;
		int imageHeight = cell.couponImageView.frame.size.height;
		
		int position = (cellHeight - textHeight)/2;
//		int imagePosition = (cellHeight = imageHeight) /2;
		[cell.mvmTextView.layer setPosition:CGPointMake(111,position)];
	}
	
}

- (void)tableViewSelectionIsChanging:(NSNotification *)aNotification{
	
	
//	NSLog(@"TextCellHeigh");
	
}




- (CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
	
	couponObjectFactory* myFactoryCoupon = [self.localArray objectAtIndex:row];
	return 	myFactoryCoupon.cellHeight;
	
}


// NSTableView delegate and datasource methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
	
	return self.localArray.count;//

}




// We want to make "group rows" for the folders
- (BOOL)tableView:(NSTableView *)tableView isGroupRow:(NSInteger)row {
	return NO;
	

}



-(void)mouseDown:(NSEvent *)theEvent {
    NSPoint p = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	
	// which column and cell has been hit?
	
	long column = [self columnAtPoint:p];
	long row = [self rowAtPoint:p];
	
	
	
    /* The following lines can be used if you want to retrieve the selected cell instead of the row/column indices
	 */
	
    if(column == -1 || row == -1) {
        
    }
    else {
		
		
		couponObjectFactory* myFactoryCoupon = [self.localArray objectAtIndex:row];

		mvmCouponListCellView *lastSelectedRow = [self viewAtColumn:0 row:self.appManager.mvmCouponIndex makeIfNecessary:YES];

		lastSelectedRow.editButton.hidden = true;

		
		
		mvmCouponListCellView *selectedRow = [self viewAtColumn:0 row:row makeIfNecessary:YES];
		
		if(myFactoryCoupon.errorState == true){
			
			selectedRow.editButton.hidden = false;
			selectedRow.editButton.enabled = true;
			
		}else{
			
			selectedRow.editButton.hidden = true;
		}
		
		
		self.appManager.mvmCouponIndex = row;
		self.appManager.currentCouponObject = myFactoryCoupon;
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"updateWebView" object:nil userInfo:nil];
		NSLog(@"touchedRow:%li",row);
		
		
		
		
		
    }
    [super mouseDown:theEvent];
}




@end
