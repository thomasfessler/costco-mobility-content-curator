//
//  CWCmvmTextView.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/6/14.
//
//

#import <Cocoa/Cocoa.h>
#import "CWCAppManager.h"

@interface mvmCouponTextView : NSTextView

-(void) setCouponAttributedString:(NSAttributedString*)couponString;

@end
