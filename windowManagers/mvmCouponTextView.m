//
//  CWCmvmTextView.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/6/14.
//
//

#import "mvmCouponTextView.h"

@implementation mvmCouponTextView

- (instancetype)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}



-(void) setCouponAttributedString:(NSAttributedString*)couponString{
	if(couponString != nil){
	
	[[self textStorage] setAttributedString:couponString];
	[[self layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0, [[self textStorage] length])];
	[self sizeToFit];
	
	[super viewDidChangeBackingProperties];
//	[[self layer] setContentsScale:[[self window] backingScaleFactor]];
	self.editable = false;
//	[self setCanDrawConcurrently:true];
//	[self setVerticallyResizable:true];
	}
}


@end
