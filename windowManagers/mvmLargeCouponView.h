//
//  CWCmvmCouponViewController.h
//  CostcoApp
//
//  Created by Thomas Fessler on 5/28/14.
//  Copyright (c) 2014 Costco Wholesale. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "CWCAppManager.h"
@interface mvmLargeCouponView : NSView

@property (nonatomic, retain)  NSString*  couponImageName;
@property (strong, nonatomic) IBOutlet NSImageView *couponImageView;
@property (strong, nonatomic) IBOutlet NSImageView *couponImageWarningView;
@property (strong, nonatomic) IBOutlet NSImageView *couponTextWarningView;


@property (strong, nonatomic) IBOutlet NSImageView *couponListImageView;

@property (strong, nonatomic) IBOutlet NSView *borderViewList;
@property (strong, nonatomic) IBOutlet NSScrollView *borderScrollViewLarge;



@property (strong, nonatomic) IBOutlet NSButton *proofedDetail;
@property (strong, nonatomic) IBOutlet NSButton *proofedList;
@property (strong, nonatomic) IBOutlet NSButton *proofedWeb;
@property (strong, nonatomic) IBOutlet NSButton *approvedFinal;

@property (strong, nonatomic) IBOutlet mvmCouponTextView *couponTitle;


@property (strong, nonatomic) IBOutlet mvmCouponTextView *couponListTitle;
@property (strong, nonatomic) IBOutlet NSTextField *couponBanner;
@property (strong, nonatomic) IBOutlet NSTextField *couponBannerList;


@property (strong, nonatomic) IBOutlet NSImageView *availableDotCom;
@property (strong, nonatomic) IBOutlet NSImageView *availableWarehouse;

@end
