//
//  CWCmvmCouponView.m
//  CostcoApp
//
//  Created by Thomas Fessler on 5/28/14.
//  Copyright (c) 2014 Costco Wholesale. All rights reserved.
//

#import "mvmLargeCouponView.h"

//#import "UIImage+Extras.h"


@interface mvmLargeCouponView(){
	
	CWCAppManager *myAppManager;
}

@end

@implementation mvmLargeCouponView
@synthesize couponImageName, couponImageView, couponTitle,availableDotCom, availableWarehouse, couponListImageView, couponListTitle, borderViewList, couponImageWarningView, couponTextWarningView, couponBanner, couponBannerList;


-(void) awakeFromNib{
	
	self->myAppManager = [CWCAppManager sharedAppManager];

		[self updatePageWithContext:self->myAppManager forIndex:self->myAppManager.mvmCouponIndex];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWebView) name:@"updateWebView" object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWebView) name:@"updateApprovalView" object:nil];
	

	
	
	
	[self updateCellLayoutWithContext:self->myAppManager forIndex:0];
	[self.couponTitle setNeedsDisplay:true];

	self.layer.backgroundColor =CGColorCreateGenericRGB(.9, .9,.9, 1.0);
	self.layer.borderWidth = 1.5;
	self.layer.cornerRadius = 3;
	
	
	
	CALayer *viewLayer = [CALayer layer];
	[viewLayer setBackgroundColor:CGColorCreateGenericRGB(1.0, 1.0,1.0, 1.0)];
	[self.borderViewList setWantsLayer:YES];
	[self.borderViewList setLayer:viewLayer];
	self.borderViewList.layer.borderColor =[NSColor grayColor].CGColor;
	self.borderViewList.layer.borderWidth = 1.5;
	self.borderViewList.layer.cornerRadius = 3;

	CALayer *scrollViewLayer = [CALayer layer];
	[scrollViewLayer setBackgroundColor:CGColorCreateGenericRGB(1.0, 1.0,1.0, 1.0)];
	[self.borderScrollViewLarge setWantsLayer:YES];
	[self.borderScrollViewLarge setLayer:scrollViewLayer];
	self.borderScrollViewLarge.layer.borderColor =[NSColor grayColor].CGColor;
	self.borderScrollViewLarge.layer.borderWidth = 1.5;
	
	self.borderScrollViewLarge.layer.cornerRadius = 3;
	[self.borderScrollViewLarge.documentView setFrame: CGRectMake(0, 0, self.borderScrollViewLarge.frame.size.width, 2000)];
	

}

-(void) updateWebView{
		
	[self updatePageWithContext:self->myAppManager forIndex:self->myAppManager.mvmCouponIndex];
	
}

- (void)viewDidLoad
{
//    [super viewDidLoad];



}

-(void) viewWillAppear:(BOOL)animated{

	

}

-(IBAction)editImage:(id)sender{
	
	[self->myAppManager editActiveCouponImage];
	
}



-(void) updatePageWithContext:(CWCAppManager*)appManager forIndex:(int)cellIndex{

	[self updateCellLayoutWithContext:appManager forIndex:appManager.mvmCouponIndex];
	[self updateBannerWithContext:appManager forIndex:appManager.mvmCouponIndex];
	[self updateProductViewContext:appManager forIndex:appManager.mvmCouponIndex];

}


-(void) updateCellLayoutWithContext:(CWCAppManager*)appManager forIndex:(int)cellIndex{
    
	
	self.couponTitle.backgroundColor = [NSColor clearColor];
	
	[self.couponTitle setCouponAttributedString:self->myAppManager.currentCouponObject.couponTextDetailAttributed];

	[self.couponListTitle setCouponAttributedString:self->myAppManager.currentCouponObject.couponTextListAttributed];

	[self.couponListTitle display];
	[self.couponListTitle sizeToFit];
	int cellHeight = self.borderViewList.frame.size.height;
	int textHeight = self.couponListTitle.layer.frame.size.height;
//	int imageHeight = cell.couponImageView.frame.size.height;
	
	int position = (cellHeight - textHeight*.5)- textHeight*.5;
	//		int imagePosition = (cellHeight = imageHeight) /2;
	
	if(self->myAppManager.currentCouponObject.showDotComLink == false){
			[self.couponListTitle.layer setPosition:CGPointMake(self.couponListTitle.layer.position.x,position+20)];
		
	}else{
	
	[self.couponListTitle.layer setPosition:CGPointMake(self.couponListTitle.layer.position.x,position)];
	
	}
	
}

-(void) updateProductViewContext:(CWCAppManager*)appManager forIndex:(int)cellIndex{

	if(appManager.currentCouponObject.showCouponBanner == true){
		self.couponBanner.layer.hidden = false;
		self.couponBanner.stringValue = appManager.currentCouponObject.couponBannerText;
		self.couponBannerList.layer.hidden = false;
		self.couponBannerList.stringValue = appManager.currentCouponObject.couponBannerText;
	}else{
		self.couponBanner.layer.hidden = true;
		self.couponBannerList.layer.hidden = true;
	}
	
	
	if(appManager.currentCouponObject.errorState == true){
		
		//		self.layer.backgroundColor = [NSColor redColor].CGColor;
		for(NSDictionary* errors in appManager.currentCouponObject.errorArray){
			
			if([errors valueForKeyPath:@"errorImageName"] != nil ||[errors valueForKeyPath:@"errorImagePath"] || [errors valueForKeyPath:@"errorImageWidth"] ){
				
				self.couponImageWarningView.hidden = false;
			}
			
			if([errors valueForKeyPath:@"errorProductNameLength"] != nil ){
				
				self.couponTextWarningView.hidden = false;
			}
			
			
			if([errors valueForKeyPath:@"errorChooseFrom"] != nil ){
				
				self.couponTextWarningView.hidden = false;
			}
			
		}
		
	}else{
		
		
		self.couponImageWarningView.hidden = true;
		self.couponTextWarningView.hidden = true;
	}

	
	[self.proofedDetail setState:appManager.currentCouponObject.approvedDetail];
	[self.proofedWeb setState:appManager.currentCouponObject.approvedWeb];
	[self.proofedList setState:appManager.currentCouponObject.approvedList];
	[self.approvedFinal setState:appManager.currentCouponObject.approvedFinal];
	
	
	
	NSString *fileName = @"/Users/tfessler/costco-mobility-content-curator/mvmImages/";


	if(appManager.currentCouponObject.imageFilenameMobile != nil){
	fileName = [fileName stringByAppendingString:appManager.currentCouponObject.imageFilenameMobile];
	self.couponImageView.image = [[NSImage alloc]initWithContentsOfFile:fileName];
		self.couponListImageView.image = [[NSImage alloc]initWithContentsOfFile:fileName];
		
		
	}

}

-(IBAction)checkboxDetailPressed:(NSButton*)sender{
		
	if(sender.tag == 200){
		self->myAppManager.currentCouponObject.approvedDetail = sender.state ;
	}
	if(sender.tag == 201){
		self->myAppManager.currentCouponObject.approvedList = sender.state ;
	}
	if(sender.tag == 202){
		self->myAppManager.currentCouponObject.approvedWeb = sender.state ;
	}
	if(sender.tag == 203){
		self->myAppManager.currentCouponObject.approvedFinal = sender.state ;
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadList" object:nil userInfo:nil];

}




-(void) updateBannerWithContext:(CWCAppManager*)appManager forIndex:(int)cellIndex
	{
		
		
		
/*		self.availableWarehouse.layer.shadowOffset = CGSizeMake(4, 4);
		self.availableWarehouse.layer.shadowColor = [UIColor blackColor].CGColor;
		self.availableWarehouse.layer.masksToBounds=NO;
		self.availableWarehouse.layer.shadowRadius= 3;
		self.availableWarehouse.layer.shadowOpacity= 0.7;
		
		self.availableDotCom.layer.shadowOffset = CGSizeMake(4, 4);
		self.availableDotCom.layer.shadowColor = [UIColor blackColor].CGColor;
		self.availableDotCom.layer.masksToBounds=NO;
		self.availableDotCom.layer.shadowRadius= 3;
		self.availableDotCom.layer.shadowOpacity= 0.7;

		
		if([[self->myAppManager.CWCmvmDataArray objectAtIndex:self->myAppManager.mvmCouponIndex] valueForKeyPath:@"comBug"] != nil){
			self.availableDotCom.hidden = false;
			self.availableWarehouse.hidden = true;
		}
		else{
			self.availableDotCom.hidden = true;
			self.availableWarehouse.hidden = true;
		}
		//	[self.availableWarehouse.layer setPosition: CGPointMake(60, 60)];
		
		[self.availableDotCom.layer setPosition: CGPointMake(60, 40)];
		*/
	}




@end
