//
//  mvmPathControl.h
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/8/14.
//
//

#import <Cocoa/Cocoa.h>

@interface mvmPathControl : NSPathControl<NSPathControlDelegate>


- (void)pathCtrlValueChanged:(id)sender;

@end
