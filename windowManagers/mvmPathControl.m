//
//  mvmPathControl.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/8/14.
//
//

#import "mvmPathControl.h"

@implementation mvmPathControl

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

-(void) awakeFromNib{
	
	self.delegate = self;
	
}


- (void)pathCtrlValueChanged:(id)sender {
    NSURL *url = [self objectValue];
	//    _rootContents = [[ATDesktopFolderEntity alloc] initWithFileURL:url];
	//    [_outlineView reloadData];
}


@end
