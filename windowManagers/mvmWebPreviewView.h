

//
//  mvmWebView
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "CWCAppManager.h"

@interface mvmWebPreviewView : WebView {

}

//- (void)injectCookie:(NSHTTPCookie *)cookie;

@property       CWCAppManager* myAppManager;
@property		bool	loaded;
@property		NSString*	myURL;


- (void)loadRequestFromString:(NSString *)myURL;


@end
