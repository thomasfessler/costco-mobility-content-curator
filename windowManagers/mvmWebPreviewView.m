
//
//  mvmWebView
//

#import "mvmWebPreviewView.h"
#import "NSTimer+Blocks.h"

#pragma mark -
#pragma mark main class implementation

@implementation mvmWebPreviewView
@synthesize loaded, myAppManager, myURL;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		self.loaded = false;
        // Initialization code here.
		[self awakeFromNib];
    }
    return self;
}

- (void)awakeFromNib
{
	
	if(self.loaded == false){
		self.loaded = true;
	
		self.myAppManager = [CWCAppManager sharedAppManager];

		[self setFrameLoadDelegate:self];

		
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWebView) name:@"updateWebView" object:nil];
		
		CALayer *viewLayer = [CALayer layer];
		[viewLayer setBackgroundColor:CGColorCreateGenericRGB(1.0, 1.0,1.0, 1.0)];
		[self setWantsLayer:YES];
		[self setLayer:viewLayer];
		self.layer.borderColor =[NSColor grayColor].CGColor;
		self.layer.borderWidth = 1.5;
		self.layer.cornerRadius = 3;
		[self updateWebView];
		[[[self mainFrame] frameView] setAllowsScrolling:NO];
		
		NSScrollView* scrollView = [[[[self mainFrame] frameView] documentView] enclosingScrollView];
		scrollView.hasVerticalScroller = NO;
		scrollView.hasHorizontalScroller = NO;
		
		[[scrollView horizontalScroller] setControlSize: NSSmallControlSize];
	}
	

}

-(void) updateWebView{

	
		 couponObjectFactory* myFactory = self.myAppManager.currentCouponObject;
		 
	if(![myFactory.dotComURL isEqualToString:self.myURL]){
		
		[self.mainFrame loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/web/loading.html"]]];
	
		self.myURL = myFactory.dotComURL;
		 if( myFactory.dotComURL!= nil){
			 
			 [NSTimer scheduledTimerWithTimeInterval:.5 block:^
			  {

			 
				  [self loadRequestFromString: myFactory.dotComURL];
			 	 } repeats:NO];
		 }else{
			 [NSTimer scheduledTimerWithTimeInterval:.5 block:^
			  {
			 [self.mainFrame loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:@"/Users/tfessler/costco-mobility-content-curator/mvmImages/web/blank.html"]]];
			 
			 			 	 } repeats:NO];
		 }
	}
	 

	
		
	
	
	
	
}

- (void)loadRequestFromString:(NSString *)myURL
{
	
	NSLog(@"loading request for location:%@",myURL);
	
	NSURL *url = [NSURL URLWithString:myURL];
    NSMutableURLRequest *myRequest = [[NSMutableURLRequest alloc] initWithURL:url];
	
	
	NSDictionary *userAgentReplacement = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3", @"UserAgent", nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:userAgentReplacement];
	[self setCustomUserAgent:@"Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"];
	
	
	[myRequest setValue:[NSString stringWithFormat:@"Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"] forHTTPHeaderField:@"User-Agent"];
	
	[self.mainFrame loadRequest:myRequest];

	
}



@end
