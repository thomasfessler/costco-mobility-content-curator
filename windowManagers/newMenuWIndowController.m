//
//  newMenuWIndowController.m
//  CostcoMobiltyContentManager
//
//  Created by Thomas Fessler on 6/11/14.
//
//

#import "newMenuWIndowController.h"

@interface newMenuWIndowController ()

@end

@implementation newMenuWIndowController
@synthesize arrayController = _arrayController;


- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
	
	[_arrayController addObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Mailo", @"name",
                                  nil
                                  ]
     ];
    
    [_arrayController addObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Renne", @"name",
                                  nil
                                  ]
     ];
    
    [_arrayController addObject: [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  @"Rudolph", @"name",
                                  nil
                                  ]
     ];
    
	
}


-(NSString *)windowNibName {
    return @"newMenu";
}



@end
